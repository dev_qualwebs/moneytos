//
//  AppDelegate.swift
//  Moneytoss
//
//  Created by qw on 14/08/20.
//  Copyright © 2020 qw. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import GooglePlaces
import GoogleMapsBase
import FirebaseMessaging
import Firebase

@UIApplicationMain
    class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        //Handling smart keyboard
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.enableAutoToolbar = true
        
        configureFirebase(application: application)
        initializeGoogleMap()
        return true
    }
    
    func initializeGoogleMap(){
        // GMSServices.provideAPIKey("AIzaSyAZTQHLuxlQSaiYnb3l_nkQX96bHbhbYEo")
        GMSPlacesClient.provideAPIKey("AIzaSyAZTQHLuxlQSaiYnb3l_nkQX96bHbhbYEo")
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        if(dontRememberMe){
            UserDefaults.standard.removeObject(forKey: UD_TOKEN)
        }
    }
}


extension AppDelegate: UNUserNotificationCenterDelegate {
    func configureFirebase(application: UIApplication){
        FirebaseApp.configure()
        Messaging.messaging().delegate = self


        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self

            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }


        application.registerForRemoteNotifications()
    }

    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        Messaging.messaging().apnsToken = deviceToken
    }

    //This function work when we tap on nitification
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        let userInfo = response.notification.request.content.userInfo
        if let data = userInfo as? [String:Any] {
            // if ((data["gcm.notification.type"] as! String) == "7") {
            self.handleUserNotfication(info: userInfo)
            // }
        }
    }

    func handleUserNotfication(info:[AnyHashable:Any]){
        if let token = UserDefaults.standard.value(forKey: UD_TOKEN) as? String {
            if let data = info as? [String:Any] {
                //var initialViewController = UIViewController()
               // let navigationController = self.window?.rootViewController as! UINavigationController
              //  let stringToData = Data((data["gcm.notification.data"] as! String).utf8)
                let rootViewController = self.window!.rootViewController as! UINavigationController
                   let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
                Singleton.shared.transactionData = []
                   let profileViewController = mainStoryboard.instantiateViewController(withIdentifier: "TabbarViewController") as! TabbarViewController
                rootViewController.pushViewController(profileViewController, animated: false)
//                var jobData = StartTransactionDetail()
//                let decoder = JSONDecoder()
//                do {
//                    jobData = try decoder.decode(StartTransactionDetail.self, from: stringToData)
//                }catch {
//                    print(error.localizedDescription)
//                }
//                let currentVC =  UIApplication.getTopViewController(base: ViewController())
//                switch(jobData.type){
//
//                //     'POST_SHIPMENT'|| 'POST_SHIPMENT_WITH_REGISTRATION', 1
//                case 1:
//                    initialViewController = storyboard.instantiateViewController(withIdentifier: "SingleShipmentVC") as! SingleShipmentVC
//
//                    (initialViewController as! SingleShipmentVC).id = jobData.shipment_id ?? 0
//                    navigationController.pushViewController(initialViewController, animated: false)
//                    break
//                //'DRIVER_REGISTERED', 2
//                case 2:
//                    initialViewController = storyboard.instantiateViewController(withIdentifier: "EditProfileVC") as! EditProfileVC
//
//                    navigationController.pushViewController(initialViewController, animated: false)
//                    break
//
//                default:
//                    break
//                }

            }
                 //   NotificationCenter.default.post(name: NSNotification.Name(N_NOTIFICATION_DATA),object: nil,userInfo: ["info": info])
        }
    }


    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        completionHandler([.alert, .sound, .badge])
    }

}

extension AppDelegate: MessagingDelegate {
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String?) {
        print("fcm token: \(fcmToken)")
        UserDefaults.standard.set(fcmToken, forKey: UD_FCM_TOKEN)
    }


    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
        print("foreground remote notification")
    }

    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        if let data = userInfo as? [String:Any] {
           // self.handleUserNotfication(info: userInfo)
          //  let center = UNUserNotificationCenter.current()
          // center.removeAllDeliveredNotifications()
        }
    }
}

extension UIApplication{
    class func getTopViewController(base: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {

            if let nav = base as? UINavigationController {
                return getTopViewController(base: nav.visibleViewController)

            } else if let tab = base as? UITabBarController {
                return getTopViewController(base: tab)

            } else if let presented = base?.presentedViewController {
                return getTopViewController(base: presented)
            }
            return base
        }
}
