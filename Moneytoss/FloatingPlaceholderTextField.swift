//
//  FloatingPlaceholderTextField.swift
//  Moneytoss
//
//  Created by Sagar Pandit on 28/09/21.
//  Copyright © 2021 qw. All rights reserved.
//

import Foundation
import MaterialComponents.MaterialTextFields

@IBDesignable
class FloatingPlaceholderTextField: UIView{
    
    public  var textInput: MDCTextField!
    private var controller: MDCTextInputControllerOutlined!
    private let textColor = primaryColor // Dynamic dark & light color created in the assets folder
    private var placeHolderText = ""
    private var keyboard:UIKeyboardType = .default
    public var isSecureText = false {
        didSet {
            if(textInput != nil){
                textInput.isSecureTextEntry = isSecureText
            }
        }
    }
    
    @IBInspectable var setPlaceholder: String{
        get{
            return placeHolderText
        }
        set(str){
            placeHolderText = str
        }
    }
    
    @IBInspectable var keyboardType: Int{
        get{
            return keyboardType
        }
        set(str){
            if(str == 1){
                keyboard = .default
            }else if(str == 2){
                keyboard = .numberPad
            }else if(str == 3){
                keyboard = .emailAddress
            }else if(str == 4){
                isSecureText = true
            }
        }
    }
    
    @IBInspectable var isPhoneNumber: Int = 0
    
    
    override func layoutSubviews() {
    
        setupInputView()
        setupContoller()
           
    }
    
    private func setupInputView(){
        //MARK: Text Input Setup
        
        if let _ = self.viewWithTag(1){return}
        
        textInput = MDCTextField()
        
        textInput.tag = 1
        
        textInput.translatesAutoresizingMaskIntoConstraints = false
        
        self.addSubview(textInput)
        
        textInput.placeholder = placeHolderText
        textInput.keyboardType = keyboard
        
        textInput.delegate = self
        textInput.isSecureTextEntry = self.isSecureText
        textInput.textColor = textColor
        
        NSLayoutConstraint.activate([
            (textInput.topAnchor.constraint(equalTo: self.topAnchor)),
            (textInput.bottomAnchor.constraint(equalTo: self.bottomAnchor)),
            (textInput.leadingAnchor.constraint(equalTo: self.leadingAnchor)),
            (textInput.trailingAnchor.constraint(equalTo: self.trailingAnchor))
        ])
    }
    
    private func setupContoller(){
        // MARK: Text Input Controller Setup
        
        controller = MDCTextInputControllerOutlined(textInput: textInput)
        
        controller.activeColor = .lightGray
        controller.normalColor = .lightGray
        controller.textInput?.textColor = .black
        controller.inlinePlaceholderColor = .lightGray
        controller.floatingPlaceholderActiveColor = primaryColor
        controller.floatingPlaceholderNormalColor = .lightGray
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
    }
    
}

extension FloatingPlaceholderTextField: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        //Phone Number
        if(isPhoneNumber == 1){
            let currentCharacterCount = textInput?.text?.count ?? 0
               if (range.length + range.location > currentCharacterCount){
                   return false
               }
               let newLength = currentCharacterCount + string.count - range.length
            textInput?.text = textInput?.text?.applyPatternOnNumbers(pattern: "###-###-####", replacmentCharacter: "#")
               return newLength <= 12
            //Card Number
        }else if(isPhoneNumber == 2){
            let currentCharacterCount = textInput?.text?.count ?? 0
               if (range.length + range.location > currentCharacterCount){
                   return false
               }
            let newLength = currentCharacterCount + string.count - range.length
            textInput?.text = textInput?.text?.applyPatternOnNumbers(pattern: "#### #### #### ####", replacmentCharacter: "#")
            return newLength <= 19
            //Month Field
        }else if(isPhoneNumber == 3){
            if let char = string.cString(using: String.Encoding.utf8) {
                let isBackSpace = strcmp(char, "\\b")
                if (isBackSpace == -92 || textField.text!.count < 2) {
                    return true
                }else {
                    return false
                }
            }
            //Year Field
        }else if(isPhoneNumber == 4){
            if let char = string.cString(using: String.Encoding.utf8) {
                let isBackSpace = strcmp(char, "\\b")
                if (isBackSpace == -92 || textField.text!.count < 4) {
                    return true
                }else {
                    return false
                }
            }
            //CVV Field
        }else if(isPhoneNumber == 5){
            if let char = string.cString(using: String.Encoding.utf8) {
                let isBackSpace = strcmp(char, "\\b")
                if (isBackSpace == -92 || textField.text!.count <= 3) {
                    return true
                }else {
                    return false
                }
            }
            //Account number
        }else if(isPhoneNumber == 6){
            if let char = string.cString(using: String.Encoding.utf8) {
                let isBackSpace = strcmp(char, "\\b")
                if (isBackSpace == -92 || textField.text!.count < 16) {
                    return true
                }else {
                    return false
                }
            }
            //Postal Code
        }else if(isPhoneNumber == 7){
            if let char = string.cString(using: String.Encoding.utf8) {
                let isBackSpace = strcmp(char, "\\b")
                if (isBackSpace == -92 || textField.text!.count < 6) {
                    return true
                }else {
                    return false
                }
            }
            //
        }else if(isPhoneNumber == 8){
            if let char = string.cString(using: String.Encoding.utf8) {
                let isBackSpace = strcmp(char, "\\b")
                if (isBackSpace == -92 || textField.text!.count < 10) {
                    return true
                }else {
                    return false
                }
            }
        }else if(isPhoneNumber == 9){
            let currentCharacterCount = textInput?.text?.count ?? 0
               if (range.length + range.location > currentCharacterCount){
                   return false
               }
            let newLength = currentCharacterCount + string.count - range.length
            textInput?.text = textInput?.text?.applyPatternOnNumbers(pattern: "##### : ### : #############", replacmentCharacter: "#")
            return newLength <= 27
        }
        return true
    }
}
