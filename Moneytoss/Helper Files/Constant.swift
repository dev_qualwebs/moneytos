
import UIKit


//COLORS

let primaryColor = UIColor(red: 28/255, green: 35/255, blue: 132/255, alpha: 1)//1C2384
let lightBlueColor = UIColor(red: 67/255, green: 171/255, blue: 148/255, alpha:1)//434794
let greenColor = UIColor(red: 0/255, green: 100/255, blue: 0/255, alpha:1)//006400
let redColor = UIColor(red: 139/255, green: 0/255, blue: 0/255, alpha:1)//8b0000
//247,0,125

//APIs
let U_BASE = "https://api.moneytos.com/api/"
let U_IMAGE_BASE = "https://api.moneytos.com/storage/app/public/"

let U_LOGIN = "login"
let U_REGISTER = "register"
let U_VERIFY_OTP = "verify-otp"
let U_SEND_OTP = "send-otp"
let U_FORGOT_PASS = "forget-password"
let U_VERIRFY_FORGET_PASS_OTP = "verify-otp-forget-password"
let U_RESET_PASSWORD = "reset-password"
let U_CHANGE_PASSWORD = "user/change-password"
let U_CHANGE_PIN = "user/update-security-pin"
let U_ADD_SECURITY_PIN = "user/add-security-pin"
let U_DOCUMENT_UPLOAD = "document-upload"
let U_ADD_SUPPORT_QUERY = "add-support-query"
let U_GET_SUPPORT_QUERY = "get-support-query"

let U_GET_DASHBOARD_DATA = "user/dashboard"

let U_GET_CONTACTS = "user/contacts"
let U_SEARCH_CONTACTS = "user/search"
let U_SEARCH_USER_VIA_EMAIL = "get-user-by-email"
let U_RETREIVE_BANK_ACCOUNT = "user/retrive-bank-account"
let U_UPDATE_BANK_ACCOUNT = "user/update-bank-account"
let U_SENDTO_BANK_ACCOUNT = "user/send-to-bank-account"
let U_REGISTER_BANK_ACCOUNT = "user/register-bank-account"
let U_GET_NOTIFICATION = "user/notifications"

let U_GET_PROFILE = "user/me"
let U_UPDATE_PROFILE = "user/update-profile"
let U_UPLOAD_PROFILE_IMAGE = "user/profile-image-upload"
let U_UPLOAD_DOCUMENT_IMAGE = "user/document-upload"
let U_UPLOAD_IMAGE = "image-upload"
let U_SUBMIT_DOCUMENT = "user/document-upload"
let U_GET_DOCUMENTS = "user/my-documents"

let U_GET_WALLETS = "wallet/all"
let U_ADD_MONEY_WALLET = "wallet/add-money"
let U_GET_CARDS = "user/get-saved-methods"
let U_SEARCH_USER = "user/search"
let U_WITHDRAW_MOENY = "wallet/withdraw-money"
let U_REQUEST_MONEY = "wallet/request-money"
let U_REFER_FRIEND = "refer-friend"

let U_GET_MONEY_REQUEST = "wallet/get-all-money-request/"
let U_UPDATE_MONEY_REQUEST = "wallet/update-request-status"

let U_GET_PAYMENT_METHOD = "payment/get-payment-methods"
let U_ADD_CARD = "payment/add-card"
let U_GET_ALL_TRANSACTIONS = "payment/transactions"
let U_GET_ALL_CURRENCIES = "payment/get-currencies"
let U_GET_CONVERT_CURRENCY = "payment/get-base-exchange-amount"
let U_GET_SINGLE_TRANSACTION = "payment/transaction/1"
let U_DELETE_CARD = "payment/delete-card"
let U_CALCULATE_COMISSION = "calculate-commission"

let U_GET_COUNTRIES = "get-countries"
let U_GET_CITIES = "get-cities/"
let U_GET_POSTCODE_CITY = "get-postcodes/"


let U_GENERATE_WALLET_TRANSFER_OTP = "wallet/generate-otp-for-transfer"
let U_WALLET_TRANSFER = "wallet/transfer"
let U_COD_TRANSFER = "wallet/cod-transfer"


let U_GET_POSTCODE = "postcodes"
let U_GET_SERVICE_STATION = "service-stations/"

let U_TERMS_CONDITION = "https://moneytos.com/terms"

//Constants


//Notification Center
let N_BOTTOM_NAVIGATION_OPTION = "change_bottom_navigation_name"
let N_STOP_BUTTON_ANIMATION = "N_STOP_BUTTON_ANIMATION"
let N_CHANGE_STEP_INDICATOR = "N_CHANGE_STEP_INDICATOR"

//User Default's
var UD_FCM_TOKEN = "fcm_token"
var UD_INITIAL_VIEW_CONTROLLER = "initial_view_controller"
var UD_USER_EMAIL = "user_email_address"
var UD_USER_DETAIL = "user_detail"
var UD_TOKEN = "access_token"
var UD_IS_FIRST_TIME = "launch_first_time"
var UD_CURRENT_BOTTOM_OPTION = "current_selected_option"
