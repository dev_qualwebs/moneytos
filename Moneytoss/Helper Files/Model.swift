import UIKit

struct SuccessResponse: Codable {
    var message: String?
    var status: Int?
}

struct ErrorResponse: Codable {
   var message: String?
    var status: Int?
}

struct SplashContent {
    var backgroundImage: String
    var title: String?
    var content: String?
    var subTitle: String?
}


struct MenuObject {
    var image: UIImage?
    var name: String?
}

struct UploadImage: Codable {
    var response: UploadImageResponse?
    var message: String?
    var status: Int?
}

struct UploadImageResponse: Codable {
    var image: String?
    var image_path: String?
}

struct Signup: Codable {
    var response = SignupResponse()
    var message: String?
    var status: Int?
}

struct SignupResponse: Codable {
    var id: Int?
    var username: String?
    var first_name: String?
    var last_name: String?
    var email: String?
    var email_verified_at: Int?
    var phone_number: String?
    var account_status: Int?
    var created_at: Int?
    var profile_image: String?
    var bt_customer_id: Int?
    var currency_id: Int?
    var stripe_cus_id: Int?
    var security_pin: String?
    var wallet : WalletResponse?
    var token: String?
    var updated_at: Int?
    var address: String?
    var dob: String?
    var address_latitude: String?
    var address_longitude: String?
    var qr_code: String?
    var currency = CurrencyResponse()
}

struct WalletResponse: Codable {
    var id: Int?
    var user_id: Int?
    var balance: String?
    var currency_id: Int?
    var created_at: Int?
    var iso_code: String?
    var symbol: String?
}

struct CurrencyResponse: Codable {
    var id: Int?
    var iso_code: String?
    var country: String?
    var title: String?
    var symbol: String?
    var precision: Int?
    var exchange_rate: String?
    var is_base_currency: Int?
    var is_active : Int?
}

struct Dashboard: Codable {
    var response = DashboardResponse()
    var message: String?
    var status: Int?
}

struct DashboardResponse: Codable {
    var base_url: String?
    var data = DashboardData()
    var recent_contacts = [RecentContacts]()
    var documents = [Documents]()
}

struct DashboardData: Codable {
    var sent: DashboardDataResponse?
}

struct DashboardDataResponse: Codable {
    var amount: String?
    var account_status: Int?
}

struct RecentContacts: Codable {
    
}

struct Documents: Codable {
    
}

struct GetCard: Codable {
    var response = [GetCardResponse]()
    var message: String?
    var status: Int?
}

struct GetCardResponse: Codable {
    var id: Int?
    var user_id: Int?
    var payment_gateway: Int?
    var payment_mode: Int?
    var masked_card_number: String?
    var name_on_card: String?
    var expiry_month: String?
    var expiry_year: String?
    var is_blocked: Int?
    var created_at: Int?
    var getway_name: String?
    var commission: String?
}

struct GetPaymentMethod: Codable {
    var response = [PaymentMethodResponse]()
    var message: String?
    var status: Int?
}

struct PaymentMethodResponse: Codable {
    var id: Int?
    var gateway_name: String?
    var gateway_type: String?
    var is_active: Int?
    var payment_mode: Int?
    var credential_file: String?
    var commission: String?
}

struct GetCurrencies: Codable {
    var response = [CurrencyResponse]()
    var message: String?
    var status: Int?
}

struct GetWallet: Codable {
    var response = [WalletResponse]()
    var message: String?
    var status: Int?
}

struct GetTransaction: Codable {
    var response = [TransactionResponse]()
    var message: String?
    var status: Int?
}

struct TransactionResponse: Codable {
    var id: Int?
    var transaction_type: Int?
    var amount: String?
    var local_transaction_id: String?
    var user_id: Int?
    var currency_id:String?
    var created_at: Int?
    var notes: String?
    var transaction_method: String?
    var sender_name: String?
    var sender_id: Int?
    var receiver_name: String?
    var sender_profile_image: String?
    var receiver_profile_image: String?
    var receiver_id: Int?
    var iso_code: String?
    var masked_card_number: String?
    var gateway_name: String?
    var sender_phone_number: String?
    var sender_email: String?
    var receiver_phone_number: String?
    var receiver_email: String?
}

struct SearchUser: Codable {
    var response = [SearchUserResponse]()
    var message: String?
    var status: Int?
}

struct SearchUserResponse: Codable {
    var id: Int?
    var first_name: String?
    var last_name: String?
    var username: String?
    var profile_image: String?
    var email: String?
    var phone_number: String?
    var service_station: String?
    var country_name: String?
}

struct GetPostCode: Codable {
    var response = [PostCodeResponse]()
    var message: String?
    var status: Int?
}

struct PostCodeResponse: Codable {
    var id: Int?
    var postcode: String?
    var service_station: String?
}

struct GetBankDetail: Codable{
    var response = [BankDetail]()
    var status: Int?
    var message: String?
}

struct BankDetail: Codable {
    var company = CompanyDetail()
    var capabilities = Capabilities()
    var external_accounts = ExternalAccount()
}

struct Capabilities: Codable {
    var card_payments: String?
    var transfers: String?
}

struct CompanyDetail: Codable{
    var address = AddressDetail()
}

struct AddressDetail:Codable {
    var city: String?
    var country: String?
    var line1: String?
    var line2: String?
    var postal_code: String?
    var state: String?
    var routing_number: String?
}

struct ExternalAccount: Codable {
   var data = [AddressDetail]()
}

struct GetComiission: Codable {
    var response = CommissionResponse()
    var message: String?
    var status: Int?
}

struct CommissionResponse: Codable {
    var chargable_amount: Double?
    var commission: String?
    var commission_amount: Double?
}

struct GetCountries: Codable {
    var response = CountryResponse()
    var status: Int?
    var message:String?
}

struct GetCities: Codable {
    var response = CityResponse()
    var status: Int?
    var message:String?
}

struct CountryResponse: Codable {
    var countries = [Countires]()
}

struct CityResponse: Codable {
    var cities = [Countires]()
}

struct Countires: Codable {
    var id: Int?
    var country: String?
    var country_id: Int?
    var city: String?
    var postcode: String?
}

struct GetSupportQuery: Codable {
    var response = SupportQueryData()
    var message:String?
    var status: Int?
}

struct SupportQueryData: Codable{
    var data = [SupportQuery]()
}

struct SupportQuery: Codable {
    var id: Int?
    var user_id: Int?
    var first_name: String?
    var last_name: String?
    var email: String?
    var query:String?
    var image: String?
    var created_at: String?
}

struct SendMoney: Codable{
    var response:String?
    var message:String?
    var status: Int?
}

struct GetNotification: Codable {
    var response = [NotificationResposne]()
    var message: String?
    var status: Int?
}

struct NotificationResposne: Codable{
    var id: Int?
    var receiver_id: Int?
    var sender_id: Int?
    var message: String?
    var read: Int?
    var type: Int?
    var date_time: Int?
    var created_at: Int?
    var sender_name: String?
    var request_status: String?
    var request_id: Int?
    var sender_profile_image: String?
    var receiver_name: String?
    var receiver_profile_image: String?
    var notification_type_id: Int?
    var redirect: String?
    var content: String?
}

struct GetMoneyRequest: Codable{
    var response = [MoneyRequest]()
    var message: String?
    var status: Int?
}

struct MoneyRequest: Codable {
    var id: Int?
    var user_id: Int?
    var requested_user_id: Int?
    var amount: String?
    var currency_id: Int?
    var created_at: Int?
    var status: Int?
    var converted_amount: String?
    var user_details: SearchUserResponse?
    var currency: CurrencyResponse?
}

struct UserDocument: Codable {
    var response = UserDocumentResponse()
    var message: String?
    var status: Int?
}

struct UserDocumentResponse: Codable {
    var documents = [DocumentResponse]()
}

struct DocumentResponse: Codable {
    var id: Int?
    var user_id: Int?
    var document_type: String?
    var document_path: String?
    var document_number: String?
    var is_verified: Int?
    var created_at: Int?
    var updated_at: Int?
}

struct GetUserInfoScan: Codable {
    var response = UserInforScanResponse()
    var message: String?
    var status: Int?
}

struct UserInforScanResponse: Codable {
    var id: Int?
    var first_name: String?
    var last_name: String?
    var email: String?
    var phone_number: String?
    var profile_image: String?
    var username: String?
    var qr_code: String?
}
