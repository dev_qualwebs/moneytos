//
//  APIViewController.swift
//  TCR
//
//  Created by AM on 01/08/19.
//  Copyright © 2019 AM. All rights reserved.
//

import UIKit
import CoreLocation


class NavigationController: UIViewController {
    
    static var shared = NavigationController()
    var  locationManager = CLLocationManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    func addTransition(direction: CATransitionSubtype,controller: UIViewController) {
        let transition:CATransition = CATransition()
        transition.duration = 0.4
        transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        transition.type = CATransitionType.push
        transition.subtype = direction
        
        UIApplication.shared.windows.first?.backgroundColor = UIColor.white.withAlphaComponent(0.4)
        //self.view.backgroundColor = .white
        controller.navigationController?.view.layer.add(transition, forKey: nil)
    }
    
    func hasLocationPermission() -> Bool {
        var hasPermission = false
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
            case .notDetermined, .restricted, .denied:
                hasPermission = false
            case .authorizedAlways, .authorizedWhenInUse:
                hasPermission = true
            @unknown default:
                break
            }
        } else {
            hasPermission = false
        }
        return hasPermission
    }
    
    func pushDashboard(controller: UIViewController){
        let myVC = controller.storyboard?.instantiateViewController(withIdentifier: "TabbarViewController") as! TabbarViewController
        self.addTransition(direction: .fromLeft, controller: myVC)
        controller.navigationController?.pushViewController(myVC, animated: true)
    }
}

extension NavigationController {
    func getPaymentMethod(completionHandler: @escaping ([PaymentMethodResponse]) -> Void){
        if(Singleton.shared.paymentMethods.count == 0){
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_GET_PAYMENT_METHOD, method: .get, parameter: nil, objectClass: GetPaymentMethod.self, requestCode: U_GET_PAYMENT_METHOD) { (response) in
            Singleton.shared.paymentMethods = response.response
            completionHandler(response.response)
            }
        }else {
           completionHandler(Singleton.shared.paymentMethods)
        }
        
    }
    
    func getCurrencies(completionHandler: @escaping ([CurrencyResponse]) -> Void){
        if(Singleton.shared.currencyData.count == 0){
            SessionManager.shared.methodForApiCalling(url: U_BASE + U_GET_ALL_CURRENCIES, method: .get, parameter: nil, objectClass: GetCurrencies.self, requestCode: U_GET_ALL_CURRENCIES) { (response) in
                Singleton.shared.currencyData = response.response
                completionHandler(response.response)
            }
        }else {
            completionHandler(Singleton.shared.currencyData)
        }
    }
    
    func getCardData(completionHandler: @escaping ([GetCardResponse]) -> Void){
        if(Singleton.shared.cardData.count == 0){
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_GET_CARDS, method: .get, parameter: nil, objectClass: GetCard.self, requestCode: U_GET_CARDS) { (response) in
            Singleton.shared.cardData = response.response
            completionHandler(Singleton.shared.cardData)
        }
        }else {
           completionHandler(Singleton.shared.cardData)
        }
    }
    
    func getWalletData(completionHandler: @escaping ([WalletResponse]) -> Void){
        if(Singleton.shared.walletData.count == 0){
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_GET_WALLETS, method: .get, parameter: nil, objectClass: GetWallet.self, requestCode: U_GET_WALLETS) { (response) in
            Singleton.shared.walletData = response.response
            completionHandler(Singleton.shared.walletData)
        }
        }else {
           completionHandler(Singleton.shared.walletData)
        }
    }

    func getTransactionData(completionHandler: @escaping ([TransactionResponse]) -> Void){
        if(Singleton.shared.transactionData.count == 0){
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_GET_ALL_TRANSACTIONS, method: .post, parameter: nil, objectClass: GetTransaction.self, requestCode: U_GET_ALL_TRANSACTIONS) { (response) in
            Singleton.shared.transactionData = response.response
            completionHandler(Singleton.shared.transactionData)
        }
        }else {
           completionHandler(Singleton.shared.transactionData)
        }
    }
    
    func getContactData(completionHandler: @escaping ([TransactionResponse]) -> Void){
        if(Singleton.shared.contactData.count == 0){
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_GET_CONTACTS, method: .get, parameter: nil, objectClass: GetTransaction.self, requestCode: U_GET_CONTACTS) { (response) in
            Singleton.shared.contactData = response.response
            completionHandler(Singleton.shared.contactData)
        }
        }else {
           completionHandler(Singleton.shared.contactData)
        }
    }
    
    func getCountryData(completionHandler: @escaping ([Countires]) -> Void){
        if(Singleton.shared.countryData.count == 0){
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_GET_COUNTRIES, method: .get, parameter: nil, objectClass: GetCountries.self, requestCode: U_GET_COUNTRIES) { (response) in
            Singleton.shared.countryData = response.response.countries
            completionHandler(Singleton.shared.countryData)
        }
        }else {
           completionHandler(Singleton.shared.countryData)
        }
    }

    func getUserDocument(completionHandler: @escaping (UserDocumentResponse) -> Void){
        if(Singleton.shared.userDocument.documents.count == 0){
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_GET_DOCUMENTS, method: .post, parameter: nil, objectClass: UserDocument.self, requestCode: U_GET_DOCUMENTS) { (response) in
            Singleton.shared.userDocument = response.response
            completionHandler(Singleton.shared.userDocument)
        }
        }else {
           completionHandler(Singleton.shared.userDocument)
        }
    }

}
