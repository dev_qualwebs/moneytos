//
//  NotAvailableViewController.swift
//  TCR
//
//  Created by qw on 09/06/20.
//  Copyright © 2020 qw. All rights reserved.
//

import UIKit

class NotAvailableViewController: UIViewController {
    
    //MARK: IBOutlets
    
    @IBOutlet weak var titleText: UILabel!
    
    var heading = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.titleText.text = heading
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(2)) {
          self.dismiss(animated: false, completion: nil)
        }
        
    }
    //MARK: IBActions
    @IBAction func dismissAction(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
}
