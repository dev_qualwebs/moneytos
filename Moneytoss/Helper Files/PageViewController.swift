

import UIKit

protocol PageViewScroll {
    func changeCurrentPage(page: Int)
}

class SplashPageController: UIPageViewController
{
    static var dataSource: UIPageViewControllerDataSource?
    var splashViewControllers: [UIViewController] = []
    var pageControl = UIPageControl()
    static var pageDelegate: PageViewScroll? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        handleView()
        setControllers()
        self.dataSource = self
        self.delegate = self
        SplashPageController.dataSource = self
        for view in self.view.subviews {
                   if let subView = view as? UIScrollView {
                           subView.isScrollEnabled = true
                   }
               }
    }
    
    func setControllers() {
        if let firstViewController = splashViewControllers.first {
            setViewControllers([firstViewController],
                               direction: .reverse,
                               animated: false,
                               completion: nil)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
    }
    
    override func viewDidAppear(_ animated: Bool) {
        SplashPageController.dataSource = self
    }
    
    func handleView() {
      splashViewControllers = [self.newColoredViewController(controller: "Splash1ViewController"),
        self.newColoredViewController(controller: "Splash2ViewController"),
        self.newColoredViewController(controller: "Splash3ViewController"),
        self.newColoredViewController(controller: "Splash4ViewController")]
    }
    
    func setControllerFirst() {
        if let firstViewController = splashViewControllers.first {
            setViewControllers([firstViewController],
                               direction: .forward,
                               animated: false,
                               completion: nil)
        }
    }
    
    
    func setControllerSecond() {
        setViewControllers([splashViewControllers[1]], direction: .forward, animated: false, completion: nil)
    }
    
    func setControllerThird() {
        setViewControllers([splashViewControllers[2]], direction: .forward, animated: false, completion: nil)
    }

    
    func setControllerLast() {
        if let lastViewController = splashViewControllers.last {
            setViewControllers([lastViewController],
                               direction: .forward,
                               animated: false,
                               completion: nil)
        }
    }
    

    
    private func newColoredViewController(controller: String)->UIViewController {
        return UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: controller)
    }
}

extension SplashPageController: UIPageViewControllerDataSource
{
    func pageViewController(_ pageViewController: UIPageViewController,
                            viewControllerBefore viewController: UIViewController) -> UIViewController? {
      //  currentControllerIndex(VC: viewController)
        guard let viewControllerIndex = splashViewControllers.index(of: viewController) else {
            return nil
        }
        let previousIndex = viewControllerIndex - 1
        guard previousIndex >= 0 else {
            return nil
        }
        guard splashViewControllers.count > previousIndex else {
            return nil
        }
        return splashViewControllers[previousIndex]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController,
                            viewControllerAfter viewController: UIViewController) -> UIViewController? {
        //currentControllerIndex(VC: viewController)
        guard let viewControllerIndex = splashViewControllers.index(of: viewController) else {
            return nil
        }
        let nextIndex = viewControllerIndex + 1
        guard splashViewControllers.count != nextIndex else {
            return nil
        }
        guard splashViewControllers.count > nextIndex else {
            return nil
        }
        return splashViewControllers[nextIndex]
    }
    
}

extension SplashPageController : UIPageViewControllerDelegate {
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        let pageContentViewController = pageViewController.viewControllers![0]
            self.pageControl.currentPage = splashViewControllers.index(of: pageContentViewController)!
            SplashPageController.self.pageDelegate?.changeCurrentPage(page:  splashViewControllers.index(of: pageContentViewController)!)
    }
}

