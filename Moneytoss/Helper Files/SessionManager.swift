//
//  APIManager.swift
//
//  Created by Qualwebs on 02/01/2019.
//  Copyright © 2019 Qualwebs. All rights reserved.
//

import UIKit
import Alamofire


class SessionManager: NSObject {

    static var shared = SessionManager()

    var createWallet: Bool = true

    func methodForApiCalling<T: Codable>(url: String, method: HTTPMethod, parameter: Parameters?, objectClass: T.Type, requestCode: String, completionHandler: @escaping (T) -> Void) {
        print("URL: \(url)")
        print("METHOD: \(method)")
        print("PARAMETERS: \(parameter)")
    
        Alamofire.request(url, method: method, parameters: parameter, encoding: JSONEncoding.default, headers: getHeader(reqCode: requestCode)).responseString { (dataResponse) in

            let statusCode = dataResponse.response?.statusCode
            print("statusCode: ",dataResponse.response?.statusCode)
            print("dataResponse: \(dataResponse)")

            switch dataResponse.result {
            case .success(_):
                var object:T?
                if(statusCode != 400){
                   object = self.convertDataToObject(response: dataResponse.data, T.self)
                }else if(statusCode == 200){
                   object = self.convertDataToObject(response: dataResponse.data, T.self)
                }
                let errorObject = self.convertDataToObject(response: dataResponse.data, ErrorResponse.self)
                if (statusCode == 200 || statusCode == 201) && object != nil{
                    completionHandler(object!)
                } else if statusCode == 404  {
                    NotificationCenter.default.post(name: NSNotification.Name(N_STOP_BUTTON_ANIMATION), object: nil)
                    if(errorObject?.message != "" || errorObject?.message != nil){
                        Singleton.shared.showToast(text:errorObject?.message ?? "")
                    }
                }else if statusCode == 400{
                    NotificationCenter.default.post(name: NSNotification.Name(N_STOP_BUTTON_ANIMATION), object: nil)
                        if(errorObject?.message != "" || errorObject?.message != nil){
                            Singleton.shared.showToast(text:errorObject?.message ?? "")
                        
                        }
                } else {
                    NotificationCenter.default.post(name: NSNotification.Name(N_STOP_BUTTON_ANIMATION), object: nil)
//                    if(errorObject?.message != "" || errorObject?.message != nil){
//                    NotificationCenter.default.post(name: NSNotification.Name(rawValue:N_SHOW_ERROR_MESSAGE), object: nil, userInfo: ["msg": errorObject?.message ?? ""])
//                    }
                    // NavigationController.shared.showAlertScreen(message: errorObject?.message ?? "")
                     // self.showAlert(msg: errorObject?.message)
                }
                ActivityIndicator.hide()
                break
            case .failure(_):
                ActivityIndicator.hide()
                let error = dataResponse.error?.localizedDescription
                if error == "The Internet connection appears to be offline." {
                    //Showing error message on alert
                    if(error != "" || error != nil){
                        Singleton.shared.showToast(text: error ?? "")
                    }
                   //  NavigationController.shared.showAlertScreen(message: error ?? "")
                    //self.showAlert(msg: error)
                      
                } else {
                    //Showing error message on alert
                   // self.showAlert(msg: error)
                    
                }
                break
            }
        }
    }

    private func showAlert(msg: String?) {
       // UIApplication.shared.keyWindow?.rootViewController?.showAlert(title:"", message: msg, action1Name: "Ok", action2Name: nil)

    }

    func makeMultipartRequest(url: String, fileData: Data, param: [String:Any], fileName: String, completionHandler: @escaping (Any) -> Void) {

        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in param {
                if key == "file_type" || key == "document_type" || key == "document_number" {
                    multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
                }else {
                    multipartFormData.append(value as! Data, withName: key as! String, fileName: "image.png", mimeType: "image/png")
                }
            }

        }, usingThreshold: UInt64.init(), to: url, method: .post, headers: getHeader(reqCode: "")) { (encodingResult) in

            switch encodingResult {
            case .success(let response,_,_):
                response.responseString(completionHandler: { (dataResponse) in

                    ActivityIndicator.hide()

                    let errorObject = self.convertDataToObject(response: dataResponse.data, SuccessResponse.self)

                    if dataResponse.response?.statusCode == 200 {
                        let object = self.convertDataToObject(response: dataResponse.data, UploadImage.self)
                        completionHandler(object!)
                    } else {
                    UIApplication.shared.keyWindow?.rootViewController?.showAlert(title: "Error", message: errorObject?.message, action1Name: "Ok", action2Name: nil)
                        ActivityIndicator.hide()
                    }
                })
                break
            case .failure(let error):
                //Showing error message on alert
UIApplication.shared.keyWindow?.rootViewController?.showAlert(title: "Error", message: error.localizedDescription, action1Name: "Ok", action2Name: nil)
       ActivityIndicator.hide()
                break
            }
        }
    }

    private func convertDataToObject<T: Codable>(response inData: Data?, _ object: T.Type) -> T? {
        if let data = inData {
            do {
                let decoder = JSONDecoder()
                let decoded = try decoder.decode(T.self, from: data)
                return decoded
            } catch {
                print(error)
            }
        }
        return nil
    }

    
    func getHeader(reqCode: String) -> HTTPHeaders? {
        let token = UserDefaults.standard.string(forKey: UD_TOKEN)
        if (reqCode != U_LOGIN) && (reqCode != U_REGISTER) && (reqCode != U_RESET_PASSWORD) && (reqCode != U_VERIFY_OTP) && (reqCode != U_VERIRFY_FORGET_PASS_OTP) && (reqCode != U_FORGOT_PASS) {
            if(token == nil){
                return nil
            }else {
                print(token)
                return ["Authorization": "Bearer " + token!]
            }
            } else {
                return nil
            }
        }
    }
