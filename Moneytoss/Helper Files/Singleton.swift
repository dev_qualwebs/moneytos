//
//  Singleton.swift
//  Diamonium
//
//  Created by Qualwebs on 04/01/2019.
//  Copyright © 2019 Qualwebs. All rights reserved.
//

import Foundation
import UIKit
import Toaster

class Singleton {
    
    static let shared = Singleton()
    var userDetail = getUser()
    var cardData = [GetCardResponse]()
    var paymentMethods = [PaymentMethodResponse]()
    var currencyData = [CurrencyResponse]()
    var walletData = [WalletResponse]()
    var transactionData = [TransactionResponse]()
    var contactData = [TransactionResponse]()
    var countryData = [Countires]()
    var sendDatauser = TransactionResponse()
    var sentRequestData = [MoneyRequest]()
    var receivedRequestData = [MoneyRequest]()
    var userDocument = UserDocumentResponse()
   
    
    public static func getUser() -> SignupResponse{
        if let info = UserDefaults.standard.data(forKey: UD_USER_DETAIL){
            let userData = info
            let userArray = try! JSONDecoder().decode(SignupResponse.self, from: userData)
            return userArray
        }else {
            return SignupResponse()
        }
    }
    
    func initialiseValues(){
        
    }
    
    func showToast(text: String?){
        if (ToastCenter.default.currentToast?.text == text) {
            return
        }
        if(UIScreen.main.bounds.height > 700){
            ToastView.appearance().bottomOffsetPortrait = 110
        }else {
            ToastView.appearance().bottomOffsetPortrait = 70
        }
        
        ToastView.appearance().textColor = .white
        ToastView.appearance().backgroundColor = UIColor(hue: 0, saturation: 0, brightness: 0, alpha: 0.7)
        ToastView.appearance().font = UIFont(name: "Avenir Next Medium", size: 17)
        
        Toast(text: text, delay: 0, duration: 2).show()
        Toast(text: text, delay: 0, duration: 2).cancel()
    }
}
