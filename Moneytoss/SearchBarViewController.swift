//
//  SearchBarViewController.swift
//  Moneytoss
//
//  Created by qw on 29/09/20.
//  Copyright © 2020 qw. All rights reserved.
//

import UIKit

class SearchBarViewController: UIViewController {
    //MARK: IBOutlets
    @IBOutlet weak var searchField: UITextField!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        self.searchField.becomeFirstResponder()
    }
    
    //MARK: IBAction
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func clearAction(_ sender: Any) {
        self.searchField.text = ""
    }
    
}
