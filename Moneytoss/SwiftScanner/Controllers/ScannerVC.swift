//
//  ScannerVC.swift
//  SwiftScanner
//
//  Created by Jason on 2018/11/30.
//  Copyright © 2018 Jason. All rights reserved.
//

import UIKit
import AVFoundation

protocol ScanQr{
    func getUserInfoFromScan(code: String?)
}

public class ScannerVC: UIViewController {
    //MARK: IBoutlets
    @IBOutlet weak var bottomView: View!
    @IBOutlet weak var topView: UIView!
//    @IBOutlet weak var codeView: View!
//    @IBOutlet weak var topupButton: CustomButton!
//    @IBOutlet weak var blackHeading: UILabel!
//    @IBOutlet weak var yellowHeading: UILabel!
//    @IBOutlet weak var qrField: DesignableUITextField!
    
    
    var isScanPay = false
    var scanDelegalte: ScanQr? = nil
    
    public lazy var headerViewController:HeaderVC = .init()
    
    public lazy var cameraViewController:CameraVC = .init()
    
    /// 动画样式
    public var animationStyle:ScanAnimationStyle = .default{
        didSet{
            cameraViewController.animationStyle = animationStyle
        }
    }
    
    // 扫描框颜色
    public var scannerColor:UIColor = .white{
        didSet{
            cameraViewController.scannerColor = scannerColor
        }
    }
    
    public var scannerTips:String = ""{
        didSet{
            cameraViewController.scanView.tips = scannerTips
        }
    }
    
    /// `AVCaptureMetadataOutput` metadata object types.
    public var metadata = AVMetadataObject.ObjectType.metadata {
        didSet{
            cameraViewController.metadata = metadata
        }
    }
    
    public var successBlock:((String)->())?
    
    public var errorBlock:((Error)->())?
    
    
    /// 设置标题
    public override var title: String?{
        
        didSet{
            
            if navigationController == nil {
                headerViewController.title = title
            }
        }
        
    }
    
    
    /// 设置Present模式时关闭按钮的图片
    public var closeImage: UIImage?{
        
        didSet{
            
            if navigationController == nil {
                headerViewController.closeImage = closeImage ?? UIImage()
            }
        }
        
    }
    
    override public func viewDidLoad() {
        super.viewDidLoad()
//        self.qrField.delegate = self
//        if(isScanPay){
//            self.bottomView.isHidden = true
//            self.yellowHeading.text = "PAY"
//            self.blackHeading.text = "SCAN N "
//        }
        setupUI()
        
    }
    
    
    public override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        cameraViewController.startCapturing()
        self.view.bringSubviewToFront(cameraViewController.view)
        self.view.bringSubviewToFront(topView)
       self.view.bringSubviewToFront(bottomView)
    }
    
    //MARK: IBAction
    @IBAction func scanAction(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "ScanDetailViewController") as! ScanDetailViewController
        self.navigationController?.pushViewController(myVC, animated: true)
      //  self.navigationController?.popViewController(animated: true)
        Singleton.shared.showToast(text: "Successfully scanned the document.")
    }
    
    
    @IBAction func backAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
       // self.navigationController?.popViewController(animated: true)
    }
}

// MARK: - CustomMethod
extension ScannerVC{
    
    func setupUI() {
        
        if title == nil {
            title = "Moneytos Scan"
        }
        
        view.backgroundColor = primaryColor.withAlphaComponent(0.2)
        
        headerViewController.delegate = self
        
        cameraViewController.metadata = metadata
        
        cameraViewController.animationStyle = animationStyle
        
        cameraViewController.delegate = self
        
        add(cameraViewController)
        
        if navigationController == nil {
           // add(headerViewController)
           // view.bringSubviewToFront(headerViewController.view)
        }
        
        
    }
    
    
    public func setupScanner(_ title:String? = nil, _ color:UIColor? = nil, _ style:ScanAnimationStyle? = nil, _ tips:String? = nil, _ success:@escaping ((String)->())){
        
        if title != nil {
            self.title = title
        }
        
        if color != nil {
            scannerColor = color!
        }
        
        if style != nil {
            animationStyle = style!
        }
        
        if tips != nil {
            scannerTips = tips!
        }
        
        successBlock = success
        
    }
}

// MARK: - HeaderViewControllerDelegate
extension ScannerVC:HeaderViewControllerDelegate{
    
    
    /// 点击关闭
    public func didClickedCloseButton() {
        dismiss(animated: true, completion: nil)
        
    }
    
}


extension ScannerVC:CameraViewControllerDelegate{
    func didOutput(_ code: String) {
        if (self.isValidEmail(emailStr: code)){
            if(code != Singleton.shared.userDetail.email){
             self.scanDelegalte?.getUserInfoFromScan(code: code)
             self.dismiss(animated: true, completion: nil)
                successBlock?(code)
            }
        }
    }
    
    func didReceiveError(_ error: Error) {
        errorBlock?(error)
    }
}

