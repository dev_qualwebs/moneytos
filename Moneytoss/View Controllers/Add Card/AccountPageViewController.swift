//
//  AccountPageViewController.swift
//  Moneytoss
//
//  Created by Sagar Pandit on 25/06/21.
//  Copyright © 2021 qw. All rights reserved.
//


import UIKit

var currentType = 1

class AccountPageViewController: UIPageViewController{
    
    var orderedViewControllers: [UIViewController] = []
    var pageControl = UIPageControl()
    static var dataSource:UIPageViewControllerDataSource?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.handleView()
        setControllers()
        self.dataSource = self
        self.delegate = self
        AccountPageViewController.dataSource = self
        for view in self.view.subviews {
                   if let subView = view as? UIScrollView {
                           subView.isScrollEnabled = true
                   }
               }
    }
    
    private func newColoredViewController(controller: String)->UIViewController {
           return UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: controller)
       }
    
    func handleView() {
        if(currentType == 1){
      orderedViewControllers = [self.newColoredViewController(controller: "PaymentMethodViewController"),
        self.newColoredViewController(controller: "StripeAccountViewController")]
        }else {
            orderedViewControllers = [self.newColoredViewController(controller: "SentRequestViewController"),
              self.newColoredViewController(controller: "ReceivedRequestViewController")]
        }
    }
    
    func setControllers() {
        if let firstViewController = orderedViewControllers.first {
            setViewControllers([firstViewController],
                               direction: .reverse,
                               animated: false,
                               completion: nil)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
    }
    
    override func viewDidAppear(_ animated: Bool) {
        AccountPageViewController.dataSource = self
    }
  
    func setControllerFirst() {
        if let firstViewController = orderedViewControllers.first {
            setViewControllers([firstViewController],
                               direction: .forward,
                               animated: false,
                               completion: nil)
        }
    }

    
    func setControllerSecond() {
        setViewControllers([orderedViewControllers[1]], direction: .forward, animated: false, completion: nil)
    }
  }

extension AccountPageViewController: UIPageViewControllerDataSource
{
    func pageViewController(_ pageViewController: UIPageViewController,
                            viewControllerBefore viewController: UIViewController) -> UIViewController? {
      //  currentControllerIndex(VC: viewController)
        guard let viewControllerIndex = orderedViewControllers.firstIndex(of: viewController) else {
            return nil
        }
        let previousIndex = viewControllerIndex - 1
        guard previousIndex >= 0 else {
            return nil
        }
        guard orderedViewControllers.count > previousIndex else {
            return nil
        }
        return orderedViewControllers[previousIndex]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController,
                            viewControllerAfter viewController: UIViewController) -> UIViewController? {
        //currentControllerIndex(VC: viewController)
        guard let viewControllerIndex = orderedViewControllers.firstIndex(of: viewController) else {
            return nil
        }
        let nextIndex = viewControllerIndex + 1
        guard orderedViewControllers.count != nextIndex else {
            return nil
        }
        guard orderedViewControllers.count > nextIndex else {
            return nil
        }
        return orderedViewControllers[nextIndex]
    }
    
}

extension AccountPageViewController : UIPageViewControllerDelegate {
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        let pageContentViewController = pageViewController.viewControllers![0]
            self.pageControl.currentPage = orderedViewControllers.firstIndex(of: pageContentViewController)!
    }
}
