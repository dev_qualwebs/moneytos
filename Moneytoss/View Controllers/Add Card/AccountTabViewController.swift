//
//  AccountTabViewController.swift
//  Moneytoss
//
//  Created by Sagar Pandit on 25/06/21.
//  Copyright © 2021 qw. All rights reserved.
//

import UIKit

class AccountTabViewController: UIViewController, SelectDate {
    func selectedDate(date: Int) {
        switch timeType {
        case 1:
            self.startDate.text = self.convertTimestampToDate(date, to: "yyyy/MM/dd")
            break
        case 2:
            self.endDate.text = self.convertTimestampToDate(date, to: "yyyy/MM/dd")
            break
        default:
            break
        }
    }
    
    //MARK: IBOutlets
    @IBOutlet weak var viewPosted: UIView!
    @IBOutlet weak var viewReceived: UIView!
    @IBOutlet weak var labelPosted: DesignableUILabel!
    @IBOutlet weak var labelReceived: DesignableUILabel!
    @IBOutlet weak var viewForPopup: UIView!
    @IBOutlet weak var dateStackView: UIStackView!
    @IBOutlet weak var selectDate: CustomButton!
    @IBOutlet weak var monthlyButton: CustomButton!
    @IBOutlet weak var weeklyButton: CustomButton!
    @IBOutlet weak var endDate: UITextField!
    @IBOutlet weak var startDate: UITextField!
    @IBOutlet weak var debitAmount: DesignableUILabel!
    @IBOutlet weak var creditAmount: DesignableUILabel!
    
    @IBOutlet weak var hdeadingLabel: DesignableUILabel!
    
    

    var timeType = Int()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.postedAction(self)
        if(currentType == 2){
            self.labelPosted.text = "SENT"
            self.labelReceived.text = "RECEIVED"
        }
    }

    //MARK: IBActions
    @IBAction func backAction(_ sender: Any){
        self.navigationController?.popViewController(animated: true)
    }
   
    
    @IBAction func receivedAction(_ sender: Any) {
        let pageViewController = AccountPageViewController.dataSource as! AccountPageViewController
        self.labelPosted.textColor = .lightGray
        self.labelReceived.textColor = .black
        self.viewPosted.isHidden = true
        self.viewReceived.isHidden = false
        pageViewController.setControllerSecond()
    }
    
    @IBAction func postedAction(_ sender: Any) {
        let pageViewController = AccountPageViewController.dataSource as! AccountPageViewController
        self.labelPosted.textColor = .black
        self.labelReceived.textColor = .lightGray
        self.viewPosted.isHidden = false
        self.viewReceived.isHidden = true
        pageViewController.setControllerFirst()
    }
    
    
}

