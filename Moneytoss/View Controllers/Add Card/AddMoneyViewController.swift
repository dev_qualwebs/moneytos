//
//  AddCardViewController.swift
//  Moneytoss
//
//  Created by qw on 23/08/20.
//  Copyright © 2020 qw. All rights reserved.
//

import UIKit
import TransitionButton

class AddMoneyViewController: UIViewController {
    //MARK: IBOutlets
    @IBOutlet weak var confirmButton: TransitionButton!
    @IBOutlet weak var cardCollection: UICollectionView!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var amountField: UITextField!
    @IBOutlet weak var firstSelectionView: View!
    @IBOutlet weak var secondSelectionView: View!
    @IBOutlet weak var thirdSelectionView: View!
    @IBOutlet weak var currentBalance: DesignableUILabel!
    
    @IBOutlet weak var headingLabel: DesignableUILabel!
    @IBOutlet weak var amountToWithdrawLabel: DesignableUILabel!
    @IBOutlet weak var quickAmountView: View!
    @IBOutlet weak var selectPaymentMethodLabel: DesignableUILabel!
    
    
    var amount = String()
    var cardData = [GetCardResponse]()
    var selectedCard = Int()
    var isWithdrawMoney = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.amountField.delegate = self
        self.confirmButton.layer.cornerRadius = 20
        self.amountField.becomeFirstResponder()
        if(self.isWithdrawMoney){
            self.headingLabel.text = "Withdraw Money"
            self.amountToWithdrawLabel.text = "Enter amount to withdraw"
            self.quickAmountView.isHidden = true
            self.selectPaymentMethodLabel.text = "Select a card for withdrawal amount"
            self.confirmButton.setTitle("Withdraw Money", for: .normal)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        NavigationController.shared.getCardData { (data) in
            self.cardData = data
            self.cardCollection.reloadData()
        }
        
        NavigationController.shared.getWalletData { (data) in
            if(data.count > 0){
                self.currentBalance.text = "Current Balance: " +  "\(data[0].symbol ?? "")\(data[0].balance ?? "")"
            }else {
                self.currentBalance.text = "Current Balance: " +  "$0.00"
            }
        }
    }
    
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if(scrollView == cardCollection){
            for cell in cardCollection.visibleCells {
                let indexPath = cardCollection.indexPath(for: cell)
                if(indexPath!.row == 0 || indexPath!.row == 1){
                    self.pageControl.currentPage = 0
                }else {
                    self.pageControl.currentPage = 1
                }
            }
        }
    }
    
    //MARK: IBActions
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func addCardAction(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "AddCardViewController") as! AddCardViewController
        self.navigationController?.pushViewController(myVC, animated: true)
    }
    
    @IBAction func addMoneyAction(_ sender: Any) {
        self.amountField.resignFirstResponder()
        if(self.amount == ""){
            Singleton.shared.showToast(text: "Please enter Amount.")
        }else if(self.selectedCard == 0){
            Singleton.shared.showToast(text: "Please select card.")
        }else {
            let param: [String:Any] = [
                "payment_method":self.selectedCard,
                "amount":self.amount
            ]
            var url = String()
            
            if(self.isWithdrawMoney){
                let myVC = self.storyboard?.instantiateViewController(withIdentifier: "PincodeViewController") as! PincodeViewController
                myVC.param = param
                myVC.selectedMethod = 4
                self.navigationController?.pushViewController(myVC, animated: true)
                return
            }else {
                let myVC = self.storyboard?.instantiateViewController(withIdentifier: "PincodeViewController") as! PincodeViewController
                myVC.param = param
                myVC.selectedMethod = 5
                self.navigationController?.pushViewController(myVC, animated: true)
            }
        }
    }
    
    @IBAction func firstSelectionAction(_ sender: Any) {
        self.amountField.resignFirstResponder()
        self.firstSelectionView.backgroundColor = primaryColor
        self.secondSelectionView.backgroundColor = .lightGray
        self.thirdSelectionView.backgroundColor = .lightGray
        self.amount = "100"
        self.amountField.text = "$100"
    }
    
    @IBAction func secondSelectionAction(_ sender: Any) {
        self.amountField.resignFirstResponder()
        self.secondSelectionView.backgroundColor = primaryColor
        self.firstSelectionView.backgroundColor = .lightGray
        self.thirdSelectionView.backgroundColor = .lightGray
        self.amount = "300"
        self.amountField.text = "$300"
    }
    
    @IBAction func thirdSelectionAction(_ sender: Any) {
        self.amountField.resignFirstResponder()
        self.thirdSelectionView.backgroundColor = primaryColor
        self.secondSelectionView.backgroundColor = .lightGray
        self.firstSelectionView.backgroundColor = .lightGray
        self.amount = "500"
        self.amountField.text = "$500"
    }
    
}

extension AddMoneyViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UITextFieldDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if(self.cardData.count == 0){
            self.pageControl.numberOfPages = 0
            self.pageControl.alpha = 0
           return 1
        }else {
            self.pageControl.alpha = 1
            if(self.cardData.count <= 2){
                self.pageControl.numberOfPages = 1
            }else {
                self.pageControl.numberOfPages = 2
            }
            return self.cardData.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CardCollectionCell", for: indexPath) as! CardCollectionCell
        if(self.cardData.count == 0){
            cell.view1.isHidden = true
            cell.view3.isHidden = false
            cell.addCard = {
                let myVC = self.storyboard?.instantiateViewController(withIdentifier: "AddCardViewController") as! AddCardViewController
                self.navigationController?.pushViewController(myVC, animated: true)
            }
        }else {
            cell.view3.isHidden = true
            cell.view1.isHidden = false
        let val = self.cardData[indexPath.row]
        if(self.selectedCard == val.id){
            cell.view1.borderColor = lightBlueColor
            cell.view1.borderWidth = 5
        }else {
            cell.view1.borderColor = .clear
            cell.view1.borderWidth = 0
        }
        cell.expiryDate.text = "\(val.expiry_month ?? "")/\(val.expiry_year ?? "")"
        cell.cardNumber.text = "\(val.masked_card_number?.dropFirst(8) ?? "")"
        cell.cardholderName.text = val.name_on_card
       
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width/2, height: 180)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.selectedCard = self.cardData[indexPath.row].id ?? 0
        self.cardCollection.reloadData()
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.thirdSelectionView.backgroundColor = .lightGray
        self.secondSelectionView.backgroundColor = .lightGray
        self.firstSelectionView.backgroundColor = .lightGray
        self.amount = ""
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.amount = self.amountField.text ?? ""
    }
}
