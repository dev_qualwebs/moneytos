//
//  MoneyRequestViewController.swift
//  Moneytoss
//
//  Created by Sagar Pandit on 30/08/21.
//  Copyright © 2021 qw. All rights reserved.
//

import UIKit

class SentRequestViewController: UIViewController {
    //MARK: IBOUtlets
    @IBOutlet weak var requestTable: ContentSizedTableView!
    @IBOutlet weak var noRequestLabel: UIStackView!
    
    var transactionData = [MoneyRequest]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if(self.transactionData.count == 0){
        self.getRequestData()
        }
    }
    
    
    func getRequestData(){
       // if(Singleton.shared.sentRequestData.count == 0){
            SessionManager.shared.methodForApiCalling(url: U_BASE + U_GET_MONEY_REQUEST + "1", method: .get, parameter: nil, objectClass: GetMoneyRequest.self, requestCode: U_GET_MONEY_REQUEST) { (response) in
                self.transactionData = response.response
                Singleton.shared.sentRequestData = response.response
                self.requestTable.reloadData()
            }
//        }else {
//            self.transactionData =  Singleton.shared.sentRequestData
//            self.requestTable.reloadData()
//        }
       
    }
}

extension SentRequestViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print(UIScreen.main.bounds.height)
        if(self.transactionData.count == 0){
            self.noRequestLabel.isHidden = false
        }else {
            self.noRequestLabel.isHidden = true
        }
      
        return self.transactionData.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TransactionTableCell") as! TransactionTableCell
        let val = self.transactionData[indexPath.row]
        if(val.user_id == Singleton.shared.userDetail.id){
            if((val.user_details?.profile_image ?? "").contains("http")){
                cell.userImage.sd_setImage(with: URL(string: self.getImageURL(text:val.user_details?.profile_image ?? "")), placeholderImage: #imageLiteral(resourceName: "round_image"))
            }else {
                cell.userImage.sd_setImage(with: URL(string: self.getImageURL(text:U_IMAGE_BASE + (val.user_details?.profile_image ?? ""))), placeholderImage: #imageLiteral(resourceName: "round_image"))
            }
            cell.userName.text = "\(val.user_details?.first_name  ?? "")"
                        
        }
        
        cell.transactionAmount.textColor = greenColor
        cell.transactionAmount.text = "+$" + (val.amount ?? "")
        
        if(val.status == 0){
            cell.transactionType.text = "Pending"
        }else if(val.status == 0){
            cell.transactionType.text = "Approved"
        }else {
            cell.transactionType.text = "Rejected"
        }
        cell.transactionDate.text = "On " + self.convertTimestampToDate(val.created_at ?? 0, to: "MMM dd, yyyy")
        return cell
    }

    
}
