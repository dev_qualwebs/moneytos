//
//  PaymentMethodViewController.swift
//  Moneytoss
//
//  Created by Sagar Pandit on 25/06/21.
//  Copyright © 2021 qw. All rights reserved.
//


import UIKit
import SDWebImage

protocol SelecrPaymentCard {
    func getSelectCardDetail(data: GetCardResponse)
}


class PaymentMethodViewController: UIViewController {
    //MARK: IBOutlets
    @IBOutlet weak var cardTable: UITableView!
    @IBOutlet weak var noCardsLabel: DesignableUILabel!
    
    var isSelectCard = false
    var cardDelegate: SelecrPaymentCard? = nil
    var cardData = [GetCardResponse]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
     
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
         NavigationController.shared.getCardData(completionHandler: { (data) in
             self.cardData = data
             self.cardTable.reloadData()
         })
    }
  
    func showPopup(title: String, msg: String, id: Int) {
            let alert = UIAlertController(title: title, message: msg, preferredStyle: .alert)
            let yesBtn = UIAlertAction(title:"Yes", style: .default) { (UIAlertAction) in
               ActivityIndicator.show(view: self.view)
                let param:[String:Any] = [
                    "payment_gateway": 1,
                    "id": id
                ]
                SessionManager.shared.methodForApiCalling(url: U_BASE + U_DELETE_CARD, method:.post, parameter: param, objectClass: SuccessResponse.self, requestCode: U_DELETE_CARD) { (response) in
                    Singleton.shared.cardData = []
                    NavigationController.shared.getCardData(completionHandler: { (data) in
                        self.cardData = data
                        self.cardTable.reloadData()
                    })
               }

            }
            let noBtn = UIAlertAction(title: "No", style: .default){
                (UIAlertAction) in
                self.dismiss(animated: true, completion: nil)
            }
            alert.addAction(yesBtn)
            alert.addAction(noBtn)
            
            present(alert, animated: true, completion: nil)
        }

    
    //MARK: IBActions
    @IBAction func addCardAction(_ sender: Any) {
        let dVC = storyboard?.instantiateViewController(withIdentifier: "AddCardViewController") as! AddCardViewController
       self.navigationController?.pushViewController(dVC, animated: true)
    }
}

extension PaymentMethodViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(self.cardData.count == 0){
            self.noCardsLabel.isHidden = false
        }else {
            self.noCardsLabel.isHidden = true
        }
        return self.cardData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CardViewCell") as! CardViewCell
        let val = self.cardData[indexPath.row]
        cell.expiryDate.text = "\(val.expiry_month ?? "")/\(val.expiry_year ?? "")"
        cell.cardNumber.text = "\(val.masked_card_number?.dropFirst(8) ?? "")"
        cell.imageName.text = val.name_on_card
        cell.deleteButton = {
            self.showPopup(title: "Delete Card", msg: "Are you sure?", id: val.id ?? 0)
        }
        
        return cell
    }
}

class CardViewCell: UITableViewCell {
    //MARK: IBOutlets
    @IBOutlet weak var imageName: DesignableUILabel!
    @IBOutlet weak var delete: UIButton!
    @IBOutlet weak var expiryDate: DesignableUILabel!
    @IBOutlet weak var cardNumber: DesignableUILabel!
    
    
    var deleteButton:(()-> Void)? = nil
    
    @IBAction func deleteAction(_ sender: Any) {
        if let deleteButton = self.deleteButton {
            deleteButton()
        }
    }
}
