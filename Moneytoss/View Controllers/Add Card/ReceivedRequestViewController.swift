//
//  ReceivedRequestViewController.swift
//  Moneytoss
//
//  Created by Sagar Pandit on 30/08/21.
//  Copyright © 2021 qw. All rights reserved.
//

import UIKit

class ReceivedRequestViewController: UIViewController, SelectedCard {
    func selectedCardDetail(type: Int, card: GetCardResponse) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "PincodeViewController") as! PincodeViewController
        if(selectedMethod == 1){
            myVC.param = [
                "amount": self.selectedRequest.amount,
                "user_id": self.selectedRequest.user_id,
                "send_from": type,
                "card_id": card.id,
            ]
        }else if(self.selectedMethod == 4){
            myVC.param = [
                "amount": self.selectedRequest.amount,
                "user_id": self.selectedRequest.user_id,
                "payment_gateway": 1,
                "send_from":type,
                "card_id": card.id,
            ]
        }
        if let user = self.selectedRequest.user_details{
            myVC.userDetail = user
        }
        myVC.selectedMethod = self.selectedMethod
        myVC.transferRequestId = self.selectedRequest.id
        if(selectedMethod == 1 || selectedMethod == 4 || selectedMethod == 2){
            
            self.navigationController?.pushViewController(myVC, animated: true)
        }
        
    }
    
    //MARK: IBOUtlets
    @IBOutlet weak var requestTable: ContentSizedTableView!
    @IBOutlet weak var noRequestLabel: UIStackView!
    
    var transactionData = [MoneyRequest]()
    var selectedMethod = 1
    var selectedRequest = MoneyRequest()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if(self.transactionData.count == 0){
           self.getRequestData()
        }
    }
    
    func getRequestData(){
      //  if(Singleton.shared.receivedRequestData.count == 0){
        ActivityIndicator.show(view: self.view)
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_GET_MONEY_REQUEST + "2", method: .get, parameter: nil, objectClass: GetMoneyRequest.self, requestCode: U_GET_MONEY_REQUEST) { (response) in
            for val in response.response{
                if(val.status == 0){
                    self.transactionData.append(val)
                   // Singleton.shared.receivedRequestData = response.response
                }
            }
            self.requestTable.reloadData()
            ActivityIndicator.hide()
        }
//        }else {
//            self.transactionData = Singleton.shared.receivedRequestData
//            self.requestTable.reloadData()
//        }
    }

}

extension ReceivedRequestViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print(UIScreen.main.bounds.height)
        if(self.transactionData.count == 0){
            self.noRequestLabel.isHidden = false
        }else {
            self.noRequestLabel.isHidden = true
        }
        
        return self.transactionData.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TransactionTableCell") as! TransactionTableCell
        let val = self.transactionData[indexPath.row]
            if((val.user_details?.profile_image ?? "").contains("http")){
                cell.userImage.sd_setImage(with: URL(string: self.getImageURL(text:val.user_details?.profile_image ?? "")), placeholderImage: #imageLiteral(resourceName: "round_image"))
            }else {
                cell.userImage.sd_setImage(with: URL(string: self.getImageURL(text:U_IMAGE_BASE + (val.user_details?.profile_image ?? ""))), placeholderImage: #imageLiteral(resourceName: "round_image"))
            }
            cell.userName.text = "\(val.user_details?.first_name  ?? "")"
        cell.rejectButton = {
            self.callAcceptRejectApi(type: 2, requestId: val.id ?? 0)
        }
        
        cell.sendButton = {
            if(val.user_id == nil){
                Singleton.shared.showToast(text: "No user found")
            }else {
                self.selectedRequest = val
                let myVC = self.storyboard?.instantiateViewController(withIdentifier: "SelectCardViewController") as! SelectCardViewController
                myVC.modalPresentationStyle = .overFullScreen
                myVC.cardDelegate = self
                myVC.isWalletSelected = true
                self.navigationController?.present(myVC, animated: true, completion: nil)
            }
        }
        
        cell.transactionDate.text = "$\(val.amount ?? "")"
        return cell
    }
    
    func callAcceptRejectApi(type: Int?, requestId: Int){
        let param:[String:Any] = [
            "requested_id":requestId,
            "status": type
        ]
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_UPDATE_MONEY_REQUEST, method: .post, parameter: param, objectClass: SuccessResponse.self, requestCode: U_UPDATE_MONEY_REQUEST) { (response) in
           Singleton.shared.showToast(text: "Tansferred rejected.")
            self.getRequestData()
        }
    }

}
