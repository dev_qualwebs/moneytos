//
//  StripeAccountViewController.swift
//  Moneytoss
//
//  Created by Sagar Pandit on 25/06/21.
//  Copyright © 2021 qw. All rights reserved.
//



import UIKit
import CoreLocation
import GooglePlaces

class StripeAccountViewController: UIViewController,UITextFieldDelegate , SelectDate {
    func selectedDate(date: Int) {
        self.selectedDate = date
        self.dobField.textInput?.text = self.convertTimestampToDate(date, to: "dd-MM-yy")
    }
    //MARK:IBOutlets
    @IBOutlet weak var accountNumber: FloatingPlaceholderTextField!
    @IBOutlet weak var routingNumber: FloatingPlaceholderTextField!
    @IBOutlet weak var address1: FloatingPlaceholderTextField!
    @IBOutlet weak var address2: FloatingPlaceholderTextField!
    @IBOutlet weak var postalCode: FloatingPlaceholderTextField!
    @IBOutlet weak var city: FloatingPlaceholderTextField!
    @IBOutlet weak var country: FloatingPlaceholderTextField!
    @IBOutlet weak var state: FloatingPlaceholderTextField!
    @IBOutlet weak var editButton: CustomButton!
    @IBOutlet weak var dobField: FloatingPlaceholderTextField!
    @IBOutlet weak var bankDetailView: UIStackView!
    
    
    var bankDetail = BankDetail()
    var latitude = CLLocationDegrees()
    var longitude = CLLocationDegrees()
    var selectedDate = Int()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.accountNumber.textInput?.delegate = self
        self.routingNumber.textInput?.delegate = self
        self.getAccountDetail()
        
    }
    
    func getAccountDetail(){
        let param: [String:Any] = [
            "type":2,
            "user_id": Singleton.shared.userDetail.id ?? 0,
        ]
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_RETREIVE_BANK_ACCOUNT, method: .post, parameter: param, objectClass: GetBankDetail.self, requestCode: U_RETREIVE_BANK_ACCOUNT) { (response) in
            if(response.response.count > 0){
                self.bankDetail = response.response[0]
                self.accountNumber.textInput?.text = ""
                self.routingNumber.textInput?.text = self.bankDetail.external_accounts.data[0].routing_number
                self.address1.textInput?.text = self.bankDetail.company.address.line1
                self.address2.textInput?.text = self.bankDetail.company.address.line2
                self.postalCode.textInput?.text = self.bankDetail.company.address.postal_code
                self.city.textInput?.text = self.bankDetail.company.address.city
                self.country.textInput?.text = self.bankDetail.company.address.country
                self.state.textInput?.text = self.bankDetail.company.address.state
                self.bankDetailView.isUserInteractionEnabled = false
                self.editButton.setTitle("Edit Details", for: .normal)
            }else {
                self.bankDetail = BankDetail()
                self.accountNumber.textInput?.text = ""
                self.routingNumber.textInput?.text = ""
                self.address1.textInput?.text = ""
                self.address2.textInput?.text = ""
                self.postalCode.textInput?.text = ""
                self.city.textInput?.text = ""
                self.country.textInput?.text = ""
                self.state.textInput?.text = ""
                self.dobField.textInput?.text = ""
                Singleton.shared.showToast(text: "No Bank Detail Found")
                self.bankDetailView.isUserInteractionEnabled = true
                self.editButton.setTitle("Submit Details", for: .normal)
            }
        }
    }
    
    func latLong(lat: Double,long: Double)  {
        
        let geoCoder = CLGeocoder()
        let location = CLLocation(latitude: lat , longitude: long)
        geoCoder.reverseGeocodeLocation(location, completionHandler: { (placemarks, error) -> Void in
            var placeMark: CLPlacemark!
            placeMark = placemarks?[0]
            
            // Country
            if let country = placeMark.addressDictionary!["Country"] as? String {
                self.country.textInput?.text = country
                if let city = placeMark.addressDictionary!["City"] as? String {
                    self.city.textInput?.text = city
                    
                    // State
                    if let state = placeMark.addressDictionary!["State"] as? String{
                        self.state.textInput?.text = state
                        
                        if let street = placeMark.addressDictionary!["Street"] as? String{
                            print("Street :- \(street)")
                            let str = street
                            let streetNumber = str.components(
                                separatedBy: NSCharacterSet.decimalDigits.inverted).joined(separator: "")
                            print("streetNumber :- \(streetNumber)" as Any)
                            
                            // ZIP
                            if let zip = placeMark.addressDictionary!["ZIP"] as? String{
                                self.postalCode.textInput?.text = zip.replacingOccurrences(of: " ", with: "")
                                // Location name
                                if let locationName = placeMark?.addressDictionary?["Name"] as? String {
                                    print("Location Name :- \(locationName)")
                                    // Street address
                                    if let thoroughfare = placeMark?.addressDictionary!["Thoroughfare"] as? NSString {
                                        self.address1.textInput?.text = "\(thoroughfare)"
                                    }
                                }
                            }
                        }
                    }
                }
            }
        })
    }
    
    //MARK:IBActions
    @IBAction func closeAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func addressButtonAction(_ sender: Any) {
        let autocompleteController = GMSAutocompleteViewController()
        autocompleteController.delegate = self
        
        // Specify the place data types to return.
        let fields: GMSPlaceField = GMSPlaceField(rawValue:UInt(GMSPlaceField.name.rawValue) |
                                                    UInt(GMSPlaceField.placeID.rawValue) |
                                                    UInt(GMSPlaceField.coordinate.rawValue) |
                                                    GMSPlaceField.addressComponents.rawValue |
                                                    GMSPlaceField.formattedAddress.rawValue)
        // autocompleteController.placeFields = .formattedAddress
        
        // Specify a filter.
        let filter = GMSAutocompleteFilter()
        filter.type = .address
                
        autocompleteController.autocompleteFilter = filter
        
        // Display the autocomplete view controller.
        autocompleteController.modalPresentationStyle = .overFullScreen
        present(autocompleteController, animated: true, completion: nil)
    }
   
    @IBAction func submitAction(_ sender: Any) {
        if((self.accountNumber.textInput?.text ?? "")!.isEmpty){
                Singleton.shared.showToast(text: "Enter Account Number")
        }else if((self.routingNumber.textInput?.text ?? "")!.isEmpty){
                Singleton.shared.showToast(text: "Enter Routing Number")
        }else if((self.address1.textInput?.text ?? "")!.isEmpty){
                Singleton.shared.showToast(text: "Enter Address 1")
        }else if((self.address2.textInput?.text ?? "")!.isEmpty){
                Singleton.shared.showToast(text: "Enter Address 2")
        }else if((self.postalCode.textInput?.text ?? "")!.isEmpty){
                Singleton.shared.showToast(text: "Enter Postal Code")
        }else if((self.city.textInput?.text ?? "")!.isEmpty){
                Singleton.shared.showToast(text: "Enter City")
        }else if((self.state.textInput?.text ?? "")!.isEmpty){
                Singleton.shared.showToast(text: "Enter State")
        }else if((self.country.textInput?.text ?? "")!.isEmpty){
                Singleton.shared.showToast(text: "Enter Country")
        }else if((self.dobField.textInput?.text ?? "")!.isEmpty){
                Singleton.shared.showToast(text: "Select Date Of Birth")
            }else {
                ActivityIndicator.show(view: self.view)
                var number = self.accountNumber.textInput?.text?.replacingOccurrences(of: ":", with: "")
                number = number?.replacingOccurrences(of: " ", with: "")
                
                let param:[String:Any] = [
                    "account_number" : number ?? "",
                    "routing_number" : self.routingNumber.textInput?.text ?? "",
                    "city" : self.city.textInput?.text ?? "",
                    "country" : self.country.textInput?.text ?? "",
                    "line1" : self.address1.textInput?.text ?? "",
                    "line2": self.address2.textInput?.text ?? "",
                    "postal_code": self.postalCode.textInput?.text ?? "",
                    "state" : self.state.textInput?.text ?? "",
                    "date_of_birth" : self.selectedDate,
                    "type":1,
                    "is_default":1,
                    "user_id":Singleton.shared.userDetail.id
                ]
                var url = String()
                if(self.editButton.titleLabel?.text == "Submit Details"){
                    url = U_BASE + U_REGISTER_BANK_ACCOUNT
                }else {
                    url =  U_BASE + U_UPDATE_BANK_ACCOUNT
                }
                SessionManager.shared.methodForApiCalling(url: url, method: .post, parameter: param, objectClass: SuccessResponse.self, requestCode: U_UPDATE_BANK_ACCOUNT) { (response) in
                    self.editButton.setTitle("Edit Details", for: .normal)
                    Singleton.shared.showToast(text: "Bank detail updated successfully")
                    self.bankDetailView.isUserInteractionEnabled = false
                    ActivityIndicator.hide()
                }
            }
        
    }
    
    @IBAction func dobAction(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "DatePickerViewController") as! DatePickerViewController
        myVC.dateDelegate = self
        myVC.modalPresentationStyle = .overFullScreen
        self.navigationController?.present(myVC, animated: true, completion: nil)
    }
    
}

extension StripeAccountViewController: GMSAutocompleteViewControllerDelegate {
    // Handle the user's selection.
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        //  self.address.text = place.formattedAddress
        self.latitude = place.coordinate.latitude
        self.longitude = place.coordinate.longitude
        self.latLong(lat: Double(self.latitude), long:Double(self.longitude))
        dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
}
