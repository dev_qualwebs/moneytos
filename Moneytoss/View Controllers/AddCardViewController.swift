 //
//  AddCardViewController.swift
//  Moneytoss
//
//  Created by qw on 15/08/20.
//  Copyright © 2020 qw. All rights reserved.
//

import UIKit

class AddCardViewController: UIViewController, UITextFieldDelegate {
    //MARK: IBOutlets
    @IBOutlet weak var cardName: FloatingPlaceholderTextField!
    @IBOutlet weak var cardNumber: FloatingPlaceholderTextField!
    @IBOutlet weak var cvvField: FloatingPlaceholderTextField!
    @IBOutlet weak var monthField: FloatingPlaceholderTextField!
    @IBOutlet weak var yearField: FloatingPlaceholderTextField!
    @IBOutlet weak var rememberView: View!
    
    var setDefaultCard = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initView()
    }
    
    func initView(){
        self.cardNumber.textInput?.delegate = self
        self.monthField.textInput?.delegate = self
        self.yearField.textInput?.delegate = self
        self.cvvField.textInput?.delegate = self
    }
    
    //MARK: IBAction
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func rememberAction(_ sender: Any) {
        if(self.rememberView.backgroundColor == .white){
           self.rememberView.backgroundColor = primaryColor
           self.rememberView.borderColor = primaryColor
        self.setDefaultCard = 1
        }else{
        self.rememberView.backgroundColor = .white
        self.rememberView.borderColor = primaryColor
            self.setDefaultCard = 0
        }
    }
    
    @IBAction func addCardAction(_ sender: Any) {
        if((cardNumber.textInput!.text ?? "")!.isEmpty){
            Singleton.shared.showToast(text: "Enter card number")
        }else if(cardName.textInput!.text!.isEmpty){
            Singleton.shared.showToast(text: "Enter card name")
        }else if((monthField.textInput?.text ?? "")!.isEmpty){
            Singleton.shared.showToast(text: "Enter card expiry month")
        }else if((yearField.textInput?.text ?? "")!.isEmpty){
            Singleton.shared.showToast(text: "Enter card expiry year")
        }else if((cvvField.textInput?.text ?? "")!.isEmpty){
            Singleton.shared.showToast(text: "Enter cvv number")
        }else {
            ActivityIndicator.show(view: self.view)
            let card = self.cardNumber.textInput?.text?.replacingOccurrences(of: " ", with: "")
            let param = [
                "card_name":self.cardName.textInput?.text,
                "expiry_month":self.monthField.textInput?.text,
                "expiry_year":self.yearField.textInput?.text,
                "card_number":card,
                "cvc":self.cvvField.textInput?.text,
               "payment_gateway":1,
                "is_default":self.setDefaultCard,
                ] as! [String:Any]
            
            SessionManager.shared.methodForApiCalling(url: U_BASE + U_ADD_CARD, method: .post, parameter: param, objectClass: SuccessResponse.self, requestCode: U_ADD_CARD) { (response) in
                Singleton.shared.showToast(text: "Successfully added card")
                Singleton.shared.cardData = []
                ActivityIndicator.hide()
                let myVC = self.storyboard?.instantiateViewController(withIdentifier: "TabbarViewController") as! TabbarViewController
                self.navigationController?.pushViewController(myVC, animated: true)
            }
        }
    }
    
}

