//
//  ContactViewController.swift
//  Moneytoss
//
//  Created by qw on 15/08/20.
//  Copyright © 2020 qw. All rights reserved.
//

import UIKit

class ContactViewController: UIViewController, UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
         
        self.searchText = (textField.text ?? "") + string
        self.searchData = []
        self.searchData = self.contactData.filter{
            ($0.sender_name ?? "").lowercased().contains(self.searchText.lowercased()) || ($0.receiver_name ?? "").lowercased().contains(self.searchText.lowercased())
        }
        if(self.searchText == ""){
            self.searchData = []
        }
        self.contactTable.reloadData()
        return true
        
    }
    
    
    //MARK: IBOutlets
    @IBOutlet weak var searchField: UITextField!
    @IBOutlet weak var searchIcon: UIImageView!
    @IBOutlet weak var searchButton: UIButton!
    @IBOutlet weak var contactTable: ContentSizedTableView!
    @IBOutlet weak var userImage: ImageView!
    @IBOutlet weak var scanImage: UIImageView!
    @IBOutlet weak var scanHeight: NSLayoutConstraint!
    @IBOutlet weak var referFriendButton: UIButton!
    @IBOutlet weak var noContactLabel: UIStackView!
    
    
    var contactData = [TransactionResponse]()
    var searchData = [TransactionResponse]()
    var amount = String()
    var isRequestMoney = false
    var searchText = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.searchField.delegate = self
        searchIcon.image = #imageLiteral(resourceName: "mysearch")
        if(self.isRequestMoney){
            self.scanImage.image = #imageLiteral(resourceName: "icon_back")
            self.scanHeight.constant  = 15
            self.referFriendButton.isHidden = true
        }else {
            self.scanImage.image = #imageLiteral(resourceName: "scan")
            self.referFriendButton.isHidden = false
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        NavigationController.shared.getContactData { (data) in
            self.contactData = data
            self.contactTable.reloadData()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        let profileData = Singleton.shared.userDetail
        if((profileData.profile_image ?? "").contains("http")){
            self.userImage.sd_setImage(with: URL(string: self.getImageURL(text:profileData.profile_image ?? "")), placeholderImage: #imageLiteral(resourceName: "round_image"))
        }else {
            self.userImage.sd_setImage(with: URL(string: self.getImageURL(text:U_IMAGE_BASE + (profileData.profile_image ?? ""))), placeholderImage: #imageLiteral(resourceName: "round_image"))
        }

    }

    
    //MARK: IBActions
     @IBAction func profileAction(_ sender: Any) {
         let myVC = self.storyboard?.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
         self.navigationController?.pushViewController(myVC, animated: true)
    }
    
    @IBAction func referFriendAction(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "ReferFriendViewController") as! ReferFriendViewController
        self.navigationController?.pushViewController(myVC, animated: true)
    }

    @IBAction func notificationAction(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "NotificationViewController") as! NotificationViewController
        self.navigationController?.pushViewController(myVC, animated: true)
    }
    

   
    @IBAction func searchAction(_ sender: Any) {
//        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "SearchBarViewController") as! SearchBarViewController
//        self.navigationController?.pushViewController(myVC, animated: true)
        if(searchIcon.image == #imageLiteral(resourceName: "mysearch")){
          searchIcon.image = #imageLiteral(resourceName: "clear")
            self.searchField.becomeFirstResponder()
            self.searchData = []
            self.searchField.text = ""
            self.searchText = ""
            self.contactTable.reloadData()
            self.searchField.isHidden = false
        }else {
          searchIcon.image = #imageLiteral(resourceName: "mysearch")
            self.searchField.text = ""
            self.searchText = ""
            self.searchData = []
            self.contactTable.reloadData()
            self.searchField.resignFirstResponder()
            self.searchField.isHidden = true
        }
    }
    
    @IBAction func qrScanAction(_ sender: Any) {
        if(self.isRequestMoney){
            self.navigationController?.popViewController(animated: true)
            return
        }
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "ScannerVC") as! ScannerVC
        myVC.scanDelegalte = self
        self.present(myVC, animated: true, completion: nil)
    }
    
    func getUserInfoFromScan(code: String?) {
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_SEARCH_USER_VIA_EMAIL, method: .post, parameter: ["email":code], objectClass: GetUserInfoScan.self, requestCode: U_SEARCH_USER_VIA_EMAIL) { response in
            NotificationCenter.default.post(name: NSNotification.Name("change_tab"), object: nil,userInfo: ["tab":"2", "userData": response.response])
            Singleton.shared.sendDatauser = TransactionResponse(id: response.response.id, transaction_type: nil, amount: nil, local_transaction_id: nil, user_id: nil, currency_id: nil, created_at: nil, notes: nil, transaction_method: nil, sender_name: (response.response.first_name ?? "") + " " + (response.response.last_name ?? ""), sender_id: response.response.id, receiver_name: nil, sender_profile_image: response.response.profile_image, receiver_profile_image: nil, receiver_id: nil, iso_code: nil, masked_card_number: nil, gateway_name: nil, sender_phone_number: response.response.phone_number, sender_email: response.response.email, receiver_phone_number: nil, receiver_email: nil)
            NotificationCenter.default.post(name: NSNotification.Name("change_tab"), object: nil,userInfo: ["tab":"2", "userData": Singleton.shared.sendDatauser])
        }
    }
    
}

extension ContactViewController: UITableViewDelegate, UITableViewDataSource, ScanQr {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(self.searchData.count > 0){
           return self.searchData.count
        }else {
            if(self.contactData.count == 0){
                self.noContactLabel.isHidden = false
            }else {
                self.noContactLabel.isHidden = true
            }
           return contactData.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TransactionTableCell") as! TransactionTableCell
        var val = TransactionResponse()
        if(self.searchData.count > 0){
            val = self.searchData[indexPath.row]
        }else {
            val = self.contactData[indexPath.row]
        }
            
        var requested_user_id = Int()
        if(self.isRequestMoney){
            cell.sendBtn.setTitle("Request", for: .normal)
        }
        if(val.receiver_id == Singleton.shared.userDetail.id){
            if((val.sender_profile_image ?? "").contains("http")){
                cell.userImage.sd_setImage(with: URL(string: self.getImageURL(text:val.sender_profile_image ?? "")), placeholderImage: #imageLiteral(resourceName: "round_image"))
            }else {
                 cell.userImage.sd_setImage(with: URL(string: self.getImageURL(text:U_IMAGE_BASE + (val.sender_profile_image ?? ""))), placeholderImage: #imageLiteral(resourceName: "round_image"))
            }
            cell.userName.text = val.sender_name
            cell.transactionDate.text = (val.sender_phone_number ?? "").applyPatternOnNumbers(pattern: "###-###-####", replacmentCharacter: "#")
            requested_user_id = val.sender_id ?? 0
                
                        
        }else {
            if((val.receiver_profile_image ?? "").contains("http")){
                cell.userImage.sd_setImage(with: URL(string: self.getImageURL(text:val.receiver_profile_image ?? "")), placeholderImage: #imageLiteral(resourceName: "round_image"))
            }else {
                 cell.userImage.sd_setImage(with: URL(string: self.getImageURL(text:U_IMAGE_BASE + (val.receiver_profile_image ?? ""))), placeholderImage: #imageLiteral(resourceName: "round_image"))
            }
            cell.userName.text = val.receiver_name ?? "You"
            cell.transactionDate.text = (val.receiver_phone_number ?? "").applyPatternOnNumbers(pattern: "###-###-####", replacmentCharacter: "#")
            requested_user_id = val.receiver_id ?? 0
        }
        
        cell.sendButton = {
            if(self.isRequestMoney){
                let alert = UIAlertController(title: "Request Money", message: "send reqest to \(cell.userName.text ?? "") for $\(self.amount) ", preferredStyle: .alert)
                let action1 = UIAlertAction(title: "Yes", style: .default) { (action) in
                    let param: [String:Any] = [
                        "requested_user_id": requested_user_id,
                        "amount": self.amount,
                        "currency_id": Singleton.shared.userDetail.currency_id ?? 0
                    ]
                    let myVC = self.storyboard?.instantiateViewController(withIdentifier: "PincodeViewController") as! PincodeViewController
                    myVC.param = param
                    myVC.selectedMethod = 6
                    self.navigationController?.pushViewController(myVC, animated: true)
                }
                
                let action2 = UIAlertAction(title: "No", style: .default) { (action) in
                    self.dismiss(animated: false, completion: nil)
                }
                
                alert.addAction(action1)
                alert.addAction(action2)
                self.present(alert, animated: true, completion: nil)
                
            }else {
             NotificationCenter.default.post(name: NSNotification.Name("change_tab"), object: nil,userInfo: ["tab":"2", "userData": val])
                Singleton.shared.sendDatauser = val
            }
        }
     
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var val = TransactionResponse()
        if(self.searchData.count > 0){
            val = self.searchData[indexPath.row]
        }else {
            val = self.contactData[indexPath.row]
        }
        if(val.transaction_type == 1){
            return
        }
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "TransactionDetailViewController") as! TransactionDetailViewController
        myVC.transactionDetail = val
        self.navigationController?.pushViewController(myVC, animated: true)
    }
}
