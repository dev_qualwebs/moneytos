//
//  FilterTransactionViewController.swift
//  Moneytoss
//
//  Created by qw on 23/08/20.
//  Copyright © 2020 qw. All rights reserved.
//

import UIKit

class FilterTransactionViewController: UIViewController, SelectDate
{
    func selectedDate(date: Int) {
        if(currentPicker == 1){
            self.startDate.text = self.convertTimestampToDate(date, to: "dd-MM-yyyy")
        }else if(currentPicker == 2){
              self.endDate.text = self.convertTimestampToDate(date, to: "dd-MM-yyyy")
        }
    }
    
    //MARK: IBOUtlets
    @IBOutlet weak var bankView: View!
    @IBOutlet weak var walletView: View!
    @IBOutlet weak var cashView: View!
    @IBOutlet weak var endDate: UITextField!
    @IBOutlet weak var startDate: UITextField!
    
    var currentPicker = Int()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    func presentDatePicker(mode:Int){
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "DatePickerViewController") as! DatePickerViewController
        myVC.minDate = Date()
         myVC.dateDelegate = self
        myVC.pickerMode = 1
        self.navigationController?.present(myVC, animated: true, completion: nil)
        
    }
    
    //MARK: IBActions
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    
    @IBAction func walletAction(_ sender: Any) {
        self.walletView.borderColor = .black
        self.cashView.borderColor = .clear
        self.bankView.borderColor = .clear
    }
    
    @IBAction func cashAction(_ sender: Any) {
        self.walletView.borderColor = .clear
        self.cashView.borderColor = .black
        self.bankView.borderColor = .clear
    }
    
    @IBAction func bankAction(_ sender: Any) {
        self.walletView.borderColor = .clear
        self.cashView.borderColor = .clear
        self.bankView.borderColor = .black
    }
    
    @IBAction func startAction(_ sender: Any) {
        self.currentPicker = 1
        self.presentDatePicker(mode: 1)
    }
    
    @IBAction func endAction(_ sender: Any) {
        self.currentPicker = 1
        self.presentDatePicker(mode: 1)
    }
}
