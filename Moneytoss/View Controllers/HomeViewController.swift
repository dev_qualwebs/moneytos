//
//  HomeViewController.swift
//  Moneytoss
//
//  Created by qw on 15/08/20.
//  Copyright © 2020 qw. All rights reserved.
//

import UIKit
import UPCarouselFlowLayout

class HomeViewController: UIViewController, UIScrollViewDelegate {
    //MARK: IBOutlets
    @IBOutlet weak var cardCollection: UICollectionView!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var trnasactionTable: UITableView!
    @IBOutlet weak var userImage: ImageView!
    @IBOutlet weak var noTransactionView: UIStackView!
    
    
    var dashBoardData = DashboardResponse()
    var cardData = [GetCardResponse]()
    var walletData = [WalletResponse]()
    var transactionData = [TransactionResponse]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        NavigationController.shared.getWalletData(completionHandler: { (data) in
            self.walletData = data
            self.cardCollection.reloadData()
        })
        
        NavigationController.shared.getCardData(completionHandler: { (data) in
            self.cardData = data
            self.cardCollection.reloadData()
        })
        
        NavigationController.shared.getTransactionData { (data) in
            self.transactionData = data
            self.trnasactionTable.reloadData()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        let profileData = Singleton.shared.userDetail
        if((profileData.profile_image ?? "").contains("http")){
            self.userImage.sd_setImage(with: URL(string: self.getImageURL(text:profileData.profile_image ?? "")), placeholderImage: #imageLiteral(resourceName: "round_image"))
        }else {
            self.userImage.sd_setImage(with: URL(string: self.getImageURL(text:U_IMAGE_BASE + (profileData.profile_image ?? ""))), placeholderImage: #imageLiteral(resourceName: "round_image"))
        }
       }
    
    func getWalletData(){
        ActivityIndicator.show(view: self.view)
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_GET_WALLETS, method: .get, parameter: nil, objectClass: Dashboard.self, requestCode: U_GET_WALLETS) { (response) in
            self.dashBoardData = response.response
            self.cardCollection.reloadData()
            ActivityIndicator.hide()
        }
    }
    
    func getDashboard(){
        ActivityIndicator.show(view: self.view)
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_GET_DASHBOARD_DATA, method: .get, parameter: nil, objectClass: Dashboard.self, requestCode: U_GET_DASHBOARD_DATA) { (response) in
            self.dashBoardData = response.response
            self.cardCollection.reloadData()
            ActivityIndicator.hide()
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if(scrollView == cardCollection){
            for cell in cardCollection.visibleCells {
                let indexPath = cardCollection.indexPath(for: cell)
                if(indexPath!.row == 0 || indexPath!.row == 1){
                    self.pageControl.currentPage = 0
                }else {
                    self.pageControl.currentPage = 1
                }
            }
        }
    }
    
    //MARK: IBActions
    @IBAction func profileAction(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
        self.navigationController?.pushViewController(myVC, animated: true)
    }
    
    @IBAction func addmoneyAction(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "AddMoneyViewController") as! AddMoneyViewController
        
        self.navigationController?.pushViewController(myVC, animated: true)
    }
    
    
    @IBAction func withdrawMoneyAction(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "AddMoneyViewController") as! AddMoneyViewController
        myVC.isWithdrawMoney = true
        self.navigationController?.pushViewController(myVC, animated: true)
    }
    
    
    @IBAction func moreAction(_ sender: Any) {
        NotificationCenter.default.post(name: NSNotification.Name("change_tab"), object: nil,userInfo: ["tab":"1"])
    }
    
    @IBAction func addCardAction(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "AddCardViewController") as! AddCardViewController
        self.navigationController?.pushViewController(myVC, animated: true)
    }
    
    @IBAction func notificationAction(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "NotificationViewController") as! NotificationViewController
        self.navigationController?.pushViewController(myVC, animated: true)
    }
    
    @IBAction func qrScanAction(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "ScannerVC") as! ScannerVC
        myVC.scanDelegalte = self
        self.present(myVC, animated: true, completion: nil)
    }
    
    func getUserInfoFromScan(code: String?) {
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_SEARCH_USER_VIA_EMAIL, method: .post, parameter: ["email":code], objectClass: GetUserInfoScan.self, requestCode: U_SEARCH_USER_VIA_EMAIL) { response in
            Singleton.shared.sendDatauser = TransactionResponse(id: response.response.id, transaction_type: nil, amount: nil, local_transaction_id: nil, user_id: nil, currency_id: nil, created_at: nil, notes: nil, transaction_method: nil, sender_name: (response.response.first_name ?? "") + " " + (response.response.last_name ?? ""), sender_id: response.response.id, receiver_name: nil, sender_profile_image: response.response.profile_image, receiver_profile_image: nil, receiver_id: nil, iso_code: nil, masked_card_number: nil, gateway_name: nil, sender_phone_number: response.response.phone_number, sender_email: response.response.email, receiver_phone_number: nil, receiver_email: nil)
            NotificationCenter.default.post(name: NSNotification.Name("change_tab"), object: nil,userInfo: ["tab":"2", "userData": Singleton.shared.sendDatauser])
        }
    }
    
}

extension HomeViewController: UITableViewDelegate, UITableViewDataSource,ScanQr {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var number = Int()
        print(UIScreen.main.bounds.height)
        if(self.transactionData.count == 0){
            self.noTransactionView.isHidden = false
        }else {
            self.noTransactionView.isHidden = true
        }
        if(UIScreen.main.bounds.height < 700){
            if(self.transactionData.count < 3){
                number = self.transactionData.count
            }else if((self.transactionData.count > 0)) {
              number = 3
            }
        }else if(UIScreen.main.bounds.height < 850){
            if(self.transactionData.count < 4){
                number = self.transactionData.count
            }else if((self.transactionData.count > 0)) {
              number = 4
            }

        }else {
            if(self.transactionData.count < 5){
                number = self.transactionData.count
            }else if((self.transactionData.count > 0)) {
              number = 5
            }

        }
        return number
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TransactionTableCell") as! TransactionTableCell
        let val = self.transactionData[indexPath.row]
        if(val.receiver_id == Singleton.shared.userDetail.id){
            if((val.sender_profile_image ?? "").contains("http")){
                cell.userImage.sd_setImage(with: URL(string: self.getImageURL(text:val.sender_profile_image ?? "")), placeholderImage: #imageLiteral(resourceName: "round_image"))
            }else {
                 cell.userImage.sd_setImage(with: URL(string: self.getImageURL(text:U_IMAGE_BASE + (val.receiver_profile_image ?? ""))), placeholderImage: #imageLiteral(resourceName: "round_image"))
            }
            cell.userName.text = val.sender_name
                        
        }else {
            if((val.receiver_profile_image ?? "").contains("http")){
                cell.userImage.sd_setImage(with: URL(string: self.getImageURL(text:val.receiver_profile_image ?? "")), placeholderImage: #imageLiteral(resourceName: "round_image"))
            }else {
                 cell.userImage.sd_setImage(with: URL(string: self.getImageURL(text:U_IMAGE_BASE + (val.receiver_profile_image ?? ""))), placeholderImage: #imageLiteral(resourceName: "round_image"))
            }
            cell.userName.text = val.receiver_name ?? "You"
        }
        if(val.transaction_type == 2){
            cell.transactionAmount.textColor = redColor
            cell.transactionAmount.text = "-" + (val.amount ?? "")
        }else {
            cell.transactionAmount.textColor = greenColor
            cell.transactionAmount.text = "+" + (val.amount ?? "")
        }
        
        cell.transactionType.text = val.transaction_method ?? "Stripe"
        cell.transactionDate.text = "On " + self.convertTimestampToDate(val.created_at ?? 0, to: "MMM dd, yyyy")
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if(self.transactionData[indexPath.row].transaction_type == 1){
            return
        }
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "TransactionDetailViewController") as! TransactionDetailViewController
        myVC.transactionDetail = self.transactionData[indexPath.row]
        self.navigationController?.pushViewController(myVC, animated: true)
    }
}

extension HomeViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if(self.cardData.count == 0){
            self.pageControl.numberOfPages = 1
            self.pageControl.alpha = 0
            return 2
        }else {
            self.pageControl.alpha = 1
            self.pageControl.numberOfPages = 2
            return self.cardData.count + 1
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CardCollectionCell", for: indexPath) as! CardCollectionCell
        if(indexPath.row == 0){
            cell.view1.isHidden = true
            cell.view2.isHidden = false
            if(walletData.count > 0){
                cell.walletBalance.text = (self.walletData[0].symbol ?? "") + (self.walletData[0].balance ?? "0.0")
            }
        }else {
            if(self.cardData.count == 0){
                cell.view1.isHidden = true
                cell.view2.isHidden = true
                cell.view3.isHidden = false
            }else{
            let val = self.cardData[indexPath.row-1]
            cell.view1.isHidden = false
            cell.view2.isHidden = true
            cell.view3.isHidden = true
            cell.expiryDate.text = "\(val.expiry_month ?? "")/\(val.expiry_year ?? "")"
            cell.cardNumber.text = "\(val.masked_card_number?.dropFirst(8) ?? "")"
            cell.cardholderName.text = val.name_on_card
            }
        }
        
        cell.addCard = {
            let myVC = self.storyboard?.instantiateViewController(withIdentifier: "AddCardViewController") as! AddCardViewController
            self.navigationController?.pushViewController(myVC, animated: true)
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width/2, height: 180)
    }
}


class TransactionTableCell: UITableViewCell {
    //MARK: IBOutlets
    @IBOutlet weak var userImage: ImageView!
    @IBOutlet weak var userName: DesignableUILabel!
    @IBOutlet weak var transactionDate: DesignableUILabel!
    @IBOutlet weak var transactionType: DesignableUILabel!
    @IBOutlet weak var transactionAmount: DesignableUILabel!
    @IBOutlet weak var sendBtn: CustomButton!
    
    var sendButton:(()-> Void)? = nil
    var rejectButton:(()-> Void)? = nil
    
    @IBAction func sendAction(_ sender: Any) {
        if let sendButton = self.sendButton {
            sendButton()
        }
    }
    
    @IBAction func rejectAction(_ sender: Any) {
        if let rejectButton = self.rejectButton {
            rejectButton()
        }
    }
}

class CardCollectionCell: UICollectionViewCell {
    //MARK: IBOutlets
    @IBOutlet weak var mainView: View!
    @IBOutlet weak var view1: View!
    @IBOutlet weak var view2: View!
    @IBOutlet weak var view3: View!
    
    @IBOutlet weak var expiryDate: DesignableUILabel!
    @IBOutlet weak var cardNumber: DesignableUILabel!
    @IBOutlet weak var cardImage: UIImageView!
    @IBOutlet weak var cardholderName: DesignableUILabel!
    @IBOutlet weak var walletBalance: DesignableUILabel!
    @IBOutlet weak var cardBackView: View!
    
    
    var addCard:(()-> Void)? = nil
    
    @IBAction func addCardAction(_ sender: Any) {
        if let addCard = self.addCard {
            addCard()
        }
    }
    
}
