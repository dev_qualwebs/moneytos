//
//  PincodeViewController.swift
//  Moneytoss
//
//  Created by qw on 23/08/20.
//  Copyright © 2020 qw. All rights reserved.
//

import UIKit
import TransitionButton

class PincodeViewController: UIViewController {
    //MARK: IBOutlets
    @IBOutlet weak var number1: DesignableUITextField!
    @IBOutlet weak var number2: DesignableUITextField!
    @IBOutlet weak var number3: DesignableUITextField!
    @IBOutlet weak var number4: DesignableUITextField!
    @IBOutlet weak var headingLabel: DesignableUILabel!
    @IBOutlet weak var numberLabel: DesignableUILabel!
    @IBOutlet weak var keypadView: UIView!
    
    @IBOutlet weak var confirmButton: TransitionButton!
    
    
    var numArr = ["","","","",""]
    var position = 1
    var param = [String: Any]()
    var userDetail = SearchUserResponse()
    var selectedMethod = Int()
    var transferRequestId: Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initView()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.keypadView.bounceAnimationView(view:self.keypadView)
    }
    
    
    func initView(){
        self.number1.text = ""
        self.number1.delegate = self
        self.number2.text = ""
        self.number2.delegate = self
        self.number3.text = ""
        self.number3.delegate = self
        self.number4.text = ""
        self.number4.delegate = self
        
    }
    
    
    func handleText() -> String? {
        var num = (number1.text ?? "") + (number2.text ?? "")
        num +=  (number3.text ?? "") + (number4.text ?? "")
        return num ?? ""
    }
    
    func callApiMethod(){
        self.confirmButton.startAnimation()
        NotificationCenter.default.addObserver(self, selector: #selector(self.stopAnimation), name: NSNotification.Name(N_STOP_BUTTON_ANIMATION), object: nil)
        self.confirmButton.backgroundColor = primaryColor
        var url = String()
        self.param["security_pin"] = self.handleText()
        if(self.selectedMethod == 1){
            url = U_BASE + U_WALLET_TRANSFER
        }else if(self.selectedMethod == 2){
            url = U_BASE + U_COD_TRANSFER
        }else if(self.selectedMethod == 3){
            url = U_BASE + U_SENDTO_BANK_ACCOUNT
        }else if(self.selectedMethod == 4){
            url = U_BASE + U_WITHDRAW_MOENY
        }else if(self.selectedMethod == 5){
            url = U_BASE + U_ADD_MONEY_WALLET
        }else if(self.selectedMethod == 6){
            url = U_BASE + U_REQUEST_MONEY
        }
   
        if(self.selectedMethod == 6){
            SessionManager.shared.methodForApiCalling(url: url, method: .post, parameter: self.param, objectClass: SuccessResponse.self , requestCode: url) { (response) in
                self.confirmButton.stopAnimation(animationStyle: .expand, completion: { 
                        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "TabbarViewController") as! TabbarViewController
                        NavigationController.shared.addTransition(direction:.fromTop, controller: self)
                        self.navigationController?.pushViewController(myVC, animated: false)
                        Singleton.shared.showToast(text: "Successfully sent request.")
                })
            }
            
        }else {
            
            if(self.selectedMethod == 4 || self.selectedMethod == 5 || self.selectedMethod == 6){
                SessionManager.shared.methodForApiCalling(url: url, method: .post, parameter: self.param, objectClass:SuccessResponse.self, requestCode: url) { (response) in
                    self.confirmButton.stopAnimation(animationStyle: .expand, completion: {
                        
                            Singleton.shared.walletData = []
                            Singleton.shared.transactionData = []
                            let myVC = self.storyboard?.instantiateViewController(withIdentifier: "TabbarViewController") as! TabbarViewController
                            NavigationController.shared.addTransition(direction:.fromTop, controller: self)
                            self.navigationController?.pushViewController(myVC, animated: false)
                            if(self.selectedMethod == 5){
                                Singleton.shared.showToast(text: "Money added to wallet.")
                            }else if(self.selectedMethod == 4){
                                Singleton.shared.showToast(text: "Money withdrawl to selected card.")
                            }else if(self.selectedMethod == 6){
                                Singleton.shared.showToast(text: "Successfully sent request.")
                            }
                            return
                        
                    })
                }
            }else {
                SessionManager.shared.methodForApiCalling(url: url, method: .post, parameter: self.param, objectClass:SendMoney.self, requestCode: url) { (response) in
                    self.confirmButton.stopAnimation(animationStyle: .expand, completion: {
                        Singleton.shared.sendDatauser = TransactionResponse()
                        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "SuccessScreenViewController") as! SuccessScreenViewController
                        if let id = self.transferRequestId as? Int{
                        self.callAcceptRejectApi(type: 1, requestId: id)
                        }
                        myVC.pdfUrl = response.response
                        myVC.param = self.param
                        myVC.userDetail = self.userDetail
                        NavigationController.shared.addTransition(direction:.fromTop, controller: self)
                        self.navigationController?.pushViewController(myVC, animated: false)
                    })
                }
            }
        }
    }
    
    func callAcceptRejectApi(type: Int?, requestId: Int){
       
        let param:[String:Any] = [
            "requested_id":requestId,
            "status": type
        ]
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_UPDATE_MONEY_REQUEST, method: .post, parameter: param, objectClass: SuccessResponse.self, requestCode: U_UPDATE_MONEY_REQUEST) { (response) in
           
            Singleton.shared.showToast(text: "Succesufully tansferred money")
            
        }
    }

    
    @objc func stopAnimation(){
        self.confirmButton.stopAnimation()
        self.confirmButton.backgroundColor = .clear
        self.clearAction(self)
    }
    
    func handleTextChange(val: String){
        if(position == 1){
            number1.backgroundColor = .black
            number1.text = val
            self.position  += 1
        }else if(position == 2){
            number2.backgroundColor = .black
            number2.text = val
            self.position  += 1
        }else if(position == 3){
            number3.backgroundColor = .black
            number3.text = val
            self.position  += 1
        }else if(position == 4){
            number4.backgroundColor = .black
            number4.text = val
        }
    }
    
    //MARK:IBActions
    @IBAction func oneAction(_ sender: Any) {
        self.handleTextChange(val: "1")
    }
    
    @IBAction func twoAction(_ sender: Any) {
        self.handleTextChange(val: "2")
    }
    
    @IBAction func threeAction(_ sender: Any) {
        self.handleTextChange(val: "3")
    }
    
    @IBAction func fourAction(_ sender: Any) {
        self.handleTextChange(val: "4")
    }
    
    @IBAction func fiveAction(_ sender: Any) {
        self.handleTextChange(val: "5")
    }
    
    @IBAction func sixAction(_ sender: Any) {
        self.handleTextChange(val: "6")
    }
    
    @IBAction func sevenAction(_ sender: Any) {
        self.handleTextChange(val: "7")
    }
    
    @IBAction func eightAction(_ sender: Any) {
        self.handleTextChange(val: "8")
    }
    
    @IBAction func nineAction(_ sender: Any) {
        self.handleTextChange(val: "9")
    }
    
    @IBAction func zeroAction(_ sender: Any) {
        self.handleTextChange(val: "0")
    }
    
    @IBAction func clearAction(_ sender: Any) {
        self.numArr = []
        number1.backgroundColor = .white
        number2.backgroundColor = .white
        number3.backgroundColor = .white
        number4.backgroundColor = .white
        
        self.number1.text = ""
        self.number2.text = ""
        self.number3.text = ""
        self.number4.text = ""
        self.position = 1
    }
    
    
    @IBAction func nextAction(_ sender: Any) {
        let num = self.handleText()
        if(num!.isEmpty){
            Singleton.shared.showToast(text: "Enter verification pin")
            return
        }else if(num!.count < 4){
            Singleton.shared.showToast(text: "Enter correct verification pin")
            return
        }
        if(Singleton.shared.userDetail.security_pin == nil){
            ActivityIndicator.show(view: self.view)
            SessionManager.shared.methodForApiCalling(url: U_BASE + U_ADD_SECURITY_PIN, method: .post, parameter: ["security_pin": num,"confirm_security_pin": num], objectClass: SuccessResponse.self, requestCode: U_ADD_SECURITY_PIN) { (response) in
                ActivityIndicator.hide()
                self.callApiMethod()
            }
        }else {
            self.callApiMethod()
        }
    }
    
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}

extension PincodeViewController: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        
        if let char = string.cString(using: String.Encoding.utf8) {
            let isBackSpace = strcmp(char, "\\b")
            if (isBackSpace == -92 || textField.text?.count == 0 ) {
                return true
            }else {
                return false
            }
        }
        return true
    }
}
