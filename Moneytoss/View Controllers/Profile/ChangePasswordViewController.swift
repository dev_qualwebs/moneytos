//
//  ChangePasswordViewController.swift
//  Moneytoss
//
//  Created by qw on 30/01/21.
//  Copyright © 2021 qw. All rights reserved.
//

import UIKit

class ChangePasswordViewController: UIViewController {
    //MARK: IBOutlets
    @IBOutlet weak var currentPassword: FloatingPlaceholderTextField!
    @IBOutlet weak var confirmNewPassword: FloatingPlaceholderTextField!
    @IBOutlet weak var newPassword: FloatingPlaceholderTextField!
    @IBOutlet weak var headingLabel: DesignableUILabel!
    @IBOutlet weak var confirmButton: CustomButton!
    @IBOutlet weak var firgotButton: CustomButton!
    @IBOutlet weak var remarkLabel: UILabel!
    
    
    var isChangeSecurityPin = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initView()
    }
    
    func initView(){
        if(self.isChangeSecurityPin){
            self.currentPassword.textInput?.placeholder = "Enter Current Security Pin"
            self.confirmNewPassword.textInput?.placeholder = "Confirm New Security Pin"
            self.newPassword.textInput?.placeholder = "Enter New Security Pin"
            self.headingLabel.text = "Security Pin"
            self.confirmButton.setTitle("Save", for: .normal)
            self.firgotButton.isHidden = true
            self.remarkLabel.text = "Once security pin is successfully changed, you need to login again."
        }
    }
    
    //MARK: IBAction
    @IBAction func changePassword(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func forgotPasswordAction(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "ForgotPasswordViewController") as! ForgotPasswordViewController
        self.navigationController?.pushViewController(myVC, animated: true)
    }
    
    @IBAction func confirmPasswordAction(_ sender: Any) {
        if((self.currentPassword.textInput?.text ?? "")!.isEmpty){
            Singleton.shared.showToast(text: self.currentPassword.textInput?.placeholder ?? "")
        }else if((self.newPassword.textInput?.text ?? "")!.isEmpty){
            Singleton.shared.showToast(text: self.newPassword.textInput?.placeholder ?? "")
        }else if((self.confirmNewPassword.textInput?.text ?? "")!.isEmpty){
            Singleton.shared.showToast(text: self.confirmNewPassword.textInput?.placeholder ?? "")
        }else if(self.confirmNewPassword.textInput?.text != self.newPassword.textInput?.text){
            if(isChangeSecurityPin){
                Singleton.shared.showToast(text: "Pin not matched")
            }else{
            Singleton.shared.showToast(text: "Password not matched")
            }
        }else {
            var param:[String:Any]
            var url = String()
                
            if(self.isChangeSecurityPin){
                param = [
                    "current_security_pin": self.currentPassword.textInput?.text,
                    "new_security_pin": self.newPassword.textInput?.text,
                    "confirm_security_pin":self.confirmNewPassword.textInput?.text,
            ]
                url = U_BASE + U_CHANGE_PIN
            }else{
                param = [
                    "current_password": self.currentPassword.textInput?.text,
                    "new_password": self.newPassword.textInput?.text
            ]
                url = U_BASE + U_CHANGE_PASSWORD
            }
            ActivityIndicator.show(view: self.view)
            SessionManager.shared.methodForApiCalling(url: url, method: .post, parameter: param, objectClass: SuccessResponse.self, requestCode: U_CHANGE_PASSWORD) { (response) in
                ActivityIndicator.hide()
                if(self.isChangeSecurityPin){
                    Singleton.shared.showToast(text: "Successfully changed pin")
                }else {
                    Singleton.shared.showToast(text: "Successfully changed password")
                }
                let myVC = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
                UserDefaults.standard.removeObject(forKey: UD_TOKEN)
                UserDefaults.standard.removeObject(forKey: UD_USER_EMAIL)
                UserDefaults.standard.removeObject(forKey: UD_USER_DETAIL)
                Singleton.shared.cardData = []
                Singleton.shared.paymentMethods = []
                Singleton.shared.currencyData = []
                Singleton.shared.walletData = []
                Singleton.shared.transactionData = []
                Singleton.shared.contactData = []
                Singleton.shared.countryData = []
                self.navigationController?.pushViewController(myVC, animated: true)
            }
            
        }
        
    }
    
}
