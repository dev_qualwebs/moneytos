//
//  HelpSupportViewController.swift
//  Moneytoss
//
//  Created by qw on 20/02/21.
//  Copyright © 2021 qw. All rights reserved.
//

import UIKit
import SDWebImage

class HelpSupportViewController: UIViewController, UpdateQueryTable {
    func refreshQueryTable() {
        self.getQueries()
    }
    
    //MARK: IBOutlets
    @IBOutlet weak var queryTable: UITableView!
    
    
    var queryData = [SupportQuery]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.getQueries()
        self.queryTable.tableFooterView = UIView()
    }
    
    func getQueries(){
        ActivityIndicator.show(view: self.view)
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_GET_SUPPORT_QUERY, method: .get, parameter: nil, objectClass: GetSupportQuery.self, requestCode: U_GET_SUPPORT_QUERY) { (response) in
            self.queryData = response.response.data
            self.queryTable.reloadData()
            ActivityIndicator.hide()
        }
    }
    
    //MARK: IBActions
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func addqueryAction(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "QueryViewController") as! QueryViewController
        myVC.queryDelegate = self
        self.navigationController?.pushViewController(myVC, animated: true)
    }
    
}

extension HelpSupportViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.queryData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TransactionTableCell") as! TransactionTableCell
        let val = self.queryData[indexPath.row]
        cell.userImage.sd_setImage(with: URL(string: U_IMAGE_BASE + (val.image ?? "")), placeholderImage: #imageLiteral(resourceName: "round_image")) as? UIImage
        cell.userName.text = val.query
        cell.transactionType.text = "ID-\(val.id ?? 0)"
        let timestamp = self.convertDateToTimestamp(val.created_at ?? "", to: "yyyy-MM-dd'T'HH:mm:ss.SSSZ")
        let date = self.convertTimestampToDate(timestamp, to: "dd/MM/yyyy")
        let time = self.convertTimestampToDate(timestamp, to: "h:mm a")
        cell.transactionDate.text = date + "|" + time
        cell.sendButton = { [self] in
            let imageView = UIImageView()
            imageView.image = cell.userImage.image
            imageView.frame = UIScreen.main.bounds
            imageView.backgroundColor = .white
            imageView.contentMode = .scaleAspectFit
            imageView.isUserInteractionEnabled = true
                let tap = UITapGestureRecognizer(target: self, action: #selector(dismissFullscreenImage))
            imageView.addGestureRecognizer(tap)
            
            self.view.addSubview(imageView)
            
        }
        return cell
    }
    
    @objc func dismissFullscreenImage(_ sender: UITapGestureRecognizer) {
        sender.view?.removeFromSuperview()
    }
}



