//
//  NotificationViewController.swift
//  Moneytoss
//
//  Created by Sagar Pandit on 05/08/21.
//  Copyright © 2021 qw. All rights reserved.
//

import UIKit

class NotificationViewController: UIViewController {
  
    //MARK: IBOutlets
    @IBOutlet weak var inboxTable: UITableView!
    @IBOutlet weak var noDataFound: DesignableUILabel!
    
    var notificationData = [NotificationResposne]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        inboxTable.estimatedRowHeight = 70
        inboxTable.rowHeight = UITableView.automaticDimension
        inboxTable.tableFooterView = UIView()
        self.getNotificationData(page:0)
    }
  
    
    func getNotificationData(page:Int){
        ActivityIndicator.show(view: self.view)

        SessionManager.shared.methodForApiCalling(url: U_BASE + U_GET_NOTIFICATION, method: .get, parameter: nil, objectClass: GetNotification.self, requestCode: U_GET_NOTIFICATION) { (response) in
            self.notificationData = response.response
            self.inboxTable.delegate = self
            self.inboxTable.dataSource = self
            self.inboxTable.reloadData()
            ActivityIndicator.hide()
        }
    }
    
    //MARK: IBActions
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
}

extension NotificationViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(self.notificationData.count == 0){
            self.noDataFound.isHidden = false
        }else{
            self.noDataFound.isHidden = true
        }
        return self.notificationData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "InboxViewCell") as! InboxViewCell
        let val = self.notificationData[indexPath.row]
        cell.msgLabel.text = val.redirect
        cell.userName.text = val.content
        let strArr = (val.content ?? "").split(separator: " ")
        for val in strArr{
            if(val.contains("$")){
                cell.priceLabel.text = "\(val)"
            }
        }
        cell.messageDate.text = self.convertTimestampToDate(val.created_at ?? 0, to: "dd MMM")
        
        switch val.notification_type_id {
        case 1,4:
            cell.tickImage.image = #imageLiteral(resourceName: "MicrosoftTeams-image (2)")
            break
        case 2:
            cell.tickImage.image = #imageLiteral(resourceName: "MicrosoftTeams-image (1)")
            break
        case 3:
            cell.tickImage.image = #imageLiteral(resourceName: "MicrosoftTeams-image (3)")
            break
        default:
            break
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let val = self.notificationData[indexPath.row]
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "TabbarViewController") as! TabbarViewController
        self.navigationController?.pushViewController(myVC, animated: true)
    }
}

class InboxViewCell : UITableViewCell {
    //MARK: IBOutlets
    @IBOutlet weak var userImage: ImageView!
    @IBOutlet weak var messageCount: UILabel!
    @IBOutlet weak var messageDate: UILabel!
    @IBOutlet weak var msgLabel: UILabel!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var viewForTick: UIView!
    @IBOutlet weak var tickImage: UIImageView!
    @IBOutlet weak var priceLabel: DesignableUILabel!
    
    var tickBtn: (()-> Void)? = nil
    
    //MARK: IBActions
    @IBAction func tickAction(_ sender: Any) {
        if let tickBtn = self.tickBtn {
            tickBtn()
        }
    }
}
