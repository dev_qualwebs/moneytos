//
//  ProfileDetialViewController.swift
//  Moneytoss
//
//  Created by qw on 15/08/20.
//  Copyright © 2020 qw. All rights reserved.
//

import UIKit
import CoreLocation
import GooglePlaces

class ProfileDetialViewController: UIViewController, SelectDate {
    func selectedDate(date: Int) {
        self.selectedDob = date
        self.dobField.text = self.convertTimestampToDate(date, to: "dd-MM-YYYY")
    }
    
    
    //MARK: IBOUtlets
    // @IBOutlet weak var firstNamePlaceholder: DesignableUILabel!
    // @IBOutlet weak var lastNamePlaceholder: DesignableUILabel!
    // @IBOutlet weak var emailPlaceholder: DesignableUILabel!
    @IBOutlet weak var firstNameField: DesignableUITextField!
    @IBOutlet weak var lastNameField: DesignableUITextField!
    @IBOutlet weak var emailField: DesignableUITextField!
    @IBOutlet weak var userImage: ImageView!
    @IBOutlet weak var userId: DesignableUILabel!
    @IBOutlet weak var fieldView: UIStackView!
    @IBOutlet weak var editLabel: DesignableUILabel!
    @IBOutlet weak var dobField: DesignableUITextField!
    @IBOutlet weak var addressField: DesignableUITextField!
    
    
    var profileData = SignupResponse()
    var imagePath = ""
    let picker = UIImagePickerController()
    var imageName = ""
    var latitude = CLLocationDegrees()
    var longitude = CLLocationDegrees()
    var selectedDob = Int()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.picker.delegate = self
        initView()
    }
    
    func initView(){
        self.fieldView.isUserInteractionEnabled = false
        
        if((profileData.profile_image ?? "").contains("http")){
            self.userImage.sd_setImage(with: URL(string: self.getImageURL(text:profileData.profile_image ?? "")), placeholderImage: #imageLiteral(resourceName: "round_image"))
        }else {
            self.userImage.sd_setImage(with: URL(string: self.getImageURL(text:U_IMAGE_BASE + (profileData.profile_image ?? ""))), placeholderImage: #imageLiteral(resourceName: "round_image"))
        }
        self.imagePath = self.profileData.profile_image ?? ""
        self.firstNameField.text = self.profileData.first_name
        self.lastNameField.text = self.profileData.last_name
        self.emailField.text = self.profileData.email
        self.dobField.text = self.convertTimestampToDate(Int(self.profileData.dob ?? "0")!, to: "dd-MM-YYYY")
        self.selectedDob = Int(self.profileData.dob ?? "0")!
        self.addressField.text = self.profileData.address
        let id = profileData.username?.prefix(3)
        self.userId.text = "ID" + "\(id ?? "")\(profileData.id ?? 0)" + "@MT"
    }
    
    override func viewWillAppear(_ animated: Bool) {
        initView()
    }
    
    //MARK: IBACtions
    @IBAction func backAction(_ sender: Any) {
        if(self.editLabel.text == "Save"){
            let alert = UIAlertController(title: "Going back will undo the changes", message: "Continue?", preferredStyle: .alert)
            let action1 = UIAlertAction(title: "Yes", style: .default) { (action) in
                self.navigationController?.popViewController(animated: true)
            }
            let action2 = UIAlertAction(title: "No", style: .cancel, handler: nil)
            
            alert.addAction(action1)
            alert.addAction(action2)
            self.present(alert, animated: true, completion: nil)
        }else {
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    @IBAction func uploadImageAction(_ sender: Any) {
        if(self.editLabel.text == "Save"){
            let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
            alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
                self.openCamera()
            }))
            
            alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
                self.openGallary()
            }))
            
            alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
            if let popoverController = alert.popoverPresentationController {
                popoverController.sourceView = sender as! UIView
                popoverController.sourceRect = (sender as AnyObject).bounds
            }
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    @IBAction func editAction(_ sender: Any) {
        if(self.editLabel.text == "Edit"){
            self.editLabel.text = "Save"
            self.fieldView.isUserInteractionEnabled = true
            
        }else {
            if(self.imagePath == ""){
                Singleton.shared.showToast(text: "Select Profile Image.")
            }else if(self.firstNameField.text!.isEmpty){
                Singleton.shared.showToast(text: "Enter First Name.")
            }else if(self.lastNameField.text!.isEmpty){
                Singleton.shared.showToast(text: "Enter Last Name.")
            }else if(self.dobField.text!.isEmpty){
                Singleton.shared.showToast(text: "Select Date of Birth.")
            }else if(self.addressField.text!.isEmpty){
                Singleton.shared.showToast(text: "Enter your Address.")
            }else {
                ActivityIndicator.show(view: self.view)
                let param:[String:Any] = [
                    "first_name": self.firstNameField.text,
                    "last_name": self.lastNameField.text,
                    "profile_image": self.imagePath,
                    "dob": self.selectedDob,
                    "address": self.addressField.text,
                    "latitude": self.latitude,
                    "longitude":self.longitude
                ]
                SessionManager.shared.methodForApiCalling(url: U_BASE + U_UPDATE_PROFILE, method: .post, parameter: param, objectClass: Signup.self, requestCode: U_UPDATE_PROFILE) { (response) in
                    self.editLabel.text = "Edit"
                    Singleton.shared.userDetail.profile_image = self.imagePath
                    Singleton.shared.userDetail.first_name = (self.firstNameField.text ?? "")
                    Singleton.shared.userDetail.last_name = (self.lastNameField.text ?? "")
                    Singleton.shared.userDetail.dob = "\(self.selectedDob)"
                    Singleton.shared.userDetail.address = self.addressField.text
                    Singleton.shared.userDetail.address_latitude = "\(self.latitude)"
                    Singleton.shared.userDetail.address_longitude = "\(self.longitude)"
                    let userData = try! JSONEncoder().encode(Singleton.shared.userDetail)
                    UserDefaults.standard.set(userData, forKey: UD_USER_DETAIL)
                    self.fieldView.isUserInteractionEnabled = false
                    ActivityIndicator.hide()
                    Singleton.shared.showToast(text: "Successfully upadated profile.")
                }
            }
        }
    }
    
    
    @IBAction func dobAction(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "DatePickerViewController") as! DatePickerViewController
        myVC.modalPresentationStyle = .overFullScreen
        myVC.dateDelegate = self
        myVC.maxDate = Date()
        self.navigationController?.present(myVC, animated: true, completion: nil)
    }
    
    
    @IBAction func addressAction(_ sender: Any) {
        let autocompleteController = GMSAutocompleteViewController()
        autocompleteController.delegate = self
        
        // Specify the place data types to return.
        let fields: GMSPlaceField = GMSPlaceField(rawValue:UInt(GMSPlaceField.name.rawValue) |
                                                    UInt(GMSPlaceField.placeID.rawValue) |
                                                    UInt(GMSPlaceField.coordinate.rawValue) |
                                                    GMSPlaceField.addressComponents.rawValue |
                                                    GMSPlaceField.formattedAddress.rawValue)
        // autocompleteController.placeFields = .formattedAddress
        
        // Specify a filter.
        let filter = GMSAutocompleteFilter()
        filter.type = .address
        
        autocompleteController.autocompleteFilter = filter
        
        // Display the autocomplete view controller.
        autocompleteController.modalPresentationStyle = .overFullScreen
        present(autocompleteController, animated: true, completion: nil)
    }
}


extension ProfileDetialViewController: UIImagePickerControllerDelegate,UINavigationControllerDelegate  {
    
    
    func imagePickerController(_ picker: UIImagePickerController,
                               didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]){
        guard let selectedImage = info[UIImagePickerController.InfoKey.editedImage] as? UIImage else { return }
        
        if let selectedImageName = ((info[UIImagePickerController.InfoKey.referenceURL] as? NSURL)?.lastPathComponent) {
            
            self.imageName = selectedImageName
        }else {
            self.imageName = "image.jpg"
        }
        let imageData:NSData = selectedImage.pngData()! as NSData
        
        //save in user default
        let strBase64 = imageData.base64EncodedString(options: .lineLength64Characters)
        if let cropImage = selectedImage as? UIImage {
            let data = cropImage.jpegData(compressionQuality: 0.2) as Data?
            let params = [
                "file": data!,
            ] as [String : Any]
            // HTTP Request for upload Picture
            ActivityIndicator.show(view: self.view)
            
            SessionManager.shared.makeMultipartRequest(url: U_BASE + U_UPLOAD_PROFILE_IMAGE, fileData: data!, param: params, fileName: imageName) { (path) in
                print(path)
                self.imagePath = (path as! UploadImage).response?.image ?? ""
                Singleton.shared.userDetail.profile_image = self.imagePath
                self.userImage.image = cropImage
                
            }
            ActivityIndicator.hide()
        }
        picker.dismiss(animated: true)
    }
    
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera))
        {
            picker.sourceType = UIImagePickerController.SourceType.camera
            picker.allowsEditing = true
            self.present(picker, animated: true, completion: nil)
        }
        else
        {
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func openGallary()
    {
        picker.sourceType = UIImagePickerController.SourceType.photoLibrary
        picker.allowsEditing = true
        self.present(picker, animated: true, completion: nil)
    }
    
    func presentCameraSettings() {
        let alertController = UIAlertController(title: "Allow Camera to take photos",
                                                message: "Camera access is denied",
                                                preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Cancel", style: .default){
            _ in
        })
        alertController.addAction(UIAlertAction(title: "Settings", style: .cancel) { _ in
            if let url = URL(string: UIApplication.openSettingsURLString) {
                UIApplication.shared.open(url, options: [:], completionHandler: { _ in
                    // Handle
                })
            }
        })
        self.present(alertController, animated: true)
    }
    
}

extension ProfileDetialViewController: GMSAutocompleteViewControllerDelegate {
    // Handle the user's selection.
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        self.addressField.text = place.formattedAddress
        self.latitude = place.coordinate.latitude
        self.longitude = place.coordinate.longitude
        dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
}
