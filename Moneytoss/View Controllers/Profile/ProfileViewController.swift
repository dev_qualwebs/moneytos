//
//  ProfileViewController.swift
//  Moneytoss
//
//  Created by qw on 15/08/20.
//  Copyright © 2020 qw. All rights reserved.
//

import UIKit
import  SDWebImage

class ProfileViewController: UIViewController, ScanQr {
    func getUserInfoFromScan(code: String?) {
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_SEARCH_USER_VIA_EMAIL, method: .post, parameter: ["email":code], objectClass: GetUserInfoScan.self, requestCode: U_SEARCH_USER_VIA_EMAIL) { response in
            let myVC = self.storyboard?.instantiateViewController(withIdentifier: "SendViewController") as! SendViewController
            myVC.userData = TransactionResponse(id: response.response.id, transaction_type: nil, amount: nil, local_transaction_id: nil, user_id: nil, currency_id: nil, created_at: nil, notes: nil, transaction_method: nil, sender_name: (response.response.first_name ?? "") + " " + (response.response.last_name ?? ""), sender_id: response.response.id, receiver_name: nil, sender_profile_image: response.response.profile_image, receiver_profile_image: nil, receiver_id: nil, iso_code: nil, masked_card_number: nil, gateway_name: nil, sender_phone_number: response.response.phone_number, sender_email: response.response.email, receiver_phone_number: nil, receiver_email: nil)
            myVC.isShowBack = true
            self.navigationController?.pushViewController(myVC, animated: true)
        }
    }
    
    //MARK: IBOutlets
    @IBOutlet weak var userImage: ImageView!
    @IBOutlet weak var userId: DesignableUILabel!
    @IBOutlet weak var username: DesignableUILabel!
    
    @IBOutlet weak var userqrId: DesignableUILabel!
    
    @IBOutlet weak var userqrName: DesignableUILabel!
    @IBOutlet weak var scanqrEmail: DesignableUILabel!
    @IBOutlet weak var scanqrContact: DesignableUILabel!
    @IBOutlet weak var qrImage: UIImageView!
    @IBOutlet weak var qrView: UIView!
    
    
    var profileData = SignupResponse()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NavigationController.shared.getUserDocument { response in
            
        }
    }
  
    
    override func viewDidAppear(_ animated: Bool) {
        self.profileData = Singleton.shared.userDetail
        if((profileData.profile_image ?? "").contains("http")){
            self.userImage.sd_setImage(with: URL(string: self.getImageURL(text:profileData.profile_image ?? "")), placeholderImage: #imageLiteral(resourceName: "round_image"))
        }else {
            self.userImage.sd_setImage(with: URL(string: self.getImageURL(text:U_IMAGE_BASE + (profileData.profile_image ?? ""))), placeholderImage: #imageLiteral(resourceName: "round_image"))
        }
         
        self.username.text = profileData.username ?? ((profileData.first_name ?? "") + " " + (profileData.last_name ?? ""))
        self.userqrName.text = profileData.username ?? ((profileData.first_name ?? "") + " " + (profileData.last_name ?? ""))
        
        let id = profileData.username?.prefix(3)
        self.userId.text = "ID" + "\(id ?? "")\(profileData.id ?? 0)" + "@MT"
        self.userqrId.text = self.userqrId.text
        self.scanqrEmail.text = profileData.email
        self.scanqrContact.text = profileData.phone_number
        self.qrImage.sd_setImage(with: URL(string: self.getImageURL(text:"https://api.moneytos.com/" + (profileData.qr_code ?? ""))), placeholderImage: #imageLiteral(resourceName: "qr_image"))
    }
    
    //MARK: IBActions
    @IBAction func settingsAction(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "SettingsViewController") as! SettingsViewController
        self.navigationController?.pushViewController(myVC, animated: true)
    }
    
    @IBAction func securityAction(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "ChangePasswordViewController") as! ChangePasswordViewController
        myVC.isChangeSecurityPin = true
        self.navigationController?.pushViewController(myVC, animated: true)
    }
    
    @IBAction func notificationAction(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "NotificationViewController") as! NotificationViewController
        
        self.navigationController?.pushViewController(myVC, animated: true)
    }
    
    
    
    @IBAction func logoutAction(_ sender: Any) {
      let alert = UIAlertController(title: "Logout", message: "Are you sure?", preferredStyle: .alert)
        let action1 = UIAlertAction(title: "Yes", style: .default) { (action) in
      let myVC = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        UserDefaults.standard.removeObject(forKey: UD_TOKEN)
        UserDefaults.standard.removeObject(forKey: UD_USER_EMAIL)
        UserDefaults.standard.removeObject(forKey: UD_USER_DETAIL)
        Singleton.shared.cardData = []
        Singleton.shared.paymentMethods = []
        Singleton.shared.currencyData = []
        Singleton.shared.walletData = []
        Singleton.shared.transactionData = []
        Singleton.shared.contactData = []
        Singleton.shared.countryData = []
        Singleton.shared.userDetail = SignupResponse()
      self.navigationController?.pushViewController(myVC, animated: true)
        }
        let action2 = UIAlertAction(title: "No", style: .cancel, handler: nil)
        
        alert.addAction(action1)
        alert.addAction(action2)
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func profileDetailAction(_ sender: Any) {
      
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "ProfileDetialViewController") as! ProfileDetialViewController
        myVC.profileData = self.profileData
        self.navigationController?.pushViewController(myVC, animated: true)
        
    }
    
    @IBAction func profileVerification(_ sender: Any) {
        if(Singleton.shared.userDocument.documents.count > 0){
            let myVC = self.storyboard?.instantiateViewController(withIdentifier: "ScanViewController") as! ScanViewController
            myVC.userDocument = Singleton.shared.userDocument
            self.navigationController?.pushViewController(myVC, animated: true)
        }else {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "ScanDetailViewController") as! ScanDetailViewController
        self.navigationController?.pushViewController(myVC, animated: true)
        }
    }
    
    @IBAction func backAction(_ sender: UIButton) {
        if(sender.tag == 2){
            self.qrView.transform = CGAffineTransform(scaleX: 0.5, y: 0.5)
            UIView.animate(withDuration: 0.4,
                           delay: 0,
                           usingSpringWithDamping: 0.6,
                           initialSpringVelocity: 8.0,
                           options: .curveEaseInOut,
                           animations: { [weak self] in
                            self?.qrView.isHidden = false
                            self?.qrView.transform = .identity
                           },
                           completion: nil)
        }else if(sender.tag == 3){
            UIView.animate(withDuration: 0.27, animations: {
                self.qrView.transform = CGAffineTransform(scaleX: 1.35, y: 1.35)
                self.qrView.alpha = 0.0
            }) { _ in
                
                self.qrView.isHidden = true
                self.qrView.alpha  = 1
            }
        }else {
        NavigationController.shared.pushDashboard(controller:self)
        }
        //self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func helpSupportAction(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "HelpSupportViewController") as! HelpSupportViewController
        self.navigationController?.pushViewController(myVC, animated: true)
    }
    
    @IBAction func referAppAction(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "ReferFriendViewController") as! ReferFriendViewController
        self.navigationController?.pushViewController(myVC, animated: true)
    }
    
    @IBAction func paymentMethodAction(_ sender: UIButton) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "AccountTabViewController") as! AccountTabViewController
        currentType = sender.tag
        self.navigationController?.pushViewController(myVC, animated: true)
    }
    
    @IBAction func qrScanAction(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "ScannerVC") as! ScannerVC
        myVC.scanDelegalte = self
        self.present(myVC, animated: true, completion: nil)
    }
}
