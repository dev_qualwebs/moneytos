//
//  QueryViewController.swift
//  Moneytoss
//
//  Created by qw on 20/02/21.
//  Copyright © 2021 qw. All rights reserved.
//

import UIKit

protocol UpdateQueryTable {
    func refreshQueryTable()
}

class QueryViewController: UIViewController {
    //MARK: IBOutlets
    @IBOutlet weak var queryText: UITextView!
    @IBOutlet weak var docImage: UIImageView!
    
    var imagePath = ""
    let picker = UIImagePickerController()
    var imageName = ""
    var queryDelegate: UpdateQueryTable? = nil

    override func viewDidLoad() {
        super.viewDidLoad()
        self.picker.delegate = self
    }
    
    
    //MARK: IBActions
    @IBAction func uploadImageAction(_ sender: Any) {
        let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
            self.openCamera()
        }))
        
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
            self.openGallary()
        }))
        
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        if let popoverController = alert.popoverPresentationController {
            popoverController.sourceView = sender as! UIView
            popoverController.sourceRect = (sender as AnyObject).bounds
        }
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func submitQueryAction(_ sender: Any) {
        if(self.queryText.text!.isEmpty){
            Singleton.shared.showToast(text: "Enter your query")
        }else {
            ActivityIndicator.show(view: self.view)
            
            let param:[String:Any]=[
                "query" : self.queryText.text,
                "image": self.imagePath
            ]
            
            SessionManager.shared.methodForApiCalling(url: U_BASE + U_ADD_SUPPORT_QUERY, method: .post, parameter: param, objectClass: SuccessResponse.self, requestCode: U_ADD_SUPPORT_QUERY) { (response) in
                Singleton.shared.showToast(text: "Query submitted successfully")
                self.queryDelegate?.refreshQueryTable()
                ActivityIndicator.hide()
                self.navigationController?.popViewController(animated: true)
            }
        }
    }
    
    
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
}


extension QueryViewController: UIImagePickerControllerDelegate,UINavigationControllerDelegate  {
    
    
    func imagePickerController(_ picker: UIImagePickerController,
                               didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]){
        guard let selectedImage = info[UIImagePickerController.InfoKey.editedImage] as? UIImage else { return }
        
        if let selectedImageName = ((info[UIImagePickerController.InfoKey.referenceURL] as? NSURL)?.lastPathComponent) {
            
            self.imageName = selectedImageName
        }else {
            self.imageName = "image.jpg"
        }
        let imageData:NSData = selectedImage.pngData()! as NSData
        
        //save in user default
        let strBase64 = imageData.base64EncodedString(options: .lineLength64Characters)
        if let cropImage = selectedImage as? UIImage {
            let data = cropImage.jpegData(compressionQuality: 0.2) as Data?
            let params = [
                "file": data!,
                ] as [String : Any]
            // HTTP Request for upload Picture
            ActivityIndicator.show(view: self.view)
            
            SessionManager.shared.makeMultipartRequest(url: U_BASE + U_UPLOAD_IMAGE, fileData: data!, param: params, fileName: imageName) { (path) in
                print(path)
                self.imagePath = (path as! UploadImage).response?.image ?? ""
              self.docImage.image = cropImage
            }
            ActivityIndicator.hide()
        }
        picker.dismiss(animated: true)
    }
    
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera))
        {
            picker.sourceType = UIImagePickerController.SourceType.camera
            picker.allowsEditing = true
            self.present(picker, animated: true, completion: nil)
        }
        else
        {
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func openGallary()
    {
        picker.sourceType = UIImagePickerController.SourceType.photoLibrary
        picker.allowsEditing = true
        self.present(picker, animated: true, completion: nil)
    }
    
    func presentCameraSettings() {
        let alertController = UIAlertController(title: "Allow Camera to take photos",
                                                message: "Camera access is denied",
                                                preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Cancel", style: .default){
            _ in
        })
        alertController.addAction(UIAlertAction(title: "Settings", style: .cancel) { _ in
            if let url = URL(string: UIApplication.openSettingsURLString) {
                UIApplication.shared.open(url, options: [:], completionHandler: { _ in
                    // Handle
                })
            }
        })
        self.present(alertController, animated: true)
    }

}
