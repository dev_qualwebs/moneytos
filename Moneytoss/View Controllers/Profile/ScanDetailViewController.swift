//
//  ScanDetailViewController.swift
//  Moneytoss
//
//  Created by qw on 27/09/20.
//  Copyright © 2020 qw. All rights reserved.
//

import UIKit
import SDWebImage


class ScanDetailViewController: UIViewController {
    //MARK: IBOutlets
    @IBOutlet weak var userFrontImage: ImageView!
    @IBOutlet weak var userBackImage: ImageView!
    @IBOutlet weak var documentType: DesignableUITextField!
    @IBOutlet weak var documentId: DesignableUITextField!
    
    var imagePath1 = ""
    var imagePath2 = ""
    let picker = UIImagePickerController()
    var imageName = ""
    var imageData:Data?
    var currentImage = 1
    var userDocument = UserDocumentResponse()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.picker.delegate = self
        if(self.userDocument.documents.count > 0){
            self.imagePath1 = self.userDocument.documents[0].document_path ?? ""
            self.imagePath2 = self.userDocument.documents[1].document_path ?? ""
            self.userFrontImage.sd_setImage(with: URL(string: self.getImageURL(text:self.imagePath1)), placeholderImage:nil)
            self.userBackImage.sd_setImage(with: URL(string: self.getImageURL(text:self.imagePath2)), placeholderImage:nil)
            self.documentType.text = self.userDocument.documents[0].document_type ?? ""
            self.documentId.text = self.userDocument.documents[0].document_number ?? ""
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
       
    }
    
    //MARK: IBAction
    @IBAction func submitAction(_ sender: Any) {
        if(self.imagePath1 != "" && self.imagePath2 != ""){
            NavigationController.shared.getUserDocument { response in
            let myVC = self.storyboard?.instantiateViewController(withIdentifier: "ScanViewController") as! ScanViewController
                myVC.userDocument = response
            self.navigationController?.pushViewController(myVC, animated: true)
            }
        }else {
            Singleton.shared.showToast(text: "Please upload image")
        }
    }
    
    
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func uploadImageAction(_ sender: UIButton) {
        self.currentImage = sender.tag
        
            let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
            alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
                self.openCamera()
            }))
            
            alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
                self.openGallary()
            }))
            
            alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
            if let popoverController = alert.popoverPresentationController {
                popoverController.sourceView = sender as! UIView
                popoverController.sourceRect = (sender as AnyObject).bounds
            }
            self.present(alert, animated: true, completion: nil)
        }
   
    
}

extension ScanDetailViewController: UIImagePickerControllerDelegate,UINavigationControllerDelegate  {
    
    
    func imagePickerController(_ picker: UIImagePickerController,
                               didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]){
        guard let selectedImage = info[UIImagePickerController.InfoKey.editedImage] as? UIImage else { return }
        
        if let selectedImageName = ((info[UIImagePickerController.InfoKey.referenceURL] as? NSURL)?.lastPathComponent) {
            
            self.imageName = selectedImageName
        }else {
            self.imageName = "image.jpg"
        }
        let imageData:NSData = selectedImage.pngData()! as NSData
        
        //save in user default
        let strBase64 = imageData.base64EncodedString(options: .lineLength64Characters)
        if let cropImage = selectedImage as? UIImage {
            let data = cropImage.jpegData(compressionQuality: 0.2) as Data?
            let params = [
                "file": data!,
            ] as [String : Any]
            self.imageData = data!
            
            if(self.imageData == nil){
                Singleton.shared.showToast(text: "Upload document image")
            }else if(self.documentType.text!.isEmpty){
                Singleton.shared.showToast(text: "Enter document type")
            }else if(self.documentId.text!.isEmpty){
                Singleton.shared.showToast(text: "Enter document number")
            }else {
                ActivityIndicator.show(view: self.view)
                let param:[String:Any]=[
                    "file":self.imageData,
                    "document_type":self.documentType.text ?? "",
                    "document_number":self.documentId.text ?? ""
                ]
                SessionManager.shared.makeMultipartRequest(url: U_BASE + U_SUBMIT_DOCUMENT, fileData: self.imageData!, param: param, fileName: imageName) { (path) in
                    ActivityIndicator.hide()
                    Singleton.shared.userDocument = UserDocumentResponse()
                    NavigationController.shared.getUserDocument { response in
                    }
                    Singleton.shared.showToast(text: "Successfully submitted response")
                    if(self.currentImage == 1){
                        self.userFrontImage.image = cropImage
                        self.imagePath1 = (path as? UploadImage)?.response?.image_path ?? ""
                    }else {
                        self.userBackImage.image = cropImage
                        self.imagePath2 = (path as? UploadImage)?.response?.image_path ?? ""
                    }

                }
            }
        }
        picker.dismiss(animated: true)
    }
    
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera))
        {
            picker.sourceType = UIImagePickerController.SourceType.camera
            picker.allowsEditing = true
            self.present(picker, animated: true, completion: nil)
        }
        else
        {
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func openGallary()
    {
        picker.sourceType = UIImagePickerController.SourceType.photoLibrary
        picker.allowsEditing = true
        self.present(picker, animated: true, completion: nil)
    }
    
    func presentCameraSettings() {
        let alertController = UIAlertController(title: "Allow Camera to take photos",
                                                message: "Camera access is denied",
                                                preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Cancel", style: .default){
            _ in
        })
        alertController.addAction(UIAlertAction(title: "Settings", style: .cancel) { _ in
            if let url = URL(string: UIApplication.openSettingsURLString) {
                UIApplication.shared.open(url, options: [:], completionHandler: { _ in
                    // Handle
                })
            }
        })
        self.present(alertController, animated: true)
    }
    
}
