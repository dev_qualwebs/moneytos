//
//  ScanViewController.swift
//  Moneytoss
//
//  Created by qw on 15/08/20.
//  Copyright © 2020 qw. All rights reserved.
//

import UIKit

class ScanViewController: UIViewController {
    //MARK:IBOutlets
    @IBOutlet weak var bigImage: UIImageView!
    @IBOutlet weak var smallImage: UIImageView!
    @IBOutlet weak var certStatus: DesignableUILabel!
    @IBOutlet weak var certDetail: DesignableUILabel!
    @IBOutlet weak var docType: DesignableUILabel!
    @IBOutlet weak var docNumber: DesignableUILabel!
    @IBOutlet weak var blueButton: CustomButton!
    
    @IBOutlet weak var documentReviewImage: ImageView!
    @IBOutlet weak var approvalImage: ImageView!
    @IBOutlet weak var docApprovalImage: ImageView!
    @IBOutlet weak var profileApprovalLabel: DesignableUILabel!
    @IBOutlet weak var documentReviewLabel: DesignableUILabel!
    @IBOutlet weak var submitApprovalLabel: DesignableUILabel!
    
    @IBOutlet weak var submitDateLabel: DesignableUILabel!
    @IBOutlet weak var underReviewDateLabel: DesignableUILabel!
    @IBOutlet weak var profileAcceptDateLabel: DesignableUILabel!
    
    @IBOutlet weak var noteLabel: DesignableUILabel!
    
    var userDocument = UserDocumentResponse()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.viewControllers.removeAll(where: { (vc) -> Bool in
            if vc.isKind(of: ScanDetailViewController.self) {
                return true
            } else {
                return false
            }
        })
        self.initializeView()
    }
    
    func initializeView(){
        
        let doc = self.userDocument.documents[0]
        self.docType.text = doc.document_type ?? ""
        self.docNumber.text = doc.document_number ?? ""
        if(doc.is_verified == 0){
            self.bigImage.image = UIImage(named: "shieldOrange")
            self.smallImage.image = UIImage(named: "timer")
            self.certStatus.text = "Account under review"
            self.certDetail.text = "Your account is under review, we'll notify about the status of your account."
            self.blueButton.setTitle("View attachments", for: .normal)
            self.submitDateLabel.text = self.convertTimestampToDate(doc.updated_at ?? 0, to: "MMM dd, yyyy h:mm a")
            self.underReviewDateLabel.text = self.convertTimestampToDate(doc.updated_at ?? 0, to: "MMM dd, yyyy h:mm a")
            self.profileAcceptDateLabel.text = "Pending"
            
            self.noteLabel.isHidden = false
            self.approvalImage.image = UIImage(named: "success_green 1")
            self.documentReviewImage.image = UIImage(named: "success_green 1")
            self.docApprovalImage.image = UIImage(named: "clocck_yellow")
        
        }else if(doc.is_verified == 1){
            self.bigImage.image = UIImage(named: "shieldGreen")
            self.smallImage.image = UIImage(named: "success_green 1")
            self.certStatus.text = "Approved"
            self.certDetail.text = "Your account is verified, your transaction restriction will be removed."
            self.blueButton.setTitle("Change documents", for: .normal)
            self.submitDateLabel.text = self.convertTimestampToDate(doc.created_at ?? 0, to: "MMM dd, yyyy h:mm a")
            self.underReviewDateLabel.text = self.convertTimestampToDate(doc.created_at ?? 0, to: "MMM dd, yyyy h:mm a")
            self.profileAcceptDateLabel.text = self.convertTimestampToDate(doc.updated_at ?? 0, to: "MMM dd, yyyy h:mm a")
            self.underReviewDateLabel.isHidden = false
            self.profileAcceptDateLabel.isHidden = false
            self.noteLabel.isHidden = true
            self.approvalImage.image = UIImage(named: "success_green 1")
            self.documentReviewImage.image = UIImage(named: "success_green 1")
            self.docApprovalImage.image = UIImage(named: "success_green 1")
            self.profileApprovalLabel.text = "Verification approved"
        }else if(doc.is_verified == 2){
            self.bigImage.image = UIImage(named: "shieldRed")
            self.smallImage.image = UIImage(named: "mycross")
            self.certStatus.text = "Rejected"
            self.certDetail.text = "Account verification failed."
            self.blueButton.setTitle("Submit again", for: .normal)
            self.submitDateLabel.text = self.convertTimestampToDate(doc.created_at ?? 0, to: "MMM dd, yyyy h:mm a")
            self.underReviewDateLabel.text = self.convertTimestampToDate(doc.created_at ?? 0, to: "MMM dd, yyyy h:mm a")
            self.profileAcceptDateLabel.text = self.convertTimestampToDate(doc.updated_at ?? 0, to: "MMM dd, yyyy h:mm a")
            self.underReviewDateLabel.isHidden = false
            self.profileAcceptDateLabel.isHidden = false
            self.noteLabel.isHidden = true
            self.approvalImage.image = UIImage(named: "success_green 1")
            self.documentReviewImage.image = UIImage(named: "success_green 1")
            self.docApprovalImage.image = UIImage(named: "cross_red")
            self.profileApprovalLabel.text = "Verificaton failed"
        }
        
    }
    
    
    //MARK: IBActions
    @IBAction func changeDocumentAction(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "ScanDetailViewController") as! ScanDetailViewController
        myVC.userDocument = self.userDocument
        self.navigationController?.pushViewController(myVC, animated: true)
    }
    
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
}
