//
//  SettingsViewController.swift
//  Moneytoss
//
//  Created by qw on 30/01/21.
//  Copyright © 2021 qw. All rights reserved.
//

import UIKit

class SettingsViewController: UIViewController {
    //MARK: IBOutlets
    

    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
   //MARK: IBActions
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func changeAction(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "ChangePasswordViewController") as! ChangePasswordViewController
        self.navigationController?.pushViewController(myVC, animated: true)
    }
    
}
