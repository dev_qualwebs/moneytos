//
//  UploadDocumentViewController.swift
//  Moneytoss
//
//  Created by Sagar Pandit on 27/08/21.
//  Copyright © 2021 qw. All rights reserved.
//

import UIKit

class UploadDocumentViewController: UIViewController {
    
    

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    //MARK: IBAction
    @IBAction func goBackAction(_ sender: UIButton) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "ScanViewController") as! ScanViewController
        self.navigationController?.pushViewController(myVC, animated: true)
    }
}
