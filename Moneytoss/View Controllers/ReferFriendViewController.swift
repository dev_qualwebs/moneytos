//
//  ReferFriendViewController.swift
//  Moneytoss
//
//  Created by qw on 27/01/21.
//  Copyright © 2021 qw. All rights reserved.
//

import UIKit

class ReferFriendViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
       

    }
    
    //MARK: IBActions
    @IBAction func shareAction(_ sender: Any) {
        if let urlStr = NSURL(string: "https://itunes.apple.com/us/app/myapp/id1542381572?ls=1&mt=8") {
            let objectsToShare = [urlStr]
            let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
            
            if UI_USER_INTERFACE_IDIOM() == .pad {
                if let popup = activityVC.popoverPresentationController {
                    popup.sourceView = self.view
                    popup.sourceRect = CGRect(x: self.view.frame.size.width / 2, y: self.view.frame.size.height / 4, width: 0, height: 0)
                }
            }
            activityVC.modalPresentationStyle = .overFullScreen
            self.present(activityVC, animated: true, completion: nil)
        }
    }
    
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func copyAction(_ sender: Any) {
        UIPasteboard.general.string = "https://itunes.apple.com/us/app/myapp/id1542381572?ls=1&mt=8"
       // Singleton.shared.showToast(text: "Link Copied")
    }
}
