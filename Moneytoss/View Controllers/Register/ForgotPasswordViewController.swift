//
//  ForgotPasswordViewController.swift
//  Moneytoss
//
//  Created by qw on 15/08/20.
//  Copyright © 2020 qw. All rights reserved.
//

import UIKit

class ForgotPasswordViewController: UIViewController {
    //MARK: IBOutlets
    @IBOutlet weak var emailField: FloatingPlaceholderTextField!
    @IBOutlet weak var otpField: FloatingPlaceholderTextField!
    @IBOutlet weak var passwordField: FloatingPlaceholderTextField!
    
    
    @IBOutlet weak var sendButton: CustomButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    //MARK: IBActions
    @IBAction func sendAction(_ sender: Any) {
        if(self.sendButton.titleLabel?.text == "Send"){
            if(self.emailField.textInput.text!.isEmpty){
                Singleton.shared.showToast(text: "Enter Email Address")
            }else if !(self.isValidEmail(emailStr: self.emailField.textInput.text ?? "")){
                Singleton.shared.showToast(text: "Enter valid Email Address")
            }else {
                ActivityIndicator.show(view: self.view)
                SessionManager.shared.methodForApiCalling(url: U_BASE + U_FORGOT_PASS, method: .post, parameter: ["email":self.emailField.textInput.text ?? ""], objectClass: SuccessResponse.self, requestCode: U_FORGOT_PASS) { (response) in
                    ActivityIndicator.hide()
                    self.otpField.isHidden = false
                    self.sendButton.setTitle("Verify OTP", for: .normal)
                    Singleton.shared.showToast(text: response.message ?? "")
                }
            }
        }else if(self.sendButton.titleLabel?.text == "Verify OTP"){
            if(self.otpField.textInput.text!.isEmpty){
                Singleton.shared.showToast(text: "Enter OTP")
            }else {
                ActivityIndicator.show(view: self.view)
                SessionManager.shared.methodForApiCalling(url: U_BASE + U_VERIRFY_FORGET_PASS_OTP, method: .post, parameter: ["email":self.emailField.textInput.text ?? "", "otp": self.otpField.textInput.text ?? ""], objectClass: SuccessResponse.self, requestCode: U_VERIRFY_FORGET_PASS_OTP) { (response) in
                    ActivityIndicator.hide()
                    self.sendButton.setTitle("Reset Password", for: .normal)
                    self.passwordField.isHidden = false
                    Singleton.shared.showToast(text: response.message ?? "")
                }
            }
        }else if(self.sendButton.titleLabel?.text == "Reset Password"){
            if(self.passwordField.textInput.text!.isEmpty){
                Singleton.shared.showToast(text: "Enter Password")
            }else {
                ActivityIndicator.show(view: self.view)
                SessionManager.shared.methodForApiCalling(url: U_BASE + U_RESET_PASSWORD, method: .post, parameter: ["email":self.emailField.textInput.text ?? "", "otp": self.otpField.textInput.text ?? "", "password": self.passwordField.textInput.text ?? "" ], objectClass: SuccessResponse.self, requestCode: U_RESET_PASSWORD) { (response) in
                    ActivityIndicator.hide()
                    Singleton.shared.showToast(text: response.message ?? "")
                    self.navigationController?.popViewController(animated: true)
                }
            }
        }
    }
    
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
}

