//
//  LoginViewController.swift
//  Moneytoss
//
//  Created by qw on 15/08/20.
//  Copyright © 2020 qw. All rights reserved.
//

import UIKit


var dontRememberMe = false

class LoginViewController: UIViewController {
    //MARK: IBOutlets
    @IBOutlet weak var signinLabel: DesignableUILabel!
    @IBOutlet weak var emailField: FloatingPlaceholderTextField!
    @IBOutlet weak var passwordField: FloatingPlaceholderTextField!
    @IBOutlet weak var loginButton: CustomButton!
    @IBOutlet weak var bottomText1: DesignableUILabel!
    @IBOutlet weak var bottomText2: DesignableUILabel!
    @IBOutlet weak var forgotPasswordText: DesignableUILabel!
    @IBOutlet weak var rememberView: View!
    @IBOutlet weak var loginView: UIView!
    
    var backButtonVisible = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.loginView.isHidden = backButtonVisible
        navigationController?.viewControllers.removeAll(where: { (vc) -> Bool in
            if vc.isKind(of: LoginViewController.self) {
                return false
            } else {
                return true
            }
        })
    }
    
    override func viewWillAppear(_ animated: Bool) {
       initView()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.signinLabel.text = ""
        self.passwordField.textInput.text = ""
    }
    
    func initView(){
        self.signinLabel.text = "LOGIN"
        self.passwordField.isSecureText = true
        self.loginButton.setTitle("Login", for: .normal)
        self.rememberView.borderWidth = 0.5
        self.rememberView.borderColor = primaryColor
        self.rememberView.backgroundColor = .white
    }
    
    //MARK: IBActions
    @IBAction func loginAction(_ sender: Any) {
        if(self.emailField.textInput.text!.isEmpty){
            Singleton.shared.showToast(text: "Enter Email Address.")
        }else if !(self.isValidEmail(emailStr: self.emailField.textInput.text ?? "")){
            Singleton.shared.showToast(text: "Enter valid Email Address.")
        }else if(self.passwordField.textInput.text!.isEmpty){
            Singleton.shared.showToast(text: "Enter password.")
        }else {
            ActivityIndicator.show(view: self.view)
            let token = UserDefaults.standard.value(forKey: UD_FCM_TOKEN) as? String
            let param:[String:Any] = [
                "email": self.emailField.textInput.text ?? "",
                "password": self.passwordField.textInput.text ?? "",
                "platform":2,
                "token": token ?? ""
            ]
            SessionManager.shared.methodForApiCalling(url: U_BASE + U_LOGIN, method: .post, parameter: param, objectClass: Signup.self, requestCode: U_LOGIN) { (response) in
                UserDefaults.standard.setValue(response.response.token, forKey: UD_TOKEN)
                Singleton.shared.userDetail = response.response
                if(self.rememberView.backgroundColor == primaryColor){
                  let userData = try! JSONEncoder().encode(response.response)
                  UserDefaults.standard.set(userData, forKey: UD_USER_DETAIL)
                }else {
                    dontRememberMe = true
                }
                ActivityIndicator.hide()
                let myVC = self.storyboard?.instantiateViewController(withIdentifier: "TabbarViewController") as! TabbarViewController
                self.navigationController?.pushViewController(myVC, animated: true)
            }
        }
    }
    
    @IBAction func forgotPasswordAction(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "ForgotPasswordViewController") as! ForgotPasswordViewController
        
        self.navigationController?.pushViewController(myVC, animated: true)
    }
    
    @IBAction func registerAction(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "SignupViewController") as! SignupViewController
        self.navigationController?.pushViewController(myVC, animated: true)
    }
    
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func rememberAction(_ sender: Any) {
        if(self.rememberView.backgroundColor == .white){
           self.rememberView.backgroundColor = primaryColor
           self.rememberView.borderColor = primaryColor
        }else{
        self.rememberView.backgroundColor = .white
        self.rememberView.borderColor = primaryColor
        }
    }
    
    @IBAction func showPasswordAction(_ sender: Any) {
        if(self.passwordField.isSecureText){
            self.passwordField.isSecureText = false
        }else{
            self.passwordField.isSecureText = true
        }
    }
}

extension LoginViewController: UITextFieldDelegate {
   
}
