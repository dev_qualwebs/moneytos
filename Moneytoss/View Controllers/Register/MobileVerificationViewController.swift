//
//  MobileVerificationViewController.swift
//  Moneytoss
//
//  Created by qw on 15/08/20.
//  Copyright © 2020 qw. All rights reserved.
//

import UIKit

class MobileVerificationViewController: UIViewController {
    //MARK: IBOutlets
    @IBOutlet weak var number1: DesignableUITextField!
    @IBOutlet weak var number2: DesignableUITextField!
    @IBOutlet weak var number3: DesignableUITextField!
    @IBOutlet weak var number4: DesignableUITextField!
    @IBOutlet weak var number5: DesignableUITextField!
    @IBOutlet weak var headingLabel: DesignableUILabel!
    @IBOutlet weak var numberLabel: DesignableUILabel!
    @IBOutlet weak var keypadView: UIView!
    @IBOutlet weak var backView: UIView!
    
    
    
    var numArr = ["", "", "", "", "", ""]
    var email = String()
    var position = 1
    var isComingDirect = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initView()
        if(isComingDirect){
        self.sendOtp()
        }
        DispatchQueue.main.async {
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
     self.keypadView.bounceAnimationView(view:self.keypadView)
    }
    
    func sendOtp() {
        ActivityIndicator.show(view: self.view)
        SessionManager.shared.methodForApiCalling(url: U_BASE +     U_SEND_OTP, method: .post, parameter: ["email":self.email], objectClass: SuccessResponse.self, requestCode: U_SEND_OTP) { (response) in
            ActivityIndicator.hide()
            Singleton.shared.showToast(text: response.message ?? "")
        }
    }
    
    func verifyOtp(){
        
        var number = (self.number1.text ?? "") + (self.number2.text ?? "")
        number += (self.number3.text ?? "") + (self.number4.text ?? "")
        number += (self.number5.text ?? "")
        if(number.count < 5){
            Singleton.shared.showToast(text: "Enter correct OTP")
        }else {
            ActivityIndicator.show(view: self.view)
            SessionManager.shared.methodForApiCalling(url: U_BASE + U_VERIFY_OTP, method: .post, parameter: ["email":self.email, "otp":number], objectClass: Signup.self, requestCode: U_VERIFY_OTP) { (response) in
                UserDefaults.standard.setValue(response.response.token, forKey: UD_TOKEN)
                let userData = try! JSONEncoder().encode(response.response)
                UserDefaults.standard.set(userData, forKey: UD_USER_DETAIL)
                ActivityIndicator.hide()
                let myVC = self.storyboard?.instantiateViewController(withIdentifier: "TabbarViewController") as! TabbarViewController
                UserDefaults.standard.removeObject(forKey: UD_INITIAL_VIEW_CONTROLLER)
                UserDefaults.standard.removeObject(forKey: UD_USER_EMAIL)
                self.navigationController?.pushViewController(myVC, animated: true)
                Singleton.shared.showToast(text: response.message ?? "")
            }
        }
    }
    
    func initView(){
        self.number1.text = ""
        self.number1.delegate = self
        self.number2.text = ""
        self.number2.delegate = self
        self.number3.text = ""
        self.number3.delegate = self
        self.number4.text = ""
        self.number4.delegate = self
        self.numberLabel.text = "We've  sent 5 digit code to \(self.email)"
        if(self.isComingDirect){
            self.backView.isHidden = false
        }else {
            self.backView.isHidden = true
        }
    }
    
    
    func handleText() {
        for val in self.numArr {
            // self.vehicleNumber.append(val)
        }
    }
    
    func handleTextChange(val: String){
        if(position == 1){
            number1.text = val
            self.position  += 1
        }else if(position == 2){
            number2.text = val
            self.position  += 1
        }else if(position == 3){
            number3.text = val
            self.position  += 1
        }else if(position == 4){
            number4.text = val
            self.position  += 1
        }else if(position == 5){
            number5.text = val
        }
    }
    
    //MARK:IBActions
    @IBAction func oneAction(_ sender: Any) {
        self.handleTextChange(val: "1")
    }
    
    @IBAction func twoAction(_ sender: Any) {
        self.handleTextChange(val: "2")
    }
    
    @IBAction func threeAction(_ sender: Any) {
        self.handleTextChange(val: "3")
    }
    
    @IBAction func fourAction(_ sender: Any) {
        self.handleTextChange(val: "4")
    }
    
    @IBAction func fiveAction(_ sender: Any) {
        self.handleTextChange(val: "5")
    }
    
    @IBAction func sixAction(_ sender: Any) {
        self.handleTextChange(val: "6")
    }
    
    @IBAction func sevenAction(_ sender: Any) {
        self.handleTextChange(val: "7")
    }
    
    @IBAction func eightAction(_ sender: Any) {
        self.handleTextChange(val: "8")
    }
    
    @IBAction func nineAction(_ sender: Any) {
        self.handleTextChange(val: "9")
    }
    
    @IBAction func zeroAction(_ sender: Any) {
        self.handleTextChange(val: "0")
    }
    
    @IBAction func clearAction(_ sender: Any) {
        self.numArr = []
        self.number1.text = ""
        self.number2.text = ""
        self.number3.text = ""
        self.number4.text = ""
        self.number5.text = ""
        self.position = 1
    }
    
    
    @IBAction func nextAction(_ sender: Any) {
        self.verifyOtp()
    }
    
    @IBAction func backAction(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        self.navigationController?.pushViewController(myVC, animated: true)
        
    }
    
    
}

extension MobileVerificationViewController: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        
        if let char = string.cString(using: String.Encoding.utf8) {
            let isBackSpace = strcmp(char, "\\b")
            if (isBackSpace == -92 || textField.text?.count == 0 ) {
                return true
            }else {
                return false
            }
        }
        return true
    }
}
