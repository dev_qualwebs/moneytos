//
//  SignupViewController.swift
//  Moneytoss
//
//  Created by qw on 15/08/20.
//  Copyright © 2020 qw. All rights reserved.
//

import UIKit
import CountryPickerView

class SignupViewController: UIViewController, UITextFieldDelegate,SelectFromPicker {
    func selectedItem(name: String, id: Int) {
        if(currentPicker == 1){
            self.countryCodeField.textInput?.text = name
            self.cityField.textInput?.text = ""
            self.getCityData(id: self.countries[id].id ?? 0)
            self.selectedCity = Countires()
            self.selectedCountry = countries[id]
        }else if(self.currentPicker == 2){
            self.cityField.textInput?.text = name
            self.selectedCity = cities[id]
        }
    }
    
    //MARK: IBOutlets
    @IBOutlet weak var signupLabel: DesignableUILabel!
    
    @IBOutlet weak var firstName: FloatingPlaceholderTextField!
    @IBOutlet weak var lastName: FloatingPlaceholderTextField!
    @IBOutlet weak var emailField: FloatingPlaceholderTextField!
    @IBOutlet weak var numberField: FloatingPlaceholderTextField!
    @IBOutlet weak var passwordField: FloatingPlaceholderTextField!
    @IBOutlet weak var referralCode: FloatingPlaceholderTextField!
    
    @IBOutlet weak var countryCodeField: FloatingPlaceholderTextField!
    @IBOutlet weak var cityField: FloatingPlaceholderTextField!
    
    @IBOutlet weak var countryPickerView: CountryPickerView!
    @IBOutlet weak var registerButton: CustomButton!
    @IBOutlet weak var termsText: DesignableUILabel!
    
    var currentPicker = 1
    var countries: [Countires] = []
    var cities: [Countires] = []
    var selectedCity = Countires()
    var selectedCountry = Countires()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initView()
        NavigationController.shared.getCountryData { (response) in
            self.countries = response
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        initView()
    }
    
    func initView(){
        self.signupLabel.text = "SIGN UP"
        self.registerButton.setTitle("Sign Up", for: .normal)
        self.numberField.textInput?.delegate = self
    }
    
    
    func getCityData(id: Int){
        ActivityIndicator.show(view: self.view)
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_GET_CITIES + "\(id)", method: .get, parameter: nil, objectClass: GetCities.self, requestCode: U_GET_CITIES) { (response) in
            self.cities = response.response.cities
            ActivityIndicator.hide()
        }
    }
    
    //MARK: IBActions
    @IBAction func registerAction(_ sender: Any) {
        if(self.firstName.textInput.text!.isEmpty){
            Singleton.shared.showToast(text: "Enter First Name.")
        }else if(self.lastName.textInput.text!.isEmpty){
            Singleton.shared.showToast(text: "Enter Last Name.")
        }else if(self.emailField.textInput.text!.isEmpty){
            Singleton.shared.showToast(text: "Enter Email Address.")
        }else if !(self.isValidEmail(emailStr: self.emailField.textInput.text ?? "")){
            Singleton.shared.showToast(text: "Enter valid Email Address.")
        }else if(countryCodeField.textInput.text!.isEmpty){
            Singleton.shared.showToast(text: "Select Country")
        }else if(countryCodeField.textInput.text!.isEmpty){
            Singleton.shared.showToast(text: "Select City")
        }else if(self.numberField.textInput.text!.isEmpty){
            Singleton.shared.showToast(text: "Enter Phone Number.")
        }else if(self.passwordField.textInput.text!.isEmpty){
             Singleton.shared.showToast(text: "Enter password.")
        }else{
            ActivityIndicator.show(view: self.view)
            let number = self.numberField.textInput.text?.replacingOccurrences(of: "-", with: "")
            let token = UserDefaults.standard.value(forKey: UD_FCM_TOKEN) as? String
            let param:[String:Any] = [
                "first_name": self.firstName.textInput.text ?? "",
                "last_name": self.lastName.textInput.text ?? "",
                "email": self.emailField.textInput.text ?? "",
                "phone_number":number,
                "password": self.passwordField.textInput.text ?? "",
                "referal_id":self.referralCode.textInput.text ?? "",
                "platform":2,
                "token":token ?? "",
                "country_id" : self.selectedCountry.id,
                "city_id" : self.selectedCity.id

            ]
            SessionManager.shared.methodForApiCalling(url: U_BASE + U_REGISTER, method: .post, parameter: param, objectClass: SuccessResponse.self, requestCode: U_REGISTER) { (response) in
                let myVC = self.storyboard?.instantiateViewController(withIdentifier: "MobileVerificationViewController") as! MobileVerificationViewController
                UserDefaults.standard.set(self.emailField.textInput.text ?? "", forKey: UD_USER_EMAIL)
                UserDefaults.standard.set("MobileVerificationViewController", forKey: UD_INITIAL_VIEW_CONTROLLER)
                myVC.email = self.emailField.textInput.text ?? ""
                self.navigationController?.pushViewController(myVC, animated: true)
            }
        }
    }
    
    @IBAction func loginAction(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        myVC.backButtonVisible = false
               self.navigationController?.pushViewController(myVC, animated: true)
    }
    
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func countryPickerAction(_ sender: UIButton) {

        self.currentPicker = sender.tag
        
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "PickerViewController") as! PickerViewController
        if(sender.tag == 1){
            for val in self.countries{
                myVC.pickerData.append(val.country ?? "")
            }
        }else if(sender.tag == 2){
            for val in self.cities{
                myVC.pickerData.append(val.city ?? "")
            }
        }
        
        myVC.modalPresentationStyle = .overFullScreen
        myVC.pickerDelegate = self
        if(myVC.pickerData.count > 0){
            self.navigationController?.present(myVC, animated: true, completion: nil)
        }
    }
    
    @IBAction func tdAction(_ sender: Any) {
        self.openUrl(urlStr: U_TERMS_CONDITION)
    }
    
}
