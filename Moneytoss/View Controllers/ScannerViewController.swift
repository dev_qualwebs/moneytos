//
//  ScanViewController.swift
//  Moneytoss
//
//  Created by qw on 07/09/20.
//  Copyright © 2020 qw. All rights reserved.
//

import UIKit
import AVFoundation

public class ScannerViewController: UIViewController {
    //MARK: IBoutlets

    
    
    var isScanPay = false
    
    public lazy var headerViewController:HeaderVC = .init()
    
    public lazy var cameraViewController:CameraVC = .init()
    
    /// 动画样式
    public var animationStyle:ScanAnimationStyle = .default{
        didSet{
            cameraViewController.animationStyle = animationStyle
        }
    }
    
    // 扫描框颜色
    public var scannerColor:UIColor = .white{
        didSet{
            cameraViewController.scannerColor = scannerColor
        }
    }
    
    public var scannerTips:String = ""{
        didSet{
            cameraViewController.scanView.tips = scannerTips
        }
    }
    
    /// `AVCaptureMetadataOutput` metadata object types.
    public var metadata = AVMetadataObject.ObjectType.metadata {
        didSet{
            cameraViewController.metadata = metadata
        }
    }
    
    public var successBlock:((String)->())?
    
    public var errorBlock:((Error)->())?
    
    
    /// 设置标题
    public override var title: String?{
        
        didSet{
            
            if navigationController == nil {
                headerViewController.title = title
            }
        }
        
    }
    
    
    /// 设置Present模式时关闭按钮的图片
    public var closeImage: UIImage?{
        
        didSet{
            
            if navigationController == nil {
                headerViewController.closeImage = closeImage ?? UIImage()
            }
        }
        
    }
    
    override public func viewDidLoad() {
        super.viewDidLoad()
    }
    
    
    public override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        cameraViewController.startCapturing()
        self.view.bringSubviewToFront(cameraViewController.view)
    }
    
}

// MARK: - CustomMethod
extension ScannerViewController{
    
    func setupUI() {
        
        if title == nil {
            title = "itreat Pay"
        }
        
        view.backgroundColor = primaryColor
        
        headerViewController.delegate = self
        
        cameraViewController.metadata = metadata
        
        cameraViewController.animationStyle = animationStyle
        
        cameraViewController.delegate = self
        
        add(cameraViewController)
        
        if navigationController == nil {
            //            add(headerViewController)
            //            view.bringSubviewToFront(headerViewController.view)
        }
        
        
    }
    
    
    public func setupScanner(_ title:String? = nil, _ color:UIColor? = nil, _ style:ScanAnimationStyle? = nil, _ tips:String? = nil, _ success:@escaping ((String)->())){
        
        if title != nil {
            self.title = title
        }
        
        if color != nil {
            scannerColor = color!
        }
        
        if style != nil {
            animationStyle = style!
        }
        
        if tips != nil {
            scannerTips = tips!
        }
        
        successBlock = success
        
    }
}

// MARK: - HeaderViewControllerDelegate
extension ScannerViewController:HeaderViewControllerDelegate{
    
    
    /// 点击关闭
    public func didClickedCloseButton() {
        dismiss(animated: true, completion: nil)
        
    }
    
}


extension ScannerViewController:CameraViewControllerDelegate{
    func didOutput(_ code: String) {
        successBlock?(code)
        self.dismiss(animated: true, completion: nil)
    }
    
    func didReceiveError(_ error: Error) {
        errorBlock?(error)
    }
}
