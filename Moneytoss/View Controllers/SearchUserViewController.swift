//
//  SearchUserViewController.swift
//  Moneytoss
//
//  Created by qw on 12/11/20.
//  Copyright © 2020 qw. All rights reserved.
//



import UIKit

protocol SelectUser {
    func selectedUser(user:SearchUserResponse)
}

class SearchUserViewController: UIViewController {
    //MARK: IBOutlets
    @IBOutlet weak var userTable: ContentSizedTableView!
    @IBOutlet weak var searchText: DesignableUITextField!
    
   var userData = [SearchUserResponse]()
    var searchDelegate:SelectUser? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.searchText.delegate = self
        self.searchText.becomeFirstResponder()  
    }
    
    func searchUser(name: String){
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_SEARCH_USER, method: .post, parameter: ["first_name": name], objectClass: SearchUser.self, requestCode: U_SEARCH_USER) { (response) in
            self.userData = response.response
            self.userTable.reloadData()
        }
    }
}

extension SearchUserViewController: UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return userData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TransactionTableCell") as! TransactionTableCell
        let val = self.userData[indexPath.row]
        if((val.profile_image ?? "").contains("http")){
            cell.userImage.sd_setImage(with: URL(string: self.getImageURL(text:val.profile_image ?? "")), placeholderImage: #imageLiteral(resourceName: "round_image"))
        }else {
            cell.userImage.sd_setImage(with: URL(string: self.getImageURL(text:U_IMAGE_BASE + (val.profile_image ?? ""))), placeholderImage: #imageLiteral(resourceName: "round_image"))
        }
       
        cell.userName.text = val.first_name ?? val.username
        cell.transactionDate.text = (val.phone_number ?? "").applyPatternOnNumbers(pattern: "###-###-####", replacmentCharacter: "#")
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.searchDelegate?.selectedUser(user: self.userData[indexPath.row])
        self.dismiss(animated: false, completion: nil)
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if(textField == searchText){
            let text = (self.searchText.text ?? "") + string
            self.searchUser(name:text)
        }
        return true
    }
   
}
