//
//  SinglePageViewController.swift
//  Moneytoss
//
//  Created by qw on 15/08/20.
//  Copyright © 2020 qw. All rights reserved.
//

import UIKit

protocol PageContentIndexDelegate {
    func getContentIndex(index: Int)
}

class SinglePageViewController: UIPageViewController {
    
    static var dataSource1: UIPageViewControllerDataSource?
    static var indexDelegate: PageContentIndexDelegate? = nil
    
    var currentIndex: Int = 0
    var arrayContent: [SplashContent]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        arrayContent = [SplashContent]()
        arrayContent = [ SplashContent(backgroundImage: "mt_slide1", title: "GET STARTED", content: "Transfer Anywhere", subTitle: "Create your account and transfer money anywhere"),SplashContent(backgroundImage: "3926929", title: "GET STARTED", content: "Transfer Anywhere", subTitle: "Create your account and transfer money anywhere"),SplashContent(backgroundImage: "4024189", title: "GET STARTED", content: "Transfer Anywhere", subTitle: "Create your account and transfer money anywhere")
        ]
        
        self.dataSource = self
        self.delegate = self
        
        SinglePageViewController.dataSource1 = self
        self.navigationController?.isNavigationBarHidden = true
        //self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        getContent()
        self.navigationController?.isNavigationBarHidden = true
        SinglePageViewController.dataSource1 = self
    }
    
    func getContent() {
        self.setViewControllers([contentAtIndex(index: currentIndex)!], direction: .forward, animated: false, completion: nil)
    }
    
    func contentAtIndex(index: Int) -> UIViewController? {
        if (arrayContent?.count == 0) || (index >= (arrayContent?.count)!) {
            return nil
        }
        let splashContent = self.storyboard?.instantiateViewController(withIdentifier: "SplashContentViewController") as! SplashContentViewController
        guard let content = arrayContent as? [SplashContent] else { return nil }
        splashContent.content = content[index]
        splashContent.index = index
        return splashContent
    }
    
    func viewAtIndex(_ viewController: UIViewController, next: Bool) -> UIViewController? {
        var index:Int = 0
        let pageContent: SplashContentViewController = viewController as! SplashContentViewController
        index = pageContent.index
        if next {
            if (index == NSNotFound) {
                return nil
            }
            index += 1
            if index == arrayContent?.count {
                return nil
            }
        } else {
            if ((index == 0) || (index == NSNotFound)) {
                return nil
            }
            index -= 1
        }
        return contentAtIndex(index: index)
    }
}
extension SinglePageViewController: UIPageViewControllerDataSource {
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        return viewAtIndex(viewController, next: false)
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        return viewAtIndex(viewController, next: true)
    }
}
extension SinglePageViewController: UIPageViewControllerDelegate {
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        
        let currentVC: SplashContentViewController = pageViewController.viewControllers?[0] as! SplashContentViewController
        currentIndex = currentVC.index
        SinglePageViewController.indexDelegate?.getContentIndex(index: currentIndex)
    }
}

extension SinglePageViewController {
    func goToPage(animated: Bool = true, completion: ((Bool) -> Void)? = nil, next: Bool) {
        if let currentViewController = viewControllers?[0] {
            if next{
                if let nextPage = SinglePageViewController.dataSource1?.pageViewController(self, viewControllerAfter: currentViewController) {
                    SinglePageViewController.indexDelegate?.getContentIndex(index: currentIndex)
                    setViewControllers([nextPage], direction: .forward, animated: animated, completion: completion)
                }
            }else{
                if let previousPage = SinglePageViewController.dataSource1?.pageViewController(self, viewControllerBefore: currentViewController) {
                    setViewControllers([previousPage], direction: .reverse, animated: animated, completion: completion)
                }
            }
        }
    }
    
    func removeSwipeGesture(){
        for view in self.view.subviews {
            if let subView = view as? UIScrollView {
                subView.isScrollEnabled = false
            }
        }
    }
}
