//
//  SplashContentViewController.swift
//  Moneytoss
//
//  Created by qw on 15/08/20.
//  Copyright © 2020 qw. All rights reserved.
//
import UIKit

class SplashContentViewController: UIViewController {
    
    //MARK: IBOutlets
    @IBOutlet weak var currentImage: UIImageView!
    @IBOutlet weak var labelTitle: DesignableUILabel!
    @IBOutlet weak var labelContent: DesignableUILabel!
    @IBOutlet weak var labelSubtitle: DesignableUILabel!
    
    
    var content: SplashContent?
    var index: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let content = self.content {
            currentImage.image = UIImage(named: content.backgroundImage + ".png")
            labelTitle.text = content.title
            labelContent.text = content.content
            labelSubtitle.text = content.subTitle
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
}
