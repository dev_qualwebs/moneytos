//
//  SplashViewController.swift
//  Moneytoss
//
//  Created by qw on 15/08/20.
//  Copyright © 2020 qw. All rights reserved.
//

import UIKit

class SplashViewController: UIViewController, PageContentIndexDelegate {
    
    //MARK: IBOutlets
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var loginButton: CustomButton!
    @IBOutlet weak var registerButton: CustomButton!
    
    
    var currentIndex: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        SinglePageViewController.indexDelegate = self
        initView()
    }

    func initView(){
        self.loginButton.setTitle("Login", for: .normal)
        self.registerButton.setTitle("Sign Up", for: .normal)
    }
    
    func getContentIndex(index: Int) {
        pageControl.currentPage = (currentIndex == 0) ? index:currentIndex
    }
    //MARK: IBActions
    @IBAction func changePage(_ sender: Any) {
        if(pageControl.currentPage == 0){
            let page = SinglePageViewController.dataSource1 as! SinglePageViewController
            page.currentIndex = 1
            page.goToPage(next: true)
        }else if(pageControl.currentPage == 1){
            let page = SinglePageViewController.dataSource1 as! SinglePageViewController
            page.currentIndex = 2
            page.goToPage(next: true)
        }
    }
    
    @IBAction func loginAction(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        myVC.backButtonVisible = false
        self.navigationController?.pushViewController(myVC, animated: true)
        
    }
    
    @IBAction func registerAction(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "SignupViewController") as! SignupViewController
        self.navigationController?.pushViewController(myVC, animated: true)
    }
}

