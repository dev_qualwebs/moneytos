//
//  SuccessScreenViewController.swift
//  Moneytoss
//
//  Created by qw on 16/08/20.
//  Copyright © 2020 qw. All rights reserved.
//

import UIKit
import TransitionButton

class SuccessScreenViewController: CustomTransitionViewController {
    
    //MARK:IBOutlets
    @IBOutlet weak var amount: DesignableUILabel!
    @IBOutlet weak var mode: DesignableUILabel!
    @IBOutlet weak var receiverName: DesignableUILabel!
    @IBOutlet weak var receiverNumber: DesignableUILabel!
    @IBOutlet weak var country: DesignableUILabel!
    @IBOutlet weak var serviceStation: DesignableUILabel!
    
    
    var param = [String: Any]()
    var userDetail = SearchUserResponse()
    var pdfUrl: String?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initView()
    }
    
    func initView(){
        self.amount.text = param["amount"] as! String
        if((param["send_from"] as! Int) == 1){
            self.mode.text = "Wallet"
        }else {
            self.mode.text = "Card"
        }
        
        self.receiverName.text = (self.userDetail.first_name ?? "") + " " + (self.userDetail.last_name ?? "")
        self.receiverNumber.text = self.userDetail.phone_number
        self.country.text = self.userDetail.country_name
        self.serviceStation.text = self.userDetail.service_station
        
    }
    
    func saveFile(contents: [URL],dUrl:URL){
       
        for indexx in 0..<contents.count {
            if contents[indexx].lastPathComponent == dUrl.lastPathComponent {
                let activityViewController = UIActivityViewController(activityItems: [contents[indexx]], applicationActivities: nil)
                self.navigationController?.present(activityViewController, animated: true, completion: nil)
            }
        }

    }
    
    //MARK: IBActions
    @IBAction func doneAction(_ sender: UIButton) {
        Singleton.shared.transactionData = []
        Singleton.shared.walletData = []
        Singleton.shared.contactData = []
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "TabbarViewController") as! TabbarViewController
        NavigationController.shared.addTransition(direction:.fromTop, controller: self)
        if(sender.tag == 2){
            myVC.isNewPayment = true
        }
        self.navigationController?.pushViewController(myVC, animated: false)
    }
    
    @IBAction func downloadReceiptAction(_ sender: Any) {
        ActivityIndicator.show(view: self.view)
        (sender as? UIButton)?.isUserInteractionEnabled = false
        Singleton.shared.showToast(text: "Downloading pdf")
        if let url = URL(string:self.pdfUrl ?? ""){
            
            let fileName = String((url.lastPathComponent)) as NSString
            // Create destination URL
            let documentsUrl:URL =  (FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first as URL?)!
            let destinationFileUrl = documentsUrl.appendingPathComponent("\(fileName)")
            //Create URL to the source file you want to download
            let fileURL = URL(string: self.pdfUrl ?? "")
            let sessionConfig = URLSessionConfiguration.default
            let session = URLSession(configuration: sessionConfig)
            let request = URLRequest(url:fileURL!)
             let task = session.downloadTask(with: request) { (tempLocalUrl, response, error) in
                if let tempLocalUrl = tempLocalUrl, error == nil {
                    // Success
                    (sender as? UIButton)?.isUserInteractionEnabled = true
                    if let statusCode = (response as? HTTPURLResponse)?.statusCode {
                        ActivityIndicator.hide()
                        print("Successfully downloaded. Status code: \(statusCode)")
                    }
                    do {
                        try FileManager.default.copyItem(at: tempLocalUrl, to: destinationFileUrl)
                        do {
                            Singleton.shared.showToast(text: "PDF file downloaded successfully.")
                            //Show UIActivityViewController to save the downloaded file
                            let contents  = try FileManager.default.contentsOfDirectory(at: documentsUrl, includingPropertiesForKeys: nil, options: .skipsHiddenFiles)
                            ActivityIndicator.hide()
                            DispatchQueue.main.async {
                            self.saveFile(contents: contents,dUrl: destinationFileUrl)
                            }
                        }
                        catch (let err) {
                            ActivityIndicator.hide()
                            (sender as? UIButton)?.isUserInteractionEnabled = true
                             Singleton.shared.showToast(text: "Error downloading file")
                            print("error: \(err)")
                        }
                    } catch (let writeError) {
                        ActivityIndicator.hide()
                       (sender as? UIButton)?.isUserInteractionEnabled = true
                        Singleton.shared.showToast(text: "File already exist")
                        print("Error creating a file \(destinationFileUrl) : \(writeError)")
                    }
                } else {
                    ActivityIndicator.hide()
                   (sender as? UIButton)?.isUserInteractionEnabled = true
                    Singleton.shared.showToast(text: "Error downloading file")
                    print("Error took place while downloading a file. Error description: \(error?.localizedDescription ?? "")")
                }
            }
            task.resume()
        }else {
            ActivityIndicator.hide()
            Singleton.shared.showToast(text: "No URL Found.")
        }
    }
    
    @IBAction func shareAction(_ sender: Any) {
        if let urlStr = NSURL(string: self.pdfUrl ?? "") {
            let objectsToShare = [urlStr]
            let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
            
            if UI_USER_INTERFACE_IDIOM() == .pad {
                if let popup = activityVC.popoverPresentationController {
                    popup.sourceView = self.view
                    popup.sourceRect = CGRect(x: self.view.frame.size.width / 2, y: self.view.frame.size.height / 4, width: 0, height: 0)
                }
            }
            activityVC.modalPresentationStyle = .overFullScreen
            self.present(activityVC, animated: true, completion: nil)
        }
    }
}
