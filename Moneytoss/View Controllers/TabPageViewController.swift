//
//  TabPageViewController.swift
//  ITreat
//
//  Created by qw on 09/07/20.
//  Copyright © 2020 qw. All rights reserved.
//
import UIKit

protocol ControllerIndexDelegate {
    func getControllerIndex(index: Int)
}

class TabPageViewController: UIPageViewController
{
    static var dataSource1: UIPageViewControllerDataSource?
    static var index_delegate: ControllerIndexDelegate? = nil
    //    static var controller: String = S_MY_BOOK
    var firstControllerPart: [UIViewController] = []
    var dashboardViewControllers: [UIViewController] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        dataSource = self
        handleView()
        setControllers()
        self.removeSwipeGesture()
        TabPageViewController.dataSource1 = self
        NotificationCenter.default.addObserver(self, selector: #selector(self.handleBottomView), name: NSNotification.Name(N_BOTTOM_NAVIGATION_OPTION), object: nil)
    }
    
    @objc func handleBottomView(){
        self.handleView()
    }
    
    func removeSwipeGesture(){
        for view in self.view.subviews {
            if let subView = view as? UIScrollView {
                subView.isScrollEnabled = false
            }
        }
    }
    
    func setControllers() {
        if let lastViewController = dashboardViewControllers.last {
            setViewControllers([lastViewController], direction: .forward, animated: true, completion: nil)
        }
        setViewControllers([dashboardViewControllers[2]], direction: .forward, animated: true, completion: nil)
        setViewControllers([dashboardViewControllers[1]], direction: .forward, animated: true, completion: nil)
        if let firstViewController = dashboardViewControllers.first {
            setViewControllers([firstViewController], direction: .forward, animated: true, completion: nil)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
    }
    
    override func viewDidAppear(_ animated: Bool) {
        TabPageViewController.dataSource1 = self
    }
    
    func handleView() {
      dashboardViewControllers = [self.newColoredViewController(controller: "HomeViewController"),
                                        self.newColoredViewController(controller: "TrasnasctionViewController"),
                                        self.newColoredViewController(controller: "SendViewController"),
                                        self.newColoredViewController(controller: "ContactViewController"),
                                        self.newColoredViewController(controller: "WalletViewController")]
       // self.setControllerFirst()
    }
    
    func setControllerFirst() {
        if let firstViewController = dashboardViewControllers.first {
            setViewControllers([firstViewController],
                               direction: .reverse,
                               animated: false,
                               completion: nil)
        }
    }
    
    func setControllerSecond() {
        setViewControllers([dashboardViewControllers[1]], direction: .forward, animated: false, completion: nil)
    }
    
    func setControllerThird() {
        setViewControllers([dashboardViewControllers[2]], direction: .forward, animated: false, completion: nil)
    }
    
    func setControllerFourth() {
           setViewControllers([dashboardViewControllers[3]], direction: .forward, animated: false, completion: nil)
       }
    
    func setControllerLast() {
        if let lastViewController = dashboardViewControllers.last {
            setViewControllers([lastViewController],
                               direction: .forward,
                               animated: false,
                               completion: nil)
        }
    }
    
    func currentControllerIndex(VC: UIViewController) {
        if let viewControllerIndex = dashboardViewControllers.index(of: VC) {
            TabPageViewController.index_delegate?.getControllerIndex(index: viewControllerIndex)
        }
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     return [self.newColoredViewController(controller: "FindRideViewController"),
     self.newColoredViewController(controller: "OfferRideViewController")]
     // Pass the selected object to the new view controller.
     
     }
     */
    //    private(set) lazy
    private func newColoredViewController(controller: String)->UIViewController {
        return UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: controller)
    }
}

extension TabPageViewController: UIPageViewControllerDataSource
{
    func pageViewController(_ pageViewController: UIPageViewController,
                            viewControllerBefore viewController: UIViewController) -> UIViewController? {
        currentControllerIndex(VC: viewController)
        guard let viewControllerIndex = dashboardViewControllers.index(of: viewController) else {
            return nil
        }
        let previousIndex = viewControllerIndex - 1
        guard previousIndex >= 0 else {
            return nil
        }
        guard dashboardViewControllers.count > previousIndex else {
            return nil
        }
        return dashboardViewControllers[previousIndex]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController,
                            viewControllerAfter viewController: UIViewController) -> UIViewController? {
        currentControllerIndex(VC: viewController)
        guard let viewControllerIndex = dashboardViewControllers.index(of: viewController) else {
            return nil
        }
        let nextIndex = viewControllerIndex + 1
        guard dashboardViewControllers.count != nextIndex else {
            return nil
        }
        guard dashboardViewControllers.count > nextIndex else {
            return nil
        }
        return dashboardViewControllers[nextIndex]
    }
}
