//
//  TabbarViewController.swift
//  Moneytoss
//
//  Created by qw on 15/08/20.
//  Copyright © 2020 qw. All rights reserved.
//

import UIKit


class TabbarViewController: UIViewController {
    //MARK: IBOutlet
    @IBOutlet weak var homeImage: UIImageView!
    @IBOutlet weak var homeButton: CustomButton!
    @IBOutlet weak var homeSelectionView: View!
    
    @IBOutlet weak var transactionImage: UIImageView!
    @IBOutlet weak var transactionButton: CustomButton!
    @IBOutlet weak var transactionSelectionView: View!
    
    @IBOutlet weak var sendImage: UIImageView!
    @IBOutlet weak var sendSelectionView: View!
    
    @IBOutlet weak var contactImage: UIImageView!
    @IBOutlet weak var contactButton: CustomButton!
    @IBOutlet weak var contactSelectionView: View!
    
    @IBOutlet weak var walletImage: UIImageView!
    @IBOutlet weak var walletButton: CustomButton!
    @IBOutlet weak var walletSelectionView: View!
    
    var currentIndex = Int()
    var userData = TransactionResponse()
    var isNewPayment = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initView()
        if(self.isNewPayment){
            self.sendAction(self)
        }else {
            self.homeAction(self)
        }
    }
    
    func initView(){
        self.homeImage.image = #imageLiteral(resourceName: "home 1")
        self.transactionImage.image = #imageLiteral(resourceName: "transactions")
        self.sendImage.image = #imageLiteral(resourceName: "centre_black")
        self.contactImage.image = #imageLiteral(resourceName: "contacts")
        self.walletImage.image = #imageLiteral(resourceName: "mywallet")
        self.homeButton.setTitleColor(.black, for: .normal)
        self.transactionButton.setTitleColor(.black, for: .normal)
        self.contactButton.setTitleColor(.black, for: .normal)
        self.walletButton.setTitleColor(.black, for: .normal)
        self.homeSelectionView.backgroundColor = .clear
        self.transactionSelectionView.backgroundColor = .clear
        self.sendSelectionView.backgroundColor = .clear
        self.contactSelectionView.backgroundColor = .clear
        self.walletSelectionView.backgroundColor = .clear
        self.view.backgroundColor = .white
        NotificationCenter.default.addObserver(self, selector: #selector(self.showSelectedTab(_:)), name: NSNotification.Name("change_tab"), object: nil)
    }
    
    
    @objc func showSelectedTab(_ notif: Notification) {
        if let tab = notif.userInfo?["tab"] as? String {
            if(tab == "1"){
                self.transactionAction(self)
            }else if(tab == "2"){
                self.userData = notif.userInfo?["userData"] as! TransactionResponse
                self.sendAction(self)
            }
        }
        
    }
    
    //MARK: IBActions
    @IBAction func homeAction(_ sender: Any) {
        
        self.initView()
        
        self.homeSelectionView.backgroundColor = primaryColor
        let pageVC = TabPageViewController.dataSource1 as? TabPageViewController
        pageVC?.setControllerFirst()
        
    }
    
    @IBAction func transactionAction(_ sender: Any) {
        self.initView()
        self.transactionSelectionView.backgroundColor = primaryColor
        let pageVC = TabPageViewController.dataSource1 as? TabPageViewController
        pageVC?.setControllerSecond()
    }
    
    @IBAction func sendAction(_ sender: Any) {
//        self.view.backgroundColor = primaryColor
//        self.homeImage.image = #imageLiteral(resourceName: "white_home")
//        self.transactionImage.image = #imageLiteral(resourceName: "white_transaction")
//        self.sendImage.image = #imageLiteral(resourceName: "centre_white")
//        self.contactImage.image = #imageLiteral(resourceName: "contacts_white")
//        self.walletImage.image = #imageLiteral(resourceName: "contacts_wallet")
//        self.homeButton.setTitleColor(.white, for: .normal)
//        self.transactionButton.setTitleColor(.white, for: .normal)
//        self.contactButton.setTitleColor(.white, for: .normal)
//        self.walletButton.setTitleColor(.white, for: .normal)
//        self.homeSelectionView.backgroundColor = .clear
//        self.transactionSelectionView.backgroundColor = .clear
//        self.sendSelectionView.backgroundColor = lightBlueColor
//        self.contactSelectionView.backgroundColor = .clear
//        self.walletSelectionView.backgroundColor = .clear
        self.initView()
        self.sendSelectionView.backgroundColor = primaryColor
        let pageVC = TabPageViewController.dataSource1 as? TabPageViewController
        pageVC?.setControllerThird()
//        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "SendViewController") as! SendViewController
//        myVC.userData = self.userData
//        NavigationController.shared.addTransition(direction:.fromTop, controller: self)
//        self.navigationController?.pushViewController(myVC, animated: false)
    }
    
    @IBAction func contactAction(_ sender: Any) {
        self.initView()
        self.contactSelectionView.backgroundColor = primaryColor
        let pageVC = TabPageViewController.dataSource1 as? TabPageViewController
        pageVC?.setControllerFourth()
    }
    
    @IBAction func walletAction(_ sender: Any) {
        self.initView()
        self.walletSelectionView.backgroundColor = primaryColor
        let pageVC = TabPageViewController.dataSource1 as? TabPageViewController
        pageVC?.setControllerLast()
    }
}
