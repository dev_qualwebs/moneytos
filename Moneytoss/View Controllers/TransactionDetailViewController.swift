//
//  TransactionDetailViewController.swift
//  Moneytoss
//
//  Created by Sagar Pandit on 24/06/21.
//  Copyright © 2021 qw. All rights reserved.
//

import UIKit
import SDWebImage

class TransactionDetailViewController: UIViewController {
    //MARK: IBOUtlets
    @IBOutlet weak var transactionTable: ContentSizedTableView!
    
    @IBOutlet weak var userImage: ImageView!
    @IBOutlet weak var username: FloatingPlaceholderTextField!
    @IBOutlet weak var userEmail: FloatingPlaceholderTextField!
    @IBOutlet weak var userNumber: FloatingPlaceholderTextField!
    @IBOutlet weak var noTransactionLabel: DesignableUILabel!
    
    
    var transactionData = [TransactionResponse]()
    var transactionDetail = TransactionResponse()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.getTransaction()
        self.initializeView()
    }
    
    
    func initializeView(){
        if(transactionDetail.receiver_id == Singleton.shared.userDetail.id){
            self.userImage.sd_setImage(with: URL(string:self.getImageURL(text:transactionDetail.sender_profile_image ?? "")), placeholderImage: #imageLiteral(resourceName: "round_image"))
            self.username.textInput?.text = transactionDetail.sender_name
            self.userEmail.textInput?.text = transactionDetail.sender_email
            self.userNumber.textInput?.text = transactionDetail.sender_phone_number
        }else {
            self.userImage.sd_setImage(with: URL(string:self.getImageURL(text:transactionDetail.receiver_profile_image ?? "")), placeholderImage: #imageLiteral(resourceName: "round_image"))
            self.username.textInput?.text = transactionDetail.receiver_name
            self.userEmail.textInput?.text = transactionDetail.receiver_email
            self.username.textInput?.text = transactionDetail.receiver_phone_number
        }
    }
    
    
    func getTransaction(){
        ActivityIndicator.show(view: self.view)
        let id = (transactionDetail.receiver_id == Singleton.shared.userDetail.id) ? transactionDetail.sender_id:transactionDetail.receiver_id
        
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_GET_ALL_TRANSACTIONS, method: .post, parameter: ["user_id":id ?? 0], objectClass: GetTransaction.self, requestCode: U_GET_ALL_TRANSACTIONS) { (response) in
            self.transactionData = response.response
            self.transactionTable.reloadData()
            ActivityIndicator.hide()
        }
    }

    //MARK: IBActions
    @IBAction func sendAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
        NotificationCenter.default.post(name: NSNotification.Name("change_tab"), object: nil,userInfo: ["tab":"2", "userData": self.transactionDetail])
        Singleton.shared.sendDatauser = self.transactionDetail
    }
    
    @IBAction func requestAction(_ sender: Any) {
//        let alert = UIAlertController(title: "Request Money", message: "send reqest to \(cell.userName.text ?? "") for $\(self.amount) ", preferredStyle: .alert)
//        let action1 = UIAlertAction(title: "Yes", style: .default) { (action) in
//            let param: [String:Any] = [
//                "requested_user_id":(self.transactionDetail.receiver_id == Singleton.shared.userDetail.id) ? self.transactionDetail.sender_id:self.transactionDetail.receiver_id,
//                "amount": self.amount,
//                "currency_id": Singleton.shared.userDetail.currency_id ?? 0
//            ]
//
//            ActivityIndicator.show(view: self.view)
//            SessionManager.shared.methodForApiCalling(url: U_BASE + U_REQUEST_MONEY, method: .post, parameter: param, objectClass: SuccessResponse.self, requestCode: U_REQUEST_MONEY) { (response) in
//                Singleton.shared.showToast(text: "Successfully sent request.")
//            }
//        }
//
//        let action2 = UIAlertAction(title: "No", style: .default) { (action) in
//            self.dismiss(animated: false, completion: nil)
//        }
//
//        alert.addAction(action1)
//        alert.addAction(action2)
//        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
}

extension TransactionDetailViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
            if(self.transactionData.count == 0){
                self.noTransactionLabel.isHidden = false
            }else {
                self.noTransactionLabel.isHidden = true
            }
            return self.transactionData.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TransactionTableCell") as! TransactionTableCell
        
        let val = self.transactionData[indexPath.row]
        
        if(val.receiver_id == Singleton.shared.userDetail.id){
            if((val.sender_profile_image ?? "").contains("http")){
                cell.userImage.sd_setImage(with: URL(string: self.getImageURL(text:val.sender_profile_image ?? "")), placeholderImage: #imageLiteral(resourceName: "round_image"))
            }else {
                 cell.userImage.sd_setImage(with: URL(string: self.getImageURL(text:U_IMAGE_BASE + (val.sender_profile_image ?? ""))), placeholderImage: #imageLiteral(resourceName: "round_image"))
            }

            cell.userName.text = val.sender_name
            
        }else {
            if((val.receiver_profile_image ?? "").contains("http")){
                cell.userImage.sd_setImage(with: URL(string: self.getImageURL(text:val.receiver_profile_image ?? "")), placeholderImage: #imageLiteral(resourceName: "round_image"))
            }else {
                 cell.userImage.sd_setImage(with: URL(string: self.getImageURL(text:U_IMAGE_BASE + (val.receiver_profile_image ?? ""))), placeholderImage: #imageLiteral(resourceName: "round_image"))
            }

            cell.userName.text = val.receiver_name ?? "You"
        }
        if(val.transaction_type == 2){
            cell.transactionAmount.textColor = redColor
            cell.transactionAmount.text = "-" + (val.amount ?? "")
        }else {
            cell.transactionAmount.textColor = greenColor
            cell.transactionAmount.text = "+" + (val.amount ?? "")
        }
        cell.transactionType.text = val.transaction_method ?? "Stripe"
        cell.transactionDate.text = "On " + self.convertTimestampToDate(val.created_at ?? 0, to: "MMM dd, yyyy")
        return cell
    }
}
