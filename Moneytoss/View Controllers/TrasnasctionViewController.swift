//
//  TrasnasctionViewController.swift
//  Moneytoss
//
//  Created by qw on 15/08/20.
//  Copyright © 2020 qw. All rights reserved.
//

import UIKit

class TrasnasctionViewController: UIViewController, UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        self.searchText = (textField.text ?? "") + string
        self.searchData = []
        self.searchData = self.transactionData.filter{
            
                ($0.sender_name ?? "").lowercased().contains(self.searchText.lowercased()) || ($0.receiver_name ?? "").lowercased().contains(self.searchText.lowercased())
            
        }
        if(self.searchText == ""){
            self.searchData = []
        }
        self.transactionTable.reloadData()
        return true
    }
    
    
    //MARK: IBOutlets
    @IBOutlet weak var searchFIeld: DesignableUITextField!
    @IBOutlet weak var searchPlaceholder: DesignableUILabel!
    @IBOutlet weak var searchField: UITextField!
    @IBOutlet weak var searchIcon: UIImageView!
    @IBOutlet weak var searchButton: UIButton!
    @IBOutlet weak var transactionTable: ContentSizedTableView!
    @IBOutlet weak var userImage: ImageView!
    @IBOutlet weak var noTransactionLabel: UIStackView!
    
    
    var transactionData = [TransactionResponse]()
    var searchData = [TransactionResponse]()
    var searchText = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.searchField.delegate = self
        searchIcon.image = #imageLiteral(resourceName: "mysearch")
        self.searchFIeld.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        NavigationController.shared.getTransactionData { (data) in
            self.transactionData = data
            self.transactionTable.reloadData()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        let profileData = Singleton.shared.userDetail
        if((profileData.profile_image ?? "").contains("http")){
            self.userImage.sd_setImage(with: URL(string: self.getImageURL(text:profileData.profile_image ?? "")), placeholderImage: #imageLiteral(resourceName: "round_image"))
        }else {
            self.userImage.sd_setImage(with: URL(string: self.getImageURL(text:U_IMAGE_BASE + (profileData.profile_image ?? ""))), placeholderImage: #imageLiteral(resourceName: "round_image"))
        }
    }
    
    //MARK: IBActions
    @IBAction func profileAction(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
        self.navigationController?.pushViewController(myVC, animated: true)
    }
    
    @IBAction func filterAction(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "FilterTransactionViewController") as! FilterTransactionViewController
        self.navigationController?.pushViewController(myVC, animated: true)
    }
    
    @IBAction func searchAction(_ sender: Any) {
        //        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "SearchBarViewController") as! SearchBarViewController
        //        self.navigationController?.pushViewController(myVC, animated: true)
        if(searchIcon.image == #imageLiteral(resourceName: "mysearch")){
            searchIcon.image = #imageLiteral(resourceName: "clear")
            self.searchField.text = ""
            self.searchText = ""
            self.searchData = []
            self.searchField.becomeFirstResponder()
            self.transactionTable.reloadData()
            self.searchField.isHidden = false
        }else {
            searchIcon.image = #imageLiteral(resourceName: "mysearch")
            self.searchField.text = ""
            
            self.searchText = ""
            self.searchData = []
            self.searchField.resignFirstResponder()
            self.transactionTable.reloadData()
            self.searchField.isHidden = true
        }
    }
    
    @IBAction func notificationAction(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "NotificationViewController") as! NotificationViewController
        self.navigationController?.pushViewController(myVC, animated: true)
    }
    
    @IBAction func qrScanAction(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "ScannerVC") as! ScannerVC
        myVC.scanDelegalte = self
        self.present(myVC, animated: true, completion: nil)
    }
    
    func getUserInfoFromScan(code: String?) {
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_SEARCH_USER_VIA_EMAIL, method: .post, parameter: ["email":code], objectClass: GetUserInfoScan.self, requestCode: U_SEARCH_USER_VIA_EMAIL) { response in
         
            Singleton.shared.sendDatauser = TransactionResponse(id: response.response.id, transaction_type: nil, amount: nil, local_transaction_id: nil, user_id: nil, currency_id: nil, created_at: nil, notes: nil, transaction_method: nil, sender_name: (response.response.first_name ?? "") + " " + (response.response.last_name ?? ""), sender_id: response.response.id, receiver_name: nil, sender_profile_image: response.response.profile_image, receiver_profile_image: nil, receiver_id: nil, iso_code: nil, masked_card_number: nil, gateway_name: nil, sender_phone_number: response.response.phone_number, sender_email: response.response.email, receiver_phone_number: nil, receiver_email: nil)
            NotificationCenter.default.post(name: NSNotification.Name("change_tab"), object: nil,userInfo: ["tab":"2", "userData": Singleton.shared.sendDatauser])
        }
    }
    
}

extension TrasnasctionViewController: UITableViewDelegate, UITableViewDataSource, ScanQr {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(self.searchData.count > 0){
            return self.searchData.count
        }else {
            if(self.transactionData.count == 0){
                self.noTransactionLabel.isHidden = false
            }else {
                self.noTransactionLabel.isHidden = true
            }
            return self.transactionData.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TransactionTableCell") as! TransactionTableCell
        var val = TransactionResponse()
        if(self.searchData.count > 0){
            val = self.searchData[indexPath.row]
        }else {
            val = self.transactionData[indexPath.row]
        }
        if(val.receiver_id == Singleton.shared.userDetail.id){
            if((val.sender_profile_image ?? "").contains("http")){
                cell.userImage.sd_setImage(with: URL(string: self.getImageURL(text:val.sender_profile_image ?? "")), placeholderImage: #imageLiteral(resourceName: "round_image"))
            }else {
                 cell.userImage.sd_setImage(with: URL(string: self.getImageURL(text:U_IMAGE_BASE + (val.sender_profile_image ?? ""))), placeholderImage: #imageLiteral(resourceName: "round_image"))
            }

            cell.userName.text = val.sender_name
            
        }else {
            if((val.receiver_profile_image ?? "").contains("http")){
                cell.userImage.sd_setImage(with: URL(string: self.getImageURL(text:val.receiver_profile_image ?? "")), placeholderImage: #imageLiteral(resourceName: "round_image"))
            }else {
                 cell.userImage.sd_setImage(with: URL(string: self.getImageURL(text:U_IMAGE_BASE + (val.receiver_profile_image ?? ""))), placeholderImage: #imageLiteral(resourceName: "round_image"))
            }
            cell.userName.text = val.receiver_name ?? "You"
        }
        if(val.transaction_type == 2){
            cell.transactionAmount.textColor = redColor
            cell.transactionAmount.text = "-" + (val.amount ?? "")
        }else {
            cell.transactionAmount.textColor = greenColor
            cell.transactionAmount.text = "+" + (val.amount ?? "")
        }
        cell.transactionType.text = val.transaction_method ?? "Stripe"
        cell.transactionDate.text = "On " + self.convertTimestampToDate(val.created_at ?? 0, to: "MMM dd, yyyy")
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if(self.transactionData[indexPath.row].transaction_type == 1){
            return
        }
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "TransactionDetailViewController") as! TransactionDetailViewController
        myVC.transactionDetail = self.transactionData[indexPath.row]
        self.navigationController?.pushViewController(myVC, animated: true)
    }
}

