//
//  WalletViewController.swift
//  Moneytoss
//
//  Created by qw on 15/08/20.
//  Copyright © 2020 qw. All rights reserved.
//

import UIKit

class WalletViewController: UIViewController {
    //MARK: IBOutlets
    @IBOutlet weak var transactionTable: ContentSizedTableView!
    @IBOutlet weak var userImage: ImageView!
    @IBOutlet weak var noTransactionLabel: UIStackView!
    @IBOutlet weak var walletBalance: DesignableUILabel!
    
    var transactionData = [TransactionResponse]()
    var walletData = [WalletResponse]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        NavigationController.shared.getTransactionData { (data) in
            
            self.transactionData = data
            self.transactionTable.reloadData()
        }
        
        NavigationController.shared.getWalletData(completionHandler: { (data) in
            self.walletData = data
            if(self.walletData.count > 0){
            self.walletBalance.text = (self.walletData[0].symbol ?? "") + (self.walletData[0].balance ?? "0.0")
            }else {
                self.walletBalance.text = "$0"
            }
        })
    }
    
    override func viewDidAppear(_ animated: Bool) {
        let profileData = Singleton.shared.userDetail
        if((profileData.profile_image ?? "").contains("http")){
            self.userImage.sd_setImage(with: URL(string: self.getImageURL(text:profileData.profile_image ?? "")), placeholderImage: #imageLiteral(resourceName: "round_image"))
        }else {
            self.userImage.sd_setImage(with: URL(string: self.getImageURL(text:U_IMAGE_BASE + (profileData.profile_image ?? ""))), placeholderImage: #imageLiteral(resourceName: "round_image"))
        }
    }
       
    
    //MARK: IBActions
     @IBAction func profileAction(_ sender: Any) {
         let myVC = self.storyboard?.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
         self.navigationController?.pushViewController(myVC, animated: true)
    }
    
    @IBAction func notificationAction(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "NotificationViewController") as! NotificationViewController
        self.navigationController?.pushViewController(myVC, animated: true)
    }
    
    @IBAction func qrScanAction(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "ScannerVC") as! ScannerVC
        myVC.scanDelegalte = self
        self.present(myVC, animated: true, completion: nil)
    }
    
    func getUserInfoFromScan(code: String?) {
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_SEARCH_USER_VIA_EMAIL, method: .post, parameter: ["email":code], objectClass: GetUserInfoScan.self, requestCode: U_SEARCH_USER_VIA_EMAIL) { response in
            
            Singleton.shared.sendDatauser = TransactionResponse(id: nil, transaction_type: nil, amount: nil, local_transaction_id: nil, user_id: nil, currency_id: nil, created_at: nil, notes: nil, transaction_method: nil, sender_name: (response.response.first_name ?? "") + " " + (response.response.last_name ?? ""), sender_id: response.response.id, receiver_name: nil, sender_profile_image: response.response.profile_image, receiver_profile_image: nil, receiver_id: nil, iso_code: nil, masked_card_number: nil, gateway_name: nil, sender_phone_number: response.response.phone_number, sender_email: response.response.email, receiver_phone_number: nil, receiver_email: nil)
            NotificationCenter.default.post(name: NSNotification.Name("change_tab"), object: nil,userInfo: ["tab":"2", "userData": Singleton.shared.sendDatauser])
        }
    }
}


extension WalletViewController: UITableViewDelegate, UITableViewDataSource, ScanQr {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       
            if(self.transactionData.count == 0){
                self.noTransactionLabel.isHidden = false
            }else {
                self.noTransactionLabel.isHidden = true
            }
            return self.transactionData.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TransactionTableCell") as! TransactionTableCell
        var val =  self.transactionData[indexPath.row]
        if(val.receiver_id == Singleton.shared.userDetail.id){
            if((val.sender_profile_image ?? "").contains("http")){
                cell.userImage.sd_setImage(with: URL(string: self.getImageURL(text:val.sender_profile_image ?? "")), placeholderImage: #imageLiteral(resourceName: "round_image"))
            }else {
                 cell.userImage.sd_setImage(with: URL(string: self.getImageURL(text:U_IMAGE_BASE + (val.sender_profile_image ?? ""))), placeholderImage: #imageLiteral(resourceName: "round_image"))
            }

            cell.userName.text = val.sender_name
            
        }else {
            if((val.receiver_profile_image ?? "").contains("http")){
                cell.userImage.sd_setImage(with: URL(string: self.getImageURL(text:val.receiver_profile_image ?? "")), placeholderImage: #imageLiteral(resourceName: "round_image"))
            }else {
                 cell.userImage.sd_setImage(with: URL(string: self.getImageURL(text:U_IMAGE_BASE + (val.receiver_profile_image ?? ""))), placeholderImage: #imageLiteral(resourceName: "round_image"))
            }

            cell.userName.text = val.receiver_name ?? "You"
        }
        if(val.transaction_type == 2){
            cell.transactionAmount.textColor = redColor
        }else {
            cell.transactionAmount.textColor = greenColor
        }
        cell.transactionAmount.text = val.amount
        cell.transactionType.text = val.transaction_method ?? "Stripe"
        cell.transactionDate.text = "On " + self.convertTimestampToDate(val.created_at ?? 0, to: "MMM dd, yyyy")
        return cell
    }
}
