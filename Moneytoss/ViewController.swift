//
//  ViewController.swift
//  Moneytoss
//
//  Created by qw on 14/08/20.
//  Copyright © 2020 qw. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    //MARK: IBOutlets
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let controller = UserDefaults.standard.value(forKey: UD_TOKEN) as? String{
            let myVC = self.storyboard?.instantiateViewController(withIdentifier: "TabbarViewController") as! TabbarViewController
            self.navigationController?.pushViewController(myVC, animated: true)
            
        }else {
            if let isFirstTime = UserDefaults.standard.value(forKey: UD_IS_FIRST_TIME) as? String{
                if let controller = UserDefaults.standard.value(forKey: UD_INITIAL_VIEW_CONTROLLER) as? String{
                    let myVC = self.storyboard?.instantiateViewController(withIdentifier: "MobileVerificationViewController") as! MobileVerificationViewController
                    let email = UserDefaults.standard.value(forKey: UD_USER_EMAIL) as! String
                    myVC.email = email
                    myVC.isComingDirect = true
                    self.navigationController?.pushViewController(myVC, animated: true)
                }else{
                    let myVC = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
                    self.navigationController?.pushViewController(myVC, animated: true)
                }
            }else {
                let myVC = self.storyboard?.instantiateViewController(withIdentifier: "SplashViewController") as! SplashViewController
                UserDefaults.standard.setValue("false", forKey: UD_IS_FIRST_TIME)
                self.navigationController?.pushViewController(myVC, animated: true)
                
            }
        }
    }
    //MARK: IBActions
    
    
}

